<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ asset('bsb/images/user.png') }}" width="48" height="48" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}
                <span class="label bg-light-blue">{{Auth::user()->role->name ?? ''}}</span>
                <!-- {{Auth::user()->role->name}} -->
            </div>
            <div class="email">{{ Auth::user()->company->name ?? '' }}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <!-- <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="separator" class="divider"></li> -->
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                class="material-icons">input</i>Sign Out</a>


                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li id="menuHome">
                <a href="{{ route('home')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            @isset(Auth::user()->role->name)
            @if(Auth::user()->role->name == 'Administrator')
            <li class="{{$masterActive ?? ''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">assignment</i>
                    <span>Master</span>
                </a>
                <ul class="ml-menu">

                    <li class="{{$userActive ?? ''}}">
                        <a href="{{ route('user.index')}}">User</a>
                    </li>
                    <li class="{{$roleActive ?? ''}}">
                        <a href="{{ route('role.index')}}">Role</a>
                    </li>
                    <li class="{{$companyActive ?? ''}}">
                        <a href="{{ route('company.index')}}">Company</a>
                    </li>

                </ul>
            </li>
            @endif
            @if(Auth::user()->role->name == 'Admin')

            <li class="{{$masterActive ?? ''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">assignment</i>
                    <span>Master</span>
                </a>
                <ul class="ml-menu">

                    <li class="{{$statusActive ?? ''}}">
                        <a href="{{ route('status.index')}}">Status</a>
                    </li>
                    <li class="{{$divisionActive ?? ''}}">
                        <a href="{{ route('division.index')}}">Division</a>
                    </li>
                    <li class="{{$positionActive ?? ''}}">
                        <a href="{{ route('position.index')}}">Position</a>
                    </li>
                    <!-- <li class="{{$partnerActive ?? ''}}">
                        <a href="{{ route('partner.index')}}">Partner</a>
                    </li> -->
                    <li class="{{$memberActive ?? ''}}">
                        <a href="{{ route('member.index')}}">Member</a>
                    </li>
                    <li class="{{$signatureActive ?? ''}}">
                        <a href="{{ route('signature.index')}}">Signature</a>
                    </li>
                    <li class="{{$forsignatureActive ?? ''}}">
                        <a href="{{ route('forsignature.index')}}">Penempatan Tandatangan</a>
                    </li>

                </ul>
            </li>
            @endif
            @endisset

            @isset(Auth::user()->role->name)
            @if(Auth::user()->role->name == 'Admin')
            <li class="{{$partnerActive ?? ''}}">
                <a href="{{ route('partner.index')}}">
                    <i class="material-icons">text_fields</i>
                    <span>Partner</span>
                </a>
            </li>
            <!-- <li class="{{$penggajianActive ?? ''}}">
                <a href="{{ route('penggajian.index')}}">
                    <i class="material-icons">attach_money</i>
                    <span>Penggajian</span>
                </a>
            </li> -->
            <li class="{{$penggajianActive ?? ''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">attach_money</i>
                    <span>Penggajian</span>
                </a>
                <ul class="ml-menu">


                    <li class="{{$variableSetActive ?? ''}}">
                        <a href="{{ route('variable.index')}}">Variable</a>
                    </li>
                    <li class="{{$penggajianSetActive ?? ''}}">
                        <a href="{{ route('penggajian.index')}}">Setting</a>
                    </li>
                    <li class="{{$penggajianGenerateActive ?? ''}}">
                        <a href="{{ route('penggajian.generate')}}">Generate</a>
                    </li>




                </ul>
            </li>

            <li class="{{$ceklistActive ?? ''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">chrome_reader_mode</i>
                    <span>Korlap</span>
                </a>
                <ul class="ml-menu">


                    <li class="{{$dataActive ?? ''}}">
                        <a href="{{ route('ceklist.index')}}">Master Cek List</a>
                    </li>
                    <li class="{{$rekapActive ?? ''}}">
                        <a href="{{ route('ceklist.rekap')}}">Rekap Cek List</a>
                    </li>
                    <li class="{{$rencanakunjunganceklistActive ?? ''}}">
                        <a href="{{ route('visit.index')}}">Rencana Kunjungan</a>
                    </li>
                    <li class="{{$kunjunganceklistActive ?? ''}}">
                        <a href="{{ route('ceklist.kunjungan')}}">Hasil Kunjungan</a>
                    </li>
                </ul>
            </li>

            @endif

            @if(Auth::user()->role->name == 'Akuntansi'||Auth::user()->role->name == 'Admin')
            <li class="{{$invoiceActive ?? ''}}">
                <a href="{{ route('invoice.index')}}">
                    <i class="material-icons">record_voice_over</i>
                    <span>Invoice</span>
                </a>
            </li>
            <li class="{{$akuntansiActive ?? ''}}">
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">account_balance_wallet</i>
                    <span>Akuntansi</span>
                </a>
                <ul class="ml-menu">
                    <li class="{{$transaksiActive ?? ''}}">
                        <a href="{{ route('transaction.index')}}">Transaksi</a>
                    </li>
                    <li class="{{$reportActive ?? ''}}">
                        <a href="{{ route('report.index')}}">Laporan</a>
                    </li>
                </ul>
            </li>
            <li class="{{$rekapAbsenActive ?? ''}}">
                <a href="{{ route('rekap.index')}}">
                    <i class="material-icons">receipt</i>
                    <span>Rekap Absen</span>
                </a>
            </li>
            <li class="{{$PermintaanbarangActive ?? ''}}">
                <a href="{{ route('permintaanbarang.index')}}">
                    <i class="material-icons">flight_takeoff</i>
                    <span>Permintaan Barang</span>
                </a>
            </li>
            <li class="{{$serviceActive ?? ''}}">
                <a href="{{ route('service.index')}}">
                    <i class="material-icons">build</i>
                    <span>Contact Service</span>
                </a>
            </li>
            <!-- <li class="{{$visitActive ?? ''}}">
                <a href="{{ route('visit.index')}}">
                    <i class="material-icons">assessment</i>
                    <span>Rencana Kunjungan</span>
                </a>
            </li> -->
            <li class="{{$activityActive ?? ''}}">
                <a href="{{ route('activity.index')}}">
                    <i class="material-icons">directions_run</i>
                    <span>Activity</span>
                </a>
            </li>
            <li class="{{$absenActive ?? ''}}">
                <a href="{{ route('createAbsen.index')}}">
                    <i class="material-icons">view_module</i>
                    <span>Setting Absen</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->role->name == 'Koordinator Lapangan')
            <li class="{{$recordActive ?? ''}}">
                <a href="{{ route('record.index')}}">
                    <i class="material-icons">text_fields</i>
                    <span>Ceklist</span>
                </a>
            </li>
            <li class="{{$kunjunganActive ?? ''}}">
                <a href="{{ route('kunjungan.index')}}">
                    <i class="material-icons">subtitles</i>
                    <span>Hasil Kunjungan</span>
                </a>
            </li>
            <li class="{{$visitkunjunganActive ?? ''}}">
                <a href="{{ route('kunjungan.visit')}}">
                    <i class="material-icons">assessment</i>
                    <span>Rencana Kunjungan</span>
                </a>
            </li>
            <li class="{{$PermintaanbarangkorlapActive ?? ''}}">
                <a href="{{ route('permintaanbarangkorlap.index')}}">
                    <i class="material-icons">flight_takeoff</i>
                    <span>Permintaan Barang</span>
                </a>
            </li>
            <li class="{{$servicekorlapActive ?? ''}}">
                <a href="{{ route('servicekorlap.index')}}">
                    <i class="material-icons">build</i>
                    <span>Contact Service</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->role->name == 'SDM')
            <li class="{{$sdmActive ?? ''}}">
                <a href="{{ route('member_sdm.index')}}">
                    <i class="material-icons">contacts</i>
                    <span>Member</span>
                </a>
            </li>
            <li class="{{$partnersdmActive ?? ''}}">
                <a href="{{ route('partnersdm.index')}}">
                    <i class="material-icons">text_fields</i>
                    <span>Partner</span>
                </a>
            </li>
            <li class="{{$rekapAbsenSdmActive ?? ''}}">
                <a href="{{ route('rekapsdm.index')}}">
                    <i class="material-icons">receipt</i>
                    <span>Rekap Absen</span>
                </a>
            </li>
            @endif
            @if(Auth::user()->role->name == 'Logistik')
            <li class="{{$logistikActive ?? ''}}">
                <a href="{{ route('logistik.index')}}">
                    <i class="material-icons">flight_takeoff</i>
                    <span>Permintaan barang</span>
                </a>
            </li>

            @endif

            @if(Auth::user()->role->name == 'Koordinator Mitra')
            <li class="{{$barangmitraActive ?? ''}}">
                <a href="{{ route('barangmitra.index')}}">
                    <i class="material-icons">text_fields</i>
                    <span>Barang</span>
                </a>
            </li>
            @endif

            @endisset
            <!-- @if(Auth::user()->role == 'admin')
            <li id="menuAdministrator">
                <a href="">
                    <i class="material-icons">text_fields</i>
                    <span>Administrator</span>
                </a>
            </li>
            @endif
            <li id="menuSuratMasuk">
                <a href="#">
                    <i class="material-icons">layers</i>
                    <span>Surat Masuk</span>
                </a>
            </li>

            <li id="menuDokumen">
                <a href="#">
                    <i class="material-icons">remove_red_eye</i>
                    <span>E Document</span>
                </a>
            </li>
            <li id="menuKelas">
                <a href="#">
                    <i class="material-icons">account_balance</i>
                    <span>Kelas</span>
                </a>
            </li> -->

            <li class="{{$slipActive ?? ''}}">
                <a href="{{ route('slip.index')}}">
                    <i class="material-icons">credit_card</i>
                    <span>Slip</span>
                </a>
            </li>
            <li class="{{$jadwalActive ?? ''}}">
                <a href="{{ route('slip.jadwal')}}">
                    <i class="material-icons">date_range</i>
                    <span>Jadwal</span>
                </a>
            </li>

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2022 <a href="javascript:void(0);">Bhayu Studio</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1
        </div>
    </div>
    <!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->