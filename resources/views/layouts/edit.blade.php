<!-- Modal Create -->
@isset($form_action)
<div class="modal fade" id="ModalCreate" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="form_validation6" action="{{ route($form_action) }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Tambah</h4>
                </div>
                <div class="modal-body">
                    <!-- <input type="hidden" name="researche_id" class="researche_id"> -->
                    @forelse ($form as $item)


                    @if( $item['type'] == 'select')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>

                    @elseif($item['type'] == 'chain')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2" id="{{$item['name']}}_change">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>

                    @elseif($item['type'] == 'select2')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2_new_modal">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>

                    @elseif($item['type'] == 'chained')


                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2" id="{{$item['name']}}_change">
                                <option value="">-- {{$item['label']}} --</option>

                            </select>

                        </div>
                    </div>
                    @elseif($item['type'] == 'tag')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="{{$item['name']}}" data-role="tagsinput"
                                        class="form-control" autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif($item['type'] == 'date')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="date" name="{{$item['name']}}" class="form-control" autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif($item['type'] == 'daterange')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="{{$item['name']}}" class="form-control daterange"
                                        autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif($item['type'] == 'hidden')
                    <input type="hidden" name="{{$item['name']}}" value="{{$item['value']}}">
                    @else
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="{{$item['type']}}" name="{{$item['name']}}" class="form-control"
                                        autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>
                                </div>
                            </div>
                        </div>
                    </div>


                    @endif


                    @empty
                    @endforelse




                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



@endisset

@isset($form_action_upload)
<div class="modal fade" id="ModalCreate" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="form_validation6" action="{{ route($form_action_upload) }}" method="post" autocomplete="off"
                enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Tambah</h4>
                </div>
                <div class="modal-body">
                    <!-- <input type="hidden" name="researche_id" class="researche_id"> -->
                    @forelse ($form as $item)


                    @if( $item['type'] == 'select')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>

                    @elseif($item['type'] == 'chain')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2" id="{{$item['name']}}_change">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>

                    @elseif($item['type'] == 'select2')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2_new_modal">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>

                    @elseif($item['type'] == 'chained')


                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2" id="{{$item['name']}}_change">
                                <option value="">-- {{$item['label']}} --</option>

                            </select>

                        </div>
                    </div>
                    @elseif($item['type'] == 'tag')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="{{$item['name']}}" data-role="tagsinput"
                                        class="form-control" autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif($item['type'] == 'date')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="date" name="{{$item['name']}}" class="form-control" autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif($item['type'] == 'daterange')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="{{$item['name']}}" class="form-control daterange"
                                        autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>

                    @elseif($item['type'] == 'file')




                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <input type="file" name="{{$item['name']}}" class="form-control" autocomplete="off">

                        </div>
                    </div>
                    @elseif($item['type'] == 'hidden')
                    <input type="hidden" name="{{$item['name']}}" value="{{$item['value']}}">
                    @else
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="{{$item['type']}}" name="{{$item['name']}}" class="form-control"
                                        autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>
                                </div>
                            </div>
                        </div>
                    </div>


                    @endif


                    @empty
                    @endforelse




                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



@endisset


@isset($form_update)


<!-- Modal Edit -->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="form_validation6" action="{{ route($form_update) }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">

                    <h4 class="modal-title" id="largeModalLabel">Edit</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">


                    @forelse ($form as $item)

                    @if($item['type'] == 'text')
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="{{$item['type']}}" name="{{$item['name']}}" class="form-control"
                                        autocomplete="off" id="{{$item['name']}}">
                                    <label class="form-label">{{$item['label']}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($item['type'] == 'password')
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="{{$item['type']}}" name="{{$item['name']}}" class="form-control"
                                        autocomplete="off" id="{{$item['name']}}">
                                    <label class="form-label">{{$item['label']}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($item['type'] == 'tag')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="{{$item['name']}}" data-role="tagsinput"
                                        id="{{$item['name']}}" class="form-control" autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if( $item['type'] == 'select')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2" id="{{$item['name']}}">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $items)
                                <option value="{{$items['id']}}">{{$items['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>
                    @endif

                    @if( $item['type'] == 'select2')


                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2_new_modal_edit"
                                id="{{$item['name']}}">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $items)

                                <option value="{{$items['id']}}">{{$items['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>
                    @endif

                    @if( $item['type'] == 'date')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <input type="date" name="{{$item['name']}}" class="form-control" autocomplete="off"
                                id="{{$item['name']}}">

                        </div>
                    </div>
                    @endif
                    @if( $item['type'] == 'number')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <input type="number" name="{{$item['name']}}" class="form-control" autocomplete="off"
                                id="{{$item['name']}}">

                        </div>
                    </div>
                    @endif




                    @empty
                    @endforelse

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>



@endisset

@isset($form_update_upload)


<!-- Modal Edit -->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route($form_update_upload) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Edit</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">


                    @forelse ($form as $item)

                    @if($item['type'] == 'text')
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="{{$item['type']}}" name="{{$item['name']}}" class="form-control"
                                        autocomplete="off" id="{{$item['name']}}">
                                    <label class="form-label">{{$item['label']}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if( $item['type'] == 'select')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2" id="{{$item['name']}}">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)
                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>
                    @endif

                    @if($item['type'] == 'select2')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2_new_modal_edit"
                                id="{{$item['name']}}">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>
                    @endif

                    @if( $item['type'] == 'date')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <input type="date" name="{{$item['name']}}" class="form-control" autocomplete="off"
                                id="{{$item['name']}}">

                        </div>
                    </div>
                    @endif
                    @if( $item['type'] == 'file')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <input type="file" name="{{$item['name']}}" class="form-control" autocomplete="off"
                                id="{{$item['name']}}">

                        </div>
                    </div>
                    @endif




                    @empty
                    @endforelse

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>



@endisset

<script>

</script>