<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{ asset('bsb/images/user.png') }}" width="48" height="48" alt="User" />
        </div>
        @if(Auth::guard('mitra')->user()->is_premium == '1')
        <span class="badge bg-teal">Pr</span>
        @endif
        @if(Auth::guard('mitra')->user()->is_premium != '1')
        <span class="badge bg-orange">Reg</span>
        @endif
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::guard('mitra')->user()->name }}
                <span class="label bg-light-blue">{{Auth::guard('mitra')->user()->name ?? ''}} </span>

            </div>
            <div class="email">{{ Auth::guard('mitra')->user()->description ?? '' }}</div>

            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <!-- <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="separator" class="divider"></li> -->
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                class="material-icons">input</i>Sign Out</a>


                        <form id="logout-form" action="{{ route('mitra.logout') }}" method="POST"
                            style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li id="menuHome">
                <a href="{{ route('dashboard.index')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>



            <li class="{{$tagihanActive ?? ''}}">
                <a href="{{ route('dashboard.tagihan')}}">
                    <i class="material-icons">text_fields</i>
                    <span>Tagihan</span>
                </a>
            </li>
            <li class="{{$pegawaiActive ?? ''}}">
                <a href="{{ route('dashboard.pegawai')}}">
                    <i class="material-icons">people_outline</i>
                    <span>Pegawai</span>
                </a>
            </li>
            <li class="{{$contactActive ?? ''}}">
                <a href="{{ route('dashboard.contact')}}">
                    <i class="material-icons">build</i>
                    <span>Contact Service</span>
                </a>
            </li>
            <li class="{{$jadwalActive ?? ''}}">
                <a href="{{ route('dashboard.jadwal')}}">
                    <i class="material-icons">date_range</i>
                    <span>Jadwal</span>
                </a>
            </li>

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2022 <a href="javascript:void(0);">Bhayu Studio</a>.
        </div>
        <div class="version">
            <b>Version: </b> 1
        </div>
    </div>
    <!-- #Footer -->
</aside>
<!-- #END# Left Sidebar -->