<div class="row">

    <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalEdit"
        class="btn bg-yellow btn-xs waves-float edit_folder"><i class="material-icons">mode_edit</i></button>

    <a href="{{ route('penggajian.salaryDestroy', $data->id) }}" onclick="return confirm('Are you sure?')"
        class="btn bg-red btn-xs waves-effect waves-float" role="button"><i class="material-icons">delete</i></a>

    <a href="{{ route('penggajian.cetak_pdf', $data->id) }}" target="_blank"
        class="btn btn-info waves-effect btn-xs">Download</a>
</div>