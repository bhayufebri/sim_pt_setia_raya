@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreate">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row clearfix">

                    <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                        <label for="NameSurname" class="col-sm-3 control-label">Penempatan</label>
                        <input type="hidden" name="period_id" id="period_id" value="{{$period_id}}">

                        <div class="col-sm-9">
                            <div class="form-line">
                                <select name="partner_id_filter" class="form-control select2_new"
                                    id="partner_id_filter">
                                    <option value="">-- Semua --</option>
                                    @forelse ($partner as $item)

                                    <option value="{{$item['id']}}">
                                        {{$item['name']}}</option>

                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <!-- <select class="js-example-basic-single" name="state">
                            <option value="AL">Alabama</option>

                            <option value="WY">Wyoming</option>
                        </select> -->
                    </div>

                    <button class="btn btn-primary" type="button" id="generate">
                        Generate
                    </button>
                    <div class="preloader pl-size-xs hide" id="loading">
                        <div class="spinner-layer pl-red-grey">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Loading.. -->

                    <button class="btn btn-primary" type="button" id="report" disabled="disabled">
                        Report
                    </button>

                </div>
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Absen</th>
                                <th>Pendapatan</th>
                                <th>Potongan</th>
                                <th>Gaji Bersih</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>





<!-- Modal Edit -->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Edit</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" id="id">
                <div class="row">
                    <div class="col-md-8">
                        <!-- <input type="hidden" name="id" id="id"> -->
                        <input type="hidden" name="user_id" id="user_id">
                        <select class="form-control select2_new" id="variable_id" name="variable_id[]"
                            multiple="multiple">
                            <optgroup label="Penambah">
                                @foreach($variable_penambah as $item)
                                <option value="{{$item['id']}}">{{$item->name}}</option>

                                @endforeach
                            </optgroup>
                            <optgroup label="Pengurang">
                                @foreach($variable_pengurang as $item)
                                <option value="{{$item['id']}}">{{$item->name}}</option>

                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-link waves-effect" id="simpan_variable">SAVE</button>

                    </div>
                </div>

                <form id="submit_variable" method="post" autocomplete="off">
                    <input type="hidden" name="user_id_obj" id="user_id_obj">
                    <div id="result"></div>








            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-link waves-effect" id="save_edit">SAVE</button> -->
                <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                <!-- <button type="button" class="btn btn-link waves-effect" id="simpan">SAVE</button> -->
                </form>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>

        </div>
    </div>
</div>



@endsection
@section('customjs')
<!-- <script src="{{ asset('bsb/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script> -->

<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('penggajian.dataSalary') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                d.partner_id_filter = $('#partner_id_filter').val();
                d.period_id = $('#period_id').val();
                // "period_id": $('#period_id').val(),



            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'user.name',
                name: 'id'
            },
            {
                data: 'jumlah_absen',
                name: 'jumlah_absen',
                defaultContent: ""
            },
            {
                data: 'pendapatan_all',
                name: 'pendapatan_all',
                render: $.fn.dataTable.render.number('.', ',', 0, 'Rp ')
            },
            {
                data: 'potongan_all',
                name: 'potongan_all',
                render: $.fn.dataTable.render.number('.', ',', 0, 'Rp ')
            },
            {
                data: 'total_gaji',
                name: 'total_gaji',
                render: $.fn.dataTable.render.number('.', ',', 0, 'Rp ')
            },

            {
                data: 'action',
                name: 'action'
            },
        ],
        columnDefs: [

            {
                targets: 5,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],


    });

    $('#partner_id_filter').on('change', function() {
        // console.log('aaa');
        var partner_id = $('#partner_id_filter').val();
        if (partner_id) {
            $("#report").prop("disabled", false);
        } else {
            $("#report").prop("disabled", true);
        }

        oTable.ajax.reload();
    });



    let loginForm = document.getElementById("submit_variable");
    loginForm.addEventListener("submit", (e) => {
        e.preventDefault();

        var elements = document.getElementById("submit_variable").elements;
        // console.log(elements);
        var obj = {};
        for (var i = 0; i < elements.length; i++) {
            var item = elements.item(i);
            obj[item.name] = item.value;
            // console.log(item.name + ' adalah ' + item.value);
        }
        var user_id = $('#user_id_obj').val();

        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "salary_id": user_id,
                "obj": obj
            },
            dataType: 'JSON',
            url: "{!! route('penggajian.update_detail') !!}",
            success: function(data) {
                console.log(data);
                if (data == 'success') {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'Error saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                $('#ModalEdit').modal('hide');
                set_gaji(user_id);
                oTable.ajax.reload();


            }
        });


    });





    $(document).on('click', '#generate', function() {

        // $('.edit_folder').on('click', function() {
        // console.log('asdasdas');

        // var data = $(this).val();
        // console.log(data);
        // $('.code_edit').val(data['description_programm']);
        $('#loading').removeClass('hide');
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "partner_id_filter": $('#partner_id_filter').val(),
                // "id": $('#id').val(),
                "period_id": $('#period_id').val(),
                // "potongan_bpjs_kesehatan": $('#potongan_bpjs_kesehatan').val(),
                // "potongan_bpjs_ketenagakerjaan": $('#potongan_bpjs_ketenagakerjaan').val(),
                // "potongan_payroll": $('#potongan_payroll').val(),
            },
            dataType: 'JSON',
            url: "{!! route('penggajian.generateProcess') !!}",
            success: function(data) {
                console.log(data);
                $('#loading').addClass('hide');

                oTable.ajax.reload();
                // $('#ModalEdit').modal('hide');
                if (data == 'success') {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'Error saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }


                // $('#gaji_pokok').val(data['gaji_pokok']);
                // $('#tunjangan_jabatan').val(data['tunjangan_jabatan']);
                // $('#potongan_bpjs_kesehatan').val(data['potongan_bpjs_kesehatan']);
                // $('#potongan_bpjs_ketenagakerjaan').val(data['potongan_bpjs_ketenagakerjaan']);
                // $('#potongan_payroll').val(data['potongan_payroll']);
                // $('#id').val(data['id']);
                // $('.is_active').val(data['is_active']).change();
                // $('.class_id').val(data['id']);
            }
        });

    });
});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    set_gaji(data)
    console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('penggajian.look_salary') !!}",
        success: function(data) {
            console.log(data);

        }
    });

});

$(document).on('click', '#save_edit', function() {

    const trs = document.querySelectorAll('#detail_gaji tr');

    const result = [];

    for (let tr of trs) {
        let th_td = tr.getElementsByTagName('td');
        if (th_td.length == 0) {
            th_td = tr.getElementsByTagName('th');
        }

        let th_td_array = Array.from(th_td); // convert HTMLCollection to an Array
        th_td_array = th_td_array.map(tag => tag.innerText); // get the text of each element
        result.push(th_td_array);
    }

    const trs2 = document.querySelectorAll('#potongan_gaji tr');

    const result2 = [];

    for (let tr2 of trs2) {
        let th_td2 = tr2.getElementsByTagName('td');
        if (th_td2.length == 0) {
            th_td2 = tr2.getElementsByTagName('th');
        }

        let th_td_array2 = Array.from(th_td2); // convert HTMLCollection to an Array
        th_td_array2 = th_td_array2.map(tag => tag.innerText); // get the text of each element
        result2.push(th_td_array2);
    }

    console.log(result);
    console.log(result2);
});




function set_gaji(salary_id) {
    // console.log('set gaji');
    $("#result").empty();
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "salary_id": salary_id
        },
        dataType: 'JSON',
        url: "{!! route('penggajian.look_detail') !!}",
        success: function(data) {
            // console.log(data);

            $('#user_id_obj').val(salary_id);

            for (i = 0; i < data.length; i++) {
                var jum = data[i].jumlah ?? '';
                if (data[i].sifat == 'Penambah') {
                    var text = 'text-primary';
                } else {
                    var text = 'text-danger';

                }
                $("#result").append(
                    '<label class="form-label ' + text + '">' + data[i].description +
                    '</label>' +
                    '<a class="btn bg-red btn-xs waves-effect waves-float delete_component pull-right" onclick="sabar(' +
                    data[i].id +
                    ')"><i class="material-icons">delete</i></a>' +
                    '<div class="form-group ">' +
                    '<div class="form-line">' +
                    '<input type="text" name="' + data[i].id +
                    '" data-role="tagsinput" value="' + jum +
                    '" class="form-control" autocomplete="off">' +

                    '</div>' +
                    '</div>'
                );
            };


        }
    });
};


$(document).on('click', '#simpan_variable', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    // var data = $(this).val();
    var salary_id = $('#user_id_obj').val();
    var variable_id = $('#variable_id').val();


    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "salary_id": salary_id,
            "variable_id": variable_id,

        },
        dataType: 'JSON',
        url: "{!! route('penggajian.store_detail') !!}",
        success: function(data) {
            console.log(data);
            if (data == 'success') {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error saved',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
            set_gaji(salary_id);

        }
    });

});


function sabar(nilai) {

    var deleteMsg = confirm("Do you really want to delete?");
    if (deleteMsg) {
        // $('.edit_folder').on('click', function() {
        // var data = $(this).val();
        // console.log('hanya ' + nilai);
        var user_id = $('#user_id_obj').val();
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "id": nilai
            },
            dataType: 'JSON',
            url: "{!! route('penggajian.destroy_detail') !!}",
            success: function(data) {
                // console.log(data);
                if (data == 'success') {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'Error saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                set_gaji(user_id);


            }
        });
    };

};




$(document).on('click', '#report', function() {
    // javascript: void(document.all.q.value = "hello!");
    var partner_id = $('#partner_id_filter').val()
    var period_id = $('#period_id').val()
    var url = "{{URL::to('penggajian/cetak_report_pdf')}}/" + period_id + "/" + partner_id
    // console.log(url);


    window.location.href = url

    // {{URL::to('restaurants/20')}}
    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    // var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    // $('#loading').removeClass('hide');
    // $.ajax({
    //     type: 'POST',
    //     data: {
    //         "_token": "{!! csrf_token() !!}",
    //         "partner_id_filter": $('#partner_id_filter').val(),
    //         // "id": $('#id').val(),
    //         "period_id": $('#period_id').val(),
    //         // "potongan_bpjs_kesehatan": $('#potongan_bpjs_kesehatan').val(),
    //         // "potongan_bpjs_ketenagakerjaan": $('#potongan_bpjs_ketenagakerjaan').val(),
    //         // "potongan_payroll": $('#potongan_payroll').val(),
    //     },
    //     dataType: 'JSON',
    //     url: "{!! route('penggajian.generateProcess') !!}",
    //     success: function(data) {
    //         console.log(data);
    // $('#loading').addClass('hide');

    // oTable.ajax.reload();
    // // $('#ModalEdit').modal('hide');
    // if (data == 'success') {
    //     Swal.fire({
    //         position: 'top-end',
    //         icon: 'success',
    //         title: 'Your work has been saved',
    //         showConfirmButton: false,
    //         timer: 1500
    //     })
    // } else {
    //     Swal.fire({
    //         position: 'top-end',
    //         icon: 'error',
    //         title: 'Error saved',
    //         showConfirmButton: false,
    //         timer: 1500
    //     })
    // }


    // }
    // });

    // });
});
</script>

@endsection