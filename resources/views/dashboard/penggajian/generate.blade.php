@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreate">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <p>
                    <a class="btn btn-primary" data-toggle="collapse" href="#filter" role="button" aria-expanded="false"
                        aria-controls="filter">
                        Filter
                    </a>

                </p>

                <div class="collapse" id="filter">



                    <div class="row clearfix">

                        <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                            <label for="NameSurname" class="col-sm-3 control-label">Penempatan</label>
                            <div class="col-sm-9">
                                <div class="form-line">
                                    <select name="partner_id_filter" class="form-control select2"
                                        id="partner_id_filter">
                                        <option value="">-- Semua --</option>
                                        @forelse ($partner as $item)

                                        <option value="{{$item['id']}}">
                                            {{$item['name']}}</option>

                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Periode</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>





<!-- Modal Edit -->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Edit</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id" id="id">

                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" name="gaji_pokok" class="form-control" autocomplete="off"
                                    id="gaji_pokok">
                                <label class="form-label">Gaji Pokok</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" name="tunjangan_jabatan" class="form-control" autocomplete="off"
                                    id="tunjangan_jabatan">
                                <label class="form-label">Tunjangan Jabatan</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" name="potongan_bpjs_kesehatan" class="form-control"
                                    autocomplete="off" id="potongan_bpjs_kesehatan">
                                <label class="form-label">Potongan BPJS Kesehatan</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" name="potongan_bpjs_ketenagakerjaan" class="form-control"
                                    autocomplete="off" id="potongan_bpjs_ketenagakerjaan">
                                <label class="form-label">Potongan BPJS Ketenagakerjaan</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="number" name="potongan_payroll" class="form-control" autocomplete="off"
                                    id="potongan_payroll">
                                <label class="form-label">Potongan Payroll</label>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" id="simpan">SAVE</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>

        </div>
    </div>
</div>



@endsection
@section('customjs')

<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('penggajian.dataPeriod') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();
                // d.partner_id = $('#partner_id_filter').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'start',
                name: 'start'
            },

            {
                data: 'action',
                name: 'action'
            },
        ],
        columnDefs: [

            {
                targets: 2,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],


    });

    $('#partner_id_filter').on('change', function() {
        // console.log('aaa');
        oTable.ajax.reload();
    });


    $(document).on('click', '#simpan', function() {

        // $('.edit_folder').on('click', function() {
        // console.log('asdasdas');

        // var data = $(this).val();
        // console.log(data);
        // $('.code_edit').val(data['description_programm']);
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "gaji_pokok": $('#gaji_pokok').val(),
                "id": $('#id').val(),
                "tunjangan_jabatan": $('#tunjangan_jabatan').val(),
                "potongan_bpjs_kesehatan": $('#potongan_bpjs_kesehatan').val(),
                "potongan_bpjs_ketenagakerjaan": $('#potongan_bpjs_ketenagakerjaan').val(),
                "potongan_payroll": $('#potongan_payroll').val(),
            },
            dataType: 'JSON',
            url: "{!! route('penggajian.update_gaji') !!}",
            success: function(data) {
                console.log(data);
                oTable.ajax.reload();
                $('#ModalEdit').modal('hide');
                if (data == 'success') {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'Error saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }


                // $('#gaji_pokok').val(data['gaji_pokok']);
                // $('#tunjangan_jabatan').val(data['tunjangan_jabatan']);
                // $('#potongan_bpjs_kesehatan').val(data['potongan_bpjs_kesehatan']);
                // $('#potongan_bpjs_ketenagakerjaan').val(data['potongan_bpjs_ketenagakerjaan']);
                // $('#potongan_payroll').val(data['potongan_payroll']);
                // $('#id').val(data['id']);
                // $('.is_active').val(data['is_active']).change();
                // $('.class_id').val(data['id']);
            }
        });

    });
});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('penggajian.look_gaji') !!}",
        success: function(data) {
            console.log(data);

            $('#gaji_pokok').val(data['gaji_pokok']);
            $('#tunjangan_jabatan').val(data['tunjangan_jabatan']);
            $('#potongan_bpjs_kesehatan').val(data['potongan_bpjs_kesehatan']);
            $('#potongan_bpjs_ketenagakerjaan').val(data['potongan_bpjs_ketenagakerjaan']);
            $('#potongan_payroll').val(data['potongan_payroll']);
            $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});
</script>

@endsection