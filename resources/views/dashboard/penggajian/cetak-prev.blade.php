<!DOCTYPE html>
<html>

<head>
    <title>Gaji</title>
    <style>
    .header {
        text-align: center;
        margin-top: -40px;
        line-height: 0.2;

    }

    .judul {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        /* page-break-before: always; */


    }

    .judul2 {
        width: 700px;
        /* margin: auto; */
        height: 10px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .ttd {
        width: 300px;
        /* margin: auto; */
        height: auto;
        border: 0px;
        padding: 5px;
        margin-left: 400px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;

    }

    .sub1 {
        font-size: 12px;
        font-weight: bold;

    }

    .uraian1 {
        font-size: 12px;
    }

    .inviseble table {
        border-collapse: collapse;
        /* width: 100%; */
        /* padding-top: -15px; */

    }

    .inviseble th {
        height: 30px;
        text-align: center;
    }

    .inviseble table,
    td,
    th {
        /* border: 1px solid black; */
        height: 14px;
        font-size: 12px;
        /* text-align: left; */
    }

    .inviseble td {
        text-align: left;

    }

    table.border td,
    th {
        border: 1px solid black;
        text-align: center;



    }

    table.no_border td,
    th {
        /* border: 1px solid black; */
        text-align: center;
    }

    div.page_break+div.page_break {
        /* page-break-before: always; */
        page-break-inside: auto;
    }

    .no-print {
        display: none;
    }
    </style>

    <style type="text/css" media="print">
    .no-print {
        display: none;
    }
    </style>
</head>

<body>
    <button onclick="window.print()" class="no-print">Print this page</button>

    <div class="header">
        <img src="{{ public_path('logo.png') }}" width="48" height="48" alt="User">

        <h4>{{$company}}</h4>
        <h4>SLIP GAJI</h4>

    </div>
    <!-- <div class="sub1">1. JUDUL PENELITIAN</div> -->
    <!-- <div class="judul">ks</p>
    </div> -->
    <table class="inviseble">
        <tbody>
            <tr>
                <td>Nama</td>
                <td>: <strong>{{$salary->user->name}}</strong></td>
            </tr>
            <tr>
                <td>Periode Gaji</td>
                <td>: <strong>{{date('d F Y', strtotime($salary->period->start)) ?? ''}} -
                        {{ date('d F Y', strtotime($salary->period->end)) ?? ''}}</strong></td>
            </tr>


        </tbody>
    </table>



    <table class="judul3">
        <tbody>

            <tr>
                <td style="width:10%;"><strong>Pendapatan</strong></td>
                <td style="width:40%;"> </td>
                <!-- <td></td> -->

            </tr>

            @foreach ($detail_penambah as $index => $item)
            <tr>
                <td></td>
                <td>{{$item->description}}</td>
                <td>: {{  number_format($item->jumlah,0,",",".") }}</td>
                <!-- <td></td> -->

            </tr>

            @endforeach

            <tr>
                <td><strong>Potongan</strong></td>
                <td> </td>
                <!-- <td></td> -->

            </tr>

            @foreach ($detail_pengurang as $index => $item)
            <tr>
                <td></td>
                <td>{{$item->description}}</td>
                <td>: {{  number_format($item->jumlah,0,",",".") }}</td>
                <!-- <td></td> -->

            </tr>

            @endforeach
            <tr>
                <td colspan="3">
                    <strong>__________________________________________________________________________________</strong>
                </td>


            </tr>
            <tr>
                <td colspan="2"><strong>Total Gaji</strong></td>
                <td>: <strong>Rp. {{  number_format($salary->total_gaji,0,",",".") }}</strong></td>
                <!-- <td>Penguji</td> -->

            </tr>




        </tbody>
    </table>
    <br />




    <div class=" ttd">
        Kediri, {{date('d F Y', strtotime($salary->created_at)) }}
        <br />
        {{$signature->signature->jabatan}}
        <div
            style="background-image: url('{{ public_path('logo.png') }}'); background-size: 100px 100px; background-repeat: no-repeat; opacity: 0.5;">

            <!-- <img src="{{ public_path('logo.png') }}" width="80" height="80" style="opacity: 0.5;" alt="User"> -->
            <img src="{{ public_path('signature_file/'.$signature->signature->file) }}" width="120" height="120"
                alt="User">
        </div>
        <u><strong>{{$signature->signature->name}}</strong></u>
    </div>








</body>

</html>