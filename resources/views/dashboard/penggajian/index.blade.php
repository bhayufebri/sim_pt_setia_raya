@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreate">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <p>
                    <a class="btn btn-primary" data-toggle="collapse" href="#filter" role="button" aria-expanded="false"
                        aria-controls="filter">
                        Filter
                    </a>

                </p>

                <div class="collapse" id="filter">



                    <div class="row clearfix">

                        <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                            <label for="NameSurname" class="col-sm-3 control-label">Penempatan</label>
                            <div class="col-sm-9">
                                <div class="form-line">
                                    <select name="partner_id_filter" class="form-control select2"
                                        id="partner_id_filter">
                                        <option value="">-- Semua --</option>
                                        @forelse ($partner as $item)

                                        <option value="{{$item['id']}}">
                                            {{$item['name']}}</option>

                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Mitra</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>






<!-- Modal Edit -->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Edit</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <!-- <input type="hidden" name="id" id="id"> -->
                        <input type="hidden" name="user_id" id="user_id">
                        <select class="form-control show-tick" id="variable_id" name="variable_id[]" multiple>
                            <optgroup label="Penambah">
                                @foreach($variable_penambah as $item)
                                <option value="{{$item['id']}}">{{$item->name}}</option>

                                @endforeach
                            </optgroup>
                            <optgroup label="Pengurang">
                                @foreach($variable_pengurang as $item)
                                <option value="{{$item['id']}}">{{$item->name}}</option>

                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-link waves-effect" id="simpan_variable">SAVE</button>

                    </div>
                </div>
                <form id="submit_variable" method="post" autocomplete="off">
                    <input type="hidden" name="user_id_obj" id="user_id_obj">
                    <div id="result"></div>





            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                <!-- <button type="button" class="btn btn-link waves-effect" id="simpan">SAVE</button> -->
                </form>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>

        </div>
    </div>
</div>



@endsection
@section('customjs')
<script src="{{ asset('bsb/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('penggajian.indexData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();
                d.partner_id = $('#partner_id_filter').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'partner.name',
                name: 'partner',
                render: function(data, type, row) {
                    if (data) {
                        return data;
                    } else {
                        return '';
                    }
                }
            },
            {
                data: 'action',
                name: 'action'
            },
        ],
        columnDefs: [

            {
                targets: 3,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 2,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],


    });

    $('#partner_id_filter').on('change', function() {
        // console.log('aaa');
        oTable.ajax.reload();
    });


    $(document).on('click', '#simpan', function() {

        // $('.edit_folder').on('click', function() {
        // console.log('asdasdas');

        // var data = $(this).val();
        // console.log(data);
        // $('.code_edit').val(data['description_programm']);
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "gaji_pokok": $('#gaji_pokok').val(),
                "id": $('#id').val(),
                "tunjangan_jabatan": $('#tunjangan_jabatan').val(),
                "potongan_bpjs_kesehatan": $('#potongan_bpjs_kesehatan').val(),
                "potongan_bpjs_ketenagakerjaan": $('#potongan_bpjs_ketenagakerjaan').val(),
                "potongan_payroll": $('#potongan_payroll').val(),
            },
            dataType: 'JSON',
            url: "{!! route('penggajian.update_gaji') !!}",
            success: function(data) {
                // console.log(data);
                oTable.ajax.reload();
                $('#ModalEdit').modal('hide');
                if (data == 'success') {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'Error saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }


                // $('#gaji_pokok').val(data['gaji_pokok']);
                // $('#tunjangan_jabatan').val(data['tunjangan_jabatan']);
                // $('#potongan_bpjs_kesehatan').val(data['potongan_bpjs_kesehatan']);
                // $('#potongan_bpjs_ketenagakerjaan').val(data['potongan_bpjs_ketenagakerjaan']);
                // $('#potongan_payroll').val(data['potongan_payroll']);
                // $('#id').val(data['id']);
                // $('.is_active').val(data['is_active']).change();
                // $('.class_id').val(data['id']);
            }
        });

    });
});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');


    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('penggajian.look_gaji') !!}",
        success: function(data) {
            // console.log(data);

            $('#gaji_pokok').val(data['gaji_pokok']);
            $('#tunjangan_jabatan').val(data['tunjangan_jabatan']);
            $('#potongan_bpjs_kesehatan').val(data['potongan_bpjs_kesehatan']);
            $('#potongan_bpjs_ketenagakerjaan').val(data['potongan_bpjs_ketenagakerjaan']);
            $('#potongan_payroll').val(data['potongan_payroll']);
            $('#user_id').val(data['id']);
            var user = data['id'];
            set_gaji(user);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});

$(document).on('click', '#simpan_variable', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    // var data = $(this).val();
    var user_id = $('#user_id').val();

    var variable_id = $('#variable_id').val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "user_id": user_id,
            "variable_id": variable_id,
        },
        dataType: 'JSON',
        url: "{!! route('penggajian.store_component') !!}",
        success: function(data) {
            // console.log(data);
            if (data == 'success') {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error saved',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
            set_gaji(user_id);


            // $('#gaji_pokok').val(data['gaji_pokok']);
            // $('#tunjangan_jabatan').val(data['tunjangan_jabatan']);
            // $('#potongan_bpjs_kesehatan').val(data['potongan_bpjs_kesehatan']);
            // $('#potongan_bpjs_ketenagakerjaan').val(data['potongan_bpjs_ketenagakerjaan']);
            // $('#potongan_payroll').val(data['potongan_payroll']);
            // $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});



function set_gaji(id_user_set) {
    // console.log('set gaji');
    $("#result").empty();
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": id_user_set
        },
        dataType: 'JSON',
        url: "{!! route('penggajian.look_component') !!}",
        success: function(data) {
            // console.log(data);

            $('#user_id_obj').val(id_user_set);

            for (i = 0; i < data.length; i++) {
                var jum = data[i].jumlah ?? '';
                if (data[i].sifat == 'Penambah') {
                    var text = 'text-primary';
                } else {
                    var text = 'text-danger';

                }
                $("#result").append(
                    '<label class="form-label ' + text + '">' + data[i].name +
                    '</label>' +
                    '<a class="btn bg-red btn-xs waves-effect waves-float delete_component pull-right" onclick="sabar(' +
                    data[i].id +
                    ')"><i class="material-icons">delete</i></a>' +
                    '<div class="form-group ">' +
                    '<div class="form-line">' +
                    '<input type="text" name="' + data[i].id +
                    '" data-role="tagsinput" value="' + jum +
                    '" class="form-control" autocomplete="off">' +

                    '</div>' +
                    '</div>'
                );
            };


            // $('#gaji_pokok').val(data['gaji_pokok']);
            // $('#tunjangan_jabatan').val(data['tunjangan_jabatan']);
            // $('#potongan_bpjs_kesehatan').val(data['potongan_bpjs_kesehatan']);
            // $('#potongan_bpjs_ketenagakerjaan').val(data['potongan_bpjs_ketenagakerjaan']);
            // $('#potongan_payroll').val(data['potongan_payroll']);
            // $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });
};

let loginForm = document.getElementById("submit_variable");
loginForm.addEventListener("submit", (e) => {
    e.preventDefault();

    var elements = document.getElementById("submit_variable").elements;
    // console.log(elements);
    var obj = {};
    for (var i = 0; i < elements.length; i++) {
        var item = elements.item(i);
        obj[item.name] = item.value;
        // console.log(item.name + ' adalah ' + item.value);
    }
    var user_id = $('#user_id_obj').val();

    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "user_id": user_id,
            "obj": obj
        },
        dataType: 'JSON',
        url: "{!! route('penggajian.sub_obj') !!}",
        success: function(data) {
            // console.log(data);
            if (data == 'success') {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Your work has been saved',
                    showConfirmButton: false,
                    timer: 1500
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Error saved',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
            set_gaji(user_id);


        }
    });

    //   let username = document.getElementById("username");
    //   let password = document.getElementById("password");

    //   if (username.value == "" || password.value == "") {
    //     // throw error
    //   } else {
    //     // perform operation with form input
    //   }
});


// function klik_sub() {
//     console.log('masuk');

//     var elements = document.getElementById("submit_variable").elements;
//     var obj = {};
//     for (var i = 0; i < elements.length; i++) {
//         var item = elements.item(i);
//         obj[item.name] = item.value;
//     }

//     document.getElementById("demo").innerHTML = JSON.stringify(obj);
// }

function sabar(nilai) {

    var deleteMsg = confirm("Do you really want to delete?");
    if (deleteMsg) {
        // $('.edit_folder').on('click', function() {
        // var data = $(this).val();
        // console.log('hanya ' + nilai);
        var user_id = $('#user_id_obj').val();
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "id": nilai
            },
            dataType: 'JSON',
            url: "{!! route('penggajian.destroy_component') !!}",
            success: function(data) {
                // console.log(data);
                if (data == 'success') {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Your work has been saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'Error saved',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
                set_gaji(user_id);


            }
        });
    };

};
</script>

@endsection