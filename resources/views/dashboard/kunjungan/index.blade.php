@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>

            </div>
            <div class="body">
                <div id='calendar'></div>
            </div>
        </div>
    </div>


</div>






@endsection
@section('customjs')

<script>
var calendar = $('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
    },
    // editable: true,
    // events: "{!! route('partner.indexAjax') !!}",
    // events: {
    //     url: "{!! route('partner.indexAjax') !!}",
    //     type: 'POST',
    //     data: {
    //         "_token": "{!! csrf_token() !!}",
    //         "start": "",
    //         "end": "",
    //         "user_id": $('#user_id').val(),
    //         // "xxx": "vvv",
    //     },
    //     error: function() {
    //         alert('there was an error while fetching events!');
    //     },
    //     success: function(data) {
    //         console.log(data)
    //     }
    // },

    events: function(start, end, timezone, callback) {
        jQuery.ajax({
            url: "{!! route('kunjungan.indexAjax') !!}",
            type: 'POST',
            dataType: 'json',
            data: {
                start: start.format(),
                end: end.format(),
                _token: "{!! csrf_token() !!}"
            },
            success: function(doc) {
                // console.log(doc.result);
                var events = [];
                // if (!!doc.result) {
                $.map(doc, function(r) {
                    events.push({
                        id: r.id,
                        title: r.partner.name,
                        start: r.created_at,
                        end: r.created_at,
                        description: r.group ? r.group.title : '',
                        end_time: r.created_at
                    });
                });
                // }

                // $(doc).find('event').each(function() {
                //     events.push({
                //         title: $(this).attr('title'),
                //         start: $(this).attr('start'),
                //         end: $(this).attr('end')
                //     });
                // });

                callback(events);
            }
        });
    },
    displayEventTime: false,
    editable: false,
    // eventRender: function(event, element, view) {
    //     if (event.allDay === 'true') {
    //         event.allDay = true;
    //     } else {
    //         event.allDay = false;
    //     }
    // },
    selectable: true,
    selectHelper: true,

    // eventDrop: function(event, delta) {
    //     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD");
    //     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD");

    //     $.ajax({
    //         url: "{!! route('partner.ajax') !!}",
    //         data: {
    //             title: event.title,
    //             start: start,
    //             end: end,
    //             id: event.id,
    //             type: 'update',
    //             "_token": "{!! csrf_token() !!}",
    //         },
    //         type: "POST",
    //         success: function(response) {
    //             displayMessage("Event Updated Successfully");
    //         }
    //     });
    // },

    eventRender: function(event, element) {
        // console.log(event);
        element.find('.fc-title').append("<br/>" + event.description);


    }

});
</script>

@endsection