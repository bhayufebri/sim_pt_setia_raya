<div class="row">
    <a href="{{ route('ceklist.lihat', $data->id) }}"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i
            class="material-icons">remove_red_eye</i></a>
    <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalEdit"
        class="btn bg-yellow btn-circle waves-effect waves-circle waves-float edit_folder"><i
            class="material-icons">mode_edit</i></button>

    <a href="{{ route('ceklist.destroy', $data->id) }}" onclick="return confirm('Are you sure?')"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i
            class="material-icons">delete</i></a>
</div>