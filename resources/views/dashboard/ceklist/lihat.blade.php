@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>

                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">


                    </li>
                </ul>
            </div>
            <div class="body">

                @forelse ($question as $item)
                @if($item['option'])



                <h2 class="card-inside-title">{{$item['title']}}</h2>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <div class="demo-radio-button">
                                    @foreach(explode(',', $item['option']) as $key => $info)

                                    <input name="{{$item['title']}}" type="radio" id="{{$item['title'].'_'.$key}}" />
                                    <label for="{{$item['title'].'_'.$key}}">{{$info}}</label>
                                    </br>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @else
                <h2 class="card-inside-title">{{$item['title']}}</h2>
                <div class="row clearfix">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <!-- <textarea rows="2" name="{{$item['title']}}" class="form-control no-resize"
                                    placeholder="Jawaban"></textarea> -->

                                <select class="form-control select2">
                                    <option value="">-- select --</option>
                                    <!-- @foreach(explode(',', $item['option']) as $key => $info)
                                            <option value="{{$info}}">{{$info}}</option>
                                            @endforeach -->
                                    <option value="2">Baik Sekali</option>
                                    <option value="1">Baik</option>
                                    <option value="0">Buruk</option>


                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @empty
                @endforelse

            </div>
        </div>
    </div>
</div>






@endsection
@section('customjs')

<script>
$(document).ready(function() {


});
</script>

@endsection