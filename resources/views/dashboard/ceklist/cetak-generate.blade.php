<!DOCTYPE html>
<html>

<head>
    <title>Ceklist</title>
    <style>
    .header {
        text-align: center;
        margin-top: -40px;
        line-height: 0.2;

    }

    .judul {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        /* page-break-before: always; */


    }

    .judul2 {
        width: 700px;
        /* margin: auto; */
        height: 10px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 td {
        border: 1px solid black;

    }

    .ttd {
        width: 300px;
        /* margin: auto; */
        height: auto;
        border: 0px;
        padding: 5px;
        margin-left: 400px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;

    }

    .sub1 {
        font-size: 12px;
        font-weight: bold;

    }

    .uraian1 {
        font-size: 12px;
    }

    .inviseble table {
        border-collapse: collapse;
        /* width: 100%; */
        /* padding-top: -15px; */

    }

    .inviseble th {
        height: 30px;
        text-align: center;
    }

    .inviseble table,
    td,
    th {
        /* border: 1px solid black; */
        height: 14px;
        font-size: 12px;
        /* text-align: left; */
    }

    .inviseble td {
        text-align: left;

    }

    table.border td,
    th {
        border: 1px solid black;
        text-align: center;



    }

    table.no_border td,
    th {
        /* border: 1px solid black; */
        text-align: center;
    }

    div.page_break+div.page_break {
        /* page-break-before: always; */
        page-break-inside: auto;
    }

    .no-print {
        display: none;
    }
    </style>

    <style type="text/css" media="print">
    .no-print {
        display: none;
    }
    </style>
</head>

<body>
    <button onclick="window.print()" class="no-print">Print this page</button>

    <div class="header">

        <h4>{{$company}}</h4>
        <h4>CEKLIST</h4>

    </div>
    <!-- <div class="sub1">1. JUDUL PENELITIAN</div> -->
    <!-- <div class="judul">ks</p>
    </div> -->
    <table class="inviseble">
        <tbody>
            <tr>
                <td>Nama Ceklist</td>
                <td>: <strong>{{$group->title ?? ''}}</strong></td>
            </tr>
            <tr>
                <td>Nama Partner</td>
                <td>: <strong>{{$partner->name ?? ''}}</strong></td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>: <strong>{{$date ?? ''}}</strong></td>
            </tr>



        </tbody>
    </table>



    <table style="border-collapse: collapse; width: 100%;" class="border">
        <tbody>

            <tr>
                <td style="width:3%;"><strong>NO</strong></td>
                <td style="width:30%;"><strong>Jenis Pekerjaan</strong></td>
                @foreach ($master as $index => $item)

                <td><strong>{{ date('d', strtotime($item->created_at))}}</strong></td>

                @endforeach


            </tr>

            @foreach ($group->question as $index => $item)
            <tr>
                <!-- <td></td> -->
                <td>{{$index+1}}</td>
                <td style="text-align:left">{{  $item->title }}</td>
                @foreach ($master as $index2 => $item2)
                <!-- <td>{{$index2}}</td> -->
                @foreach ($item2->answer as $index3 => $item3)
                @if($item->id == $item3->question_id)
                <!-- <td>{{$item3->id}}-{{$item2->id}}-{{$item3->master_id}}</td> -->
                <!-- <td>{{$item3->jawaban}}</td> -->
                @if($item3->jawaban == 2)
                <td>BS</td>
                @elseif($item3->jawaban == 1)
                <td>B</td>
                @else
                <td>Bu</td>
                <!-- <td>{{$item3->jawaban}}</td> -->

                <!-- <td>{{$item2->jawaban == '1' ? 'X' : '-'}}</td>
                            <td>{{$item2->jawaban == '2' ? 'X' : '-'}}</td> -->
                @endif
                @endif
                @endforeach
                @endforeach

            </tr>

            @endforeach





        </tbody>
    </table>
    <br />


    <div class="ttd">
        Kediri, {{date('d F Y', strtotime($group->created_at)) }}
        <br />

        Pengawas
        <br />
        <br />
        <br />
        <br />
        <br />
        <u><strong>{{$user}}</strong></u>
    </div>








</body>

</html>