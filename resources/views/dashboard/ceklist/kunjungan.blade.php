@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>

            </div>
            <div class="body">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-line">
                        <select class="form-control select2_new" id="user_id">
                            <option value="">-- Semua Korlap --</option>
                            @forelse ($user as $item)

                            <option value="{{$item['id']}}">
                                {{$item['name']}}</option>

                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>
                <div id='calendar'></div>
            </div>
        </div>
    </div>


</div>






@endsection
@section('customjs')

<script>
var calendar = $('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
    },
    eventLimit: true,
    views: {
        month: {
            eventLimit: 3
        }
    },

    events: function(start, end, timezone, callback) {
        jQuery.ajax({
            url: "{!! route('ceklist.indexAjax') !!}",
            type: 'POST',
            dataType: 'json',
            data: {
                start: start.format(),
                end: end.format(),
                user_id: $('#user_id').val(),
                _token: "{!! csrf_token() !!}"
            },
            success: function(doc) {
                // console.log(doc.result);
                var events = [];
                // if (!!doc.result) {
                $.map(doc, function(r) {
                    events.push({
                        id: r.id,
                        title: r.partner.name,
                        start: r.created_at,
                        end: r.created_at,
                        description: r.user ? r.user.name : '',
                        end_time: r.created_at
                    });
                });


                callback(events);
            }
        });
    },
    displayEventTime: false,
    editable: false,
    selectable: true,
    selectHelper: true,

    eventRender: function(event, element) {
        // console.log(event);
        element.find('.fc-title').append("<br/>" + event.description);


    }

});

$('#user_id').on('change', function(events) {
    calendar.fullCalendar('refetchEvents');
});
</script>

@endsection