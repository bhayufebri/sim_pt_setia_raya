<div class="row">

    <a href="{{ route('ceklist.cetak_pdf_ceklist', $data->id) }}" target="_blank"><span class="badge bg-cyan"><i
                class="material-icons">file_download</i> {{date('d F Y', strtotime($data->created_at))}}</span></a>

</div>