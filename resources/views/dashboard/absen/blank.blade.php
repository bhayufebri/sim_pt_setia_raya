<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- <title>Sign In | Bootstrap Based Admin Template - Material Design</title> -->
    <title>E PEGAWAI</title>

    <!-- Favicon-->
    <!-- <link rel="icon" href="{{ asset('bsb/favicon.ico') }}" type="image/x-icon"> -->
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">


    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
        type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('bsb/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('bsb/plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('bsb/plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('bsb/css/style.css') }}" rel="stylesheet">

</head>

<body class="four-zero-four">

    <div class="four-zero-four-container">
        <div class="error-code">404</div>
        <div class="error-message">This page doesn't exist</div>
        <!-- <div class="button-place">
            <a href="../../index.html" class="btn btn-default btn-lg waves-effect">GO TO HOMEPAGE</a>
        </div> -->
    </div>



    <!-- Jquery Core Js -->
    <script src="{{ asset('bsb/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('bsb/plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('bsb/plugins/node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('bsb/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ asset('bsb/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('bsb/js/admin.js') }}"></script>
    <script src="{{ asset('bsb/js/pages/examples/sign-in.js') }}"></script>
    <script src="{{ asset('bsb/js/pages/ui/notifications.js') }}"></script>
    <!-- <script src="{{ asset('js/custom.js') }}"></script> -->

    <script>
    // $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    //   $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    $(document).ready(function() {
        $('div.alert').not('.alert-important').fadeIn().delay(3000).fadeOut();
        $('.help-block').fadeIn().delay(1500).fadeOut();
    });
    </script>

    @include('layouts.notification')


</body>

</html>