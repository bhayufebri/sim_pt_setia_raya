<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- <title>Sign In | Bootstrap Based Admin Template - Material Design</title> -->
    <title>E PEGAWAI</title>

    <!-- Favicon-->
    <!-- <link rel="icon" href="{{ asset('bsb/favicon.ico') }}" type="image/x-icon"> -->
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">


    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
        type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('bsb/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('bsb/plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('bsb/plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('bsb/css/style.css') }}" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

</head>

<body class="login-page">

    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">E <b>PEGAWAI</b></a>
            <small>SIM PEGAWAI</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="{{ route('absen.store') }}">
                    @csrf
                    <div class="msg">ABSENSI</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="nip" id="nip" placeholder="NIP" required
                                autofocus>
                            <input type="hidden" name="latitude" id="latitude">
                            <input type="hidden" name="longitude" id="longitude">
                            <input type="hidden" name="slug" id="slug" value="{{$slug}}">
                        </div>

                        @if(Session::has('error'))

                        <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ Session::get('error') }}</strong>
                        </span>
                        {{Session::forget('error')}}


                        @endif

                        @if(Session::has('success'))

                        <span class="invalid-feedback" role="alert">
                            <strong class="text-success">{{ Session::get('success') }}</strong>
                        </span>

                        {{Session::forget('success')}}

                        @endif


                    </div>
                    <!-- <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                name="password" placeholder="Password" required>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div> -->
                    @if(Session::has('image'))
                    <!-- <div class="image-area">
                        <img src="{{ asset('image/') }} {{ Session::get('image') }}"
                            onerror="this.onerror=null;this.src='{{ asset('bsb/images/user.png') }}';"
                            alt="AdminBSB - Profile Image" style="width:70%" />
                    </div> -->


                    <div class="col-xs-12 col-sm-12">
                        <div class="card profile-card">
                            <div class="profile-header">&nbsp;</div>
                            <div class="profile-body">
                                <div class="image-area">
                                    <img src="{{ asset('image/') }}/{{ Session::get('image') }}"
                                        onerror="this.onerror=null;this.src='{{ asset('bsb/images/user.png') }}';"
                                        alt="AdminBSB - Profile Image" style="width:70%" />
                                </div>
                                <div class="content-area">
                                    <h6>{{ Session::get('name') }}</h6>
                                    <!-- <p>Web Software Developer</p> -->
                                    <!-- <p>xyyy</p> -->

                                </div>
                            </div>


                        </div>


                    </div>


                    @else
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <!-- <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} class="filled-in chk-col-pink">
                        <label for="remember">Remember Me</label> -->
                        </div>


                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">SUBMIT</button>
                        </div>
                    </div>
                    @endif
                    {{Session::forget('image')}}
                    {{Session::forget('name')}}




                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <!-- <a href="sign-up.html">Register Now!</a> -->
                        </div>
                        <!-- @if (Route::has('password.request'))

                        <div class="col-xs-6 align-right">
                            <a href="{{ route('password.request') }}">Forgot Password?</a>
                        </div>
                                   
                                @endif -->

                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="ModalDetail" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h4 class="modal-title" id="judul"></h4>
                </div>
                <div class="modal-body">

                    Anda Absen Pulang?
                    <input type="hidden" id="id_absen" value="{{ Session::get('id_absen') }}">
                </div>
                <div class="modal-footer">
                    <button type="button" id="batal" class="btn btn-link waves-effect">BATAL</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">YA</button>
                </div>


            </div>
        </div>
    </div>



    <!-- Jquery Core Js -->
    <script src="{{ asset('bsb/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('bsb/plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('bsb/plugins/node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('bsb/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ asset('bsb/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('bsb/js/admin.js') }}"></script>
    <script src="{{ asset('bsb/js/pages/examples/sign-in.js') }}"></script>
    <script src="{{ asset('bsb/js/pages/ui/notifications.js') }}"></script>
    <!-- <script src="{{ asset('js/custom.js') }}"></script> -->

    <script>
    // $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    //   $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    $(document).ready(function() {
        $('div.alert').not('.alert-important').fadeIn().delay(3000).fadeOut();
        $('.help-block').fadeIn().delay(1500).fadeOut();


        // var x = document.getElementById("demo");

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                // x.innerHTML = "Geolocation is not supported by this browser.";
                console.log('location not found');
            }
        }

        function showPosition(position) {
            $('#latitude').val(position.coords.latitude);
            $('#longitude').val(position.coords.longitude);
        }

        getLocation();

    });



    $(document).on('click', '#ok', function() {



        // var data = $(this).val();
        // console.log(data);
        // $('.code_edit').val(data['description_programm']);
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var nip = $('#nip').val();
        var slug = $('#slug').val();
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "latitude": latitude,
                "longitude": longitude,
                "nip": nip,
                "slug": slug
            },
            dataType: 'JSON',
            url: "{!! route('absen.store') !!}",
            success: function(data) {
                // console.log(data);
                if (data.status == 'error') {
                    alert(data.pesan);
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: data.pesan

                    })
                }

                // $('#name').val(data['name']);
                // $('#description').val(data['description']);
                // $('#id').val(data['id']);
                // $('.is_active').val(data['is_active']).change();
                // $('.class_id').val(data['id']);
            }
        });

    });

    @if(Session::has('validation'))
    $("#ModalDetail").modal('show');

    @php
    Session::forget('validation');
    @endphp
    @endif



    $(document).on('click', '#batal', function() {

        // $('.edit_folder').on('click', function() {
        // console.log('asdasdas');


        var data = $('#id_absen').val();

        // console.log(data);
        // $('.code_edit').val(data['description_programm']);
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "data": data
            },
            dataType: 'JSON',
            url: "{!! route('absen.batal') !!}",
            success: function(data) {
                console.log(data);
                $("#ModalDetail").modal('hide');

                @php
                Session::forget('id_absen');
                @endphp
                location.reload();
                // $('#judul').val(data.order.partner.name);

            }
        });

    });
    </script>

    <!-- @include('layouts.notification') -->


</body>

</html>