@extends('layouts.app')

@section('content')
<style type="text/css">
table.dataTable td {
    font-size: 0.8em;
}
</style>



<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>

            </div>
            <div class="body">
                <form method="POST" action="{{ route('rekap.cetak_pdf') }}">
                    @csrf
                    <div class="row clearfix">

                        <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                            <label for="NameSurname" class="col-sm-3 control-label">Penempatan</label>
                            <div class="col-sm-9">
                                <div class="form-line">

                                    <select name="partner_id_filter" class="form-control select2_new"
                                        id="partner_id_filter">

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                            <label for="NameSurname" class="col-sm-3 control-label">User</label>
                            <div class="col-sm-9">
                                <div class="form-line">


                                    <select name="user_id_filter" class="form-control select2_new" id="user_id_filter">

                                    </select>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row clearfix">

                        <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                            <label for="NameSurname" class="col-sm-3 control-label">Tanggal</label>
                            <div class="col-sm-9">
                                <div class="form-line">
                                    <input class="form-control daterange" name="date_filter" type="text"
                                        id="date_filter">

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                            <label for="NameSurname" class="col-sm-3 control-label">Divisi</label>
                            <div class="col-sm-9">
                                <div class="form-line">
                                    <select name="division_id_filter" class="form-control select2_new"
                                        id="division_id_filter">
                                        <option value="">-- Semua --</option>
                                        @forelse ($division as $item)

                                        <option value="{{$item['id']}}">
                                            {{$item['name']}}</option>

                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">Export Pdf</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <!-- <th>ID</th> -->
                                <th>Nama</th>
                                <th>Partner</th>
                                <th>Divisi</th>
                                <th>Absen</th>
                                <th>Berangkat</th>
                                <th>Pulang</th>
                                <th>Keterangan</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>






@endsection
@section('customjs')

<script>
$(document).ready(function() {


    $('.daterange').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        },
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });




    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('rekap.indexData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                d.partner_id = $('#partner_id_filter').val();
                d.user_id = $('#user_id_filter').val();
                d.date = $('#date_filter').val();
                d.division_id = $('#division_id_filter').val();


            }
        },
        dom: 'Blfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ],
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        order: [
            [2, 'desc']
        ],
        columns: [
            // {
            //     data: 'id',
            //     name: 'id'
            // },
            {
                data: 'user.name',
                name: 'user.name',
                defaultContent: ""

            },
            {
                data: 'partner.name',
                name: 'partner.name',
                defaultContent: ""

            },
            {
                data: 'user.division.name',
                name: 'user.division.name',
                defaultContent: ""

            },
            {
                data: 'start_plot',
                name: 'start_plot'
            },
            {
                data: 'start_absen',
                name: 'start_absen'
            },
            {
                data: 'end_absen',
                name: 'end_absen'
            },
            {
                data: 'lembur',
                name: 'lembur',
                orderable: false,
                searchable: false
            }



        ],


    });

    $('#partner_id_filter').on('change', function() {
        oTable.ajax.reload();
    });
    $('#user_id_filter').on('change', function() {
        oTable.ajax.reload();
    });
    $('#division_id_filter').on('change', function() {
        oTable.ajax.reload();
    });


    $('.daterange').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format(
            'YYYY-MM-DD'));

        oTable.ajax.reload();

    });

    $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        oTable.ajax.reload();

    });


    $('#user_id_filter').select2({
        placeholder: "Ketik nama karyawan...",
        ajax: {
            url: "{!! route('rekap.cariUserAjax') !!}",
            dataType: 'json',
            data: function(params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });

    $('#partner_id_filter').select2({
        placeholder: "Ketik nama partner...",
        ajax: {
            url: "{!! route('rekap.cariPartnerAjax') !!}",
            dataType: 'json',
            data: function(params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });


});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('division.look') !!}",
        success: function(data) {
            // console.log(data);

            $('#name').val(data['name']);
            // $('#description').val(data['description']);
            $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});
</script>

@endsection