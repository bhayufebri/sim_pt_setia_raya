@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreate">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Deskripsi</th>
                                <th>Debet</th>
                                <th>Kredit</th>
                                <th>tanggal</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot align="right">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Penggajian
                </h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example2" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Gaji Untuk</th>
                                <th>Debet</th>
                                <th>Kredit</th>
                                <th>tanggal</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot align="right">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Invoice
                </h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example3" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Setoran Invoice dari</th>
                                <th>Debet</th>
                                <th>Kredit</th>
                                <th>tanggal</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot align="right">
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>






@endsection
@section('customjs')

<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('transaction.indexData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'description',
                name: 'description'
            },
            {
                data: 'debet',
                name: 'debet',
                defaultContent: "",
                render: function(data, type, row, meta) {
                    if (data) {

                        return 'Rp. ' + parseInt(data).toLocaleString('id-ID');
                    } else {
                        return '';
                    }
                }
            },
            {
                data: 'kredit',
                name: 'kredit',
                defaultContent: "",
                render: function(data, type, row, meta) {
                    if (data) {

                        return 'Rp. ' + parseInt(data).toLocaleString('id-ID');
                    } else {
                        return '';
                    }
                }

            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'action',
                name: 'action'
            },



        ],
        columnDefs: [

            {
                targets: 3,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 2,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 5,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],
        "footerCallback": function(row, data, start, end, display) {
            var api = this.api();
            nb_cols = api.columns().nodes().length;
            var j = 2;
            while (j < 4) {
                var pageTotal = api
                    .column(j, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function(a, b) {
                        // console.log(Number(b));
                        return Number(a) + Number(b);
                    }, 0);
                // Update footer
                $(api.column(j).footer()).html('Rp. ' + parseInt(pageTotal).toLocaleString(
                    'id-ID'));
                j++;
            }
            $(api.column(0).footer()).html('Total');
            // converting to interger to find total
            // var intVal = function(i) {
            //     return typeof i === 'string' ?
            //         i.replace(/[\$,]/g, '').replace('Rp', '').replace(' ', '').replace('.',
            //             '') * 1 :
            //         typeof i === 'number' ?
            //         i : 0;
            // };

        }


    });


    var oTable2 = $('#example2').DataTable({

        processing: true,
        serverSide: true,

        ajax: {
            url: "{!! route('transaction.dataPenggajian') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],

        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'user.name',
                name: 'user.name',
                defaultContent: ""

            },
            {
                data: 'debet',
                name: 'debet',
                defaultContent: ""
            },
            {
                data: 'kredit',
                name: 'kredit',
                defaultContent: "",
                render: function(data, type, row, meta) {
                    return 'Rp. ' + parseInt(data).toLocaleString('id-ID');
                }
            },
            {
                data: 'created_at',
                name: 'created_at'
            },




        ],
        columnDefs: [

            {
                targets: 3,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 2,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],
        "footerCallback": function(row, data, start, end, display) {
            var api = this.api();
            nb_cols = api.columns().nodes().length;
            var j = 3;
            // while (j < nb_cols) {
            var pageTotal = api
                .column(j, {
                    page: 'current'
                })
                .data()
                .reduce(function(a, b) {
                    // console.log(Number(b));
                    return Number(a) + Number(b);
                }, 0);
            // Update footer
            $(api.column(0).footer()).html('Total');
            $(api.column(j).footer()).html('Rp. ' + parseInt(pageTotal).toLocaleString(
                'id-ID'));
            // j++;
            // }
            // converting to interger to find total
            // var intVal = function(i) {
            //     return typeof i === 'string' ?
            //         i.replace(/[\$,]/g, '').replace('Rp', '').replace(' ', '').replace('.',
            //             '') * 1 :
            //         typeof i === 'number' ?
            //         i : 0;
            // };

        }


    });
    console.log(oTable2.column(3).data().sum());


    var oTable3 = $('#example3').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('transaction.dataInvoice') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'partner.name',
                name: 'partner.name'
            },
            {
                data: 'debet',
                name: 'debet',
                render: function(data, type, row, meta) {
                    return 'Rp. ' + parseInt(data).toLocaleString('id-ID');
                }
            },
            {
                data: 'kredit',
                name: 'kredit'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },




        ],
        columnDefs: [

            {
                targets: 3,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 2,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],
        "footerCallback": function(row, data, start, end, display) {
            var api = this.api();
            nb_cols = api.columns().nodes().length;
            var j = 2;
            // while (j < nb_cols) {
            var pageTotal = api
                .column(j, {
                    page: 'current'
                })
                .data()
                .reduce(function(a, b) {
                    // console.log(Number(b));
                    return Number(a) + Number(b);
                }, 0);
            // Update footer
            $(api.column(0).footer()).html('Total');
            $(api.column(j).footer()).html('Rp. ' + parseInt(pageTotal).toLocaleString(
                'id-ID'));


        }



    });





});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('transaction.look') !!}",
        success: function(data) {
            // console.log(data);

            $('#jumlah').val(data['jumlah']);
            $('#description').val(data['description']);
            $('#id').val(data['id']);
            $('#jenis').val(data['jenis']).change();
            $('#is_cash').val(data['is_cash']).change();
            // $('.class_id').val(data['id']);
        }
    });

});
</script>

@endsection