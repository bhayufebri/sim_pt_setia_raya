@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>

                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">


                    </li>
                </ul>
            </div>
            <div class="body">
                <form action="{{ route('record.store') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <input type="hidden" name="group_id" value="{{$group_id}}">
                    <h2 class="card-inside-title">Perusahaan Mitra</h2>
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <div class="demo-radio-button">


                                        <select name="partner_id" class="form-control select2_new" id="partner_id">
                                            <option value="">-- select --</option>
                                            @forelse ($partner as $item)

                                            <option value="{{$item['id']}}">
                                                {{$item['name']}}</option>

                                            @empty
                                            @endforelse

                                        </select>
                                    </div>
                                </div>
                                <div id="loading" class="hide">

                                    <div class="preloader pl-size-xl">
                                        <div class="spinner-layer">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <span class="badge hide" id="isi"></span>
                            </div>

                        </div>
                    </div>


                    <div id="content" class="hide">

                        @forelse ($question as $item)




                        <h2 class="card-inside-title">{{$item['title']}}</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <div class="demo-radio-button">
                                            <!-- @foreach(explode(',', $item['option']) as $key => $info)

                                        <input name="{{$item['title']}}" type="radio" id="{{$item['title'].'_'.$key}}"
                                            class="with-gap" />
                                        <label for="{{$item['title'].'_'.$key}}">{{$info}}</label>
                                        </br>
                                        @endforeach -->

                                            <select name="{{$item['id']}}" class="form-control select2_new jawaban">
                                                <option value="">-- select --</option>
                                                <!-- @foreach(explode(',', $item['option']) as $key => $info)
                                            <option value="{{$info}}">{{$info}}</option>
                                            @endforeach -->
                                                <option value="2">Baik Sekali</option>
                                                <option value="1">Baik</option>
                                                <option value="0">Buruk</option>


                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group form_{{$item['id']}}">
                                        <div class="form-line">
                                            <input type="text" name="note_{{$item['id']}}"
                                                class="form-control no-resize" placeholder="Tindaklanjut" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        @empty
                        @endforelse

                        <h2 class="card-inside-title">Area Pekerjaan</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="area_pekerjaan" class="form-control no-resize"
                                            placeholder="Area" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h2 class="card-inside-title">Permasalahan</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="masalah" class="form-control no-resize"
                                            placeholder="Masalah" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="hidden" id="tindaklanjut">
                        <h2 class="card-inside-title">Tindaklanjut</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="tindaklanjut" class="form-control no-resize"
                                            placeholder="Tindaklanjut" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->

                        <h2 class="card-inside-title">Catatan Tambahan</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea id="ckeditor" name="catatan" class="form-control no-resize"
                                            placeholder="Jawaban"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect">SAVE</button>
                    </div>

                </form>

                <input type="hidden" name="latitude" id="latitude">
                <input type="hidden" name="longitude" id="longitude">
            </div>
        </div>
    </div>
</div>






@endsection
@section('customjs')

<script>
$(document).ready(function() {
    $(function() {
        //CKEditor
        CKEDITOR.replace('ckeditor');

    })


    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            // x.innerHTML = "Geolocation is not supported by this browser.";
            console.log('location not found');
        }
    }

    function showPosition(position) {
        $('#latitude').val(position.coords.latitude);
        $('#longitude').val(position.coords.longitude);
        // console.log(position.coords.latitude);

    }

    getLocation();


    $(document).on('change', '#partner_id', function() {
        // console.log('pindah');
        $('#isi').addClass('hide');
        $('#content').addClass('hide');
        $('#loading').removeClass('hide');
        $('#isi').removeClass('bg-cyan');
        $('#isi').removeClass('bg-orange');

        var lat = $('#latitude').val();
        var long = $('#longitude').val();
        var partner_id = $('#partner_id').val()
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "lat": lat,
                "long": long,
                "partner_id": partner_id
            },
            dataType: 'JSON',
            url: "{!! route('record.cek_lokasi') !!}",
            success: function(data) {
                // console.log(data);
                $('#isi').removeClass('hide');

                document.getElementById("isi").innerHTML = data;
                $('#loading').addClass('hide');

                if (data == "Lokasi valid") {
                    // console.log('masuk');
                    $('#isi').addClass('bg-cyan');

                    $('#content').removeClass('hide');
                } else {
                    $('#isi').addClass('bg-orange');
                };



            }
        });
    });

});


// $(document).on('change', '.jawaban', function() {

//     // $('.edit_folder').on('click', function() {
//     // console.log('asdasdas');

//     // console.log($(this).prop("name"));
//     var name = $(this).prop("name");

//     // var data = $(this).val();
//     var jab = $.map($('.jawaban'), function(el) {
//         return el.value;
//     });

//     // var jab = $('.jawaban').val();
//     // console.log(jab.filter(function(number) {
//     //     return number === "0";
//     // }));
//     var nilai = jab.filter(function(number) {
//         return number === "0";
//     });
//     if (nilai.length > 0) {
//         // $("#tindaklanjut").removeClass("hidden").addClass("");
//         $(".form_" + name).removeClass("hidden").addClass("");
//     };
//     // console.log(jab);


// });
</script>

@endsection