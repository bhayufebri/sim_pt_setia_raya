<!DOCTYPE html>
<html>

<head>
    <title>Ceklist</title>
    <style>
    .header {
        text-align: center;
        margin-top: -40px;
        line-height: 0.2;

    }

    .judul {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        /* page-break-before: always; */


    }

    .judul2 {
        width: 700px;
        /* margin: auto; */
        height: 10px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    /* .judul3 td {
        border: 1px solid black;

    } */

    .ttd {
        width: 300px;
        /* margin: auto; */
        height: auto;
        border: 0px;
        padding: 5px;
        margin-left: 400px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;

    }

    .sub1 {
        font-size: 12px;
        font-weight: bold;

    }

    .uraian1 {
        font-size: 12px;
    }

    .inviseble table {
        border-collapse: collapse;
        /* width: 100%; */
        /* padding-top: -15px; */

    }

    .inviseble th {
        height: 30px;
        text-align: center;
    }

    .inviseble table,
    td,
    th {
        /* border: 1px solid black; */
        height: 14px;
        font-size: 12px;
        /* text-align: left; */
    }

    .inviseble td {
        text-align: left;

    }

    table.border td,
    th {
        border: 1px solid black;
        text-align: center;



    }

    table.no_border td,
    th {
        /* border: 1px solid black; */
        text-align: center;
    }

    div.page_break+div.page_break {
        /* page-break-before: always; */
        page-break-inside: auto;
    }

    .no-print {
        display: none;
    }
    </style>

    <style type="text/css" media="print">
    .no-print {
        display: none;
    }
    </style>
</head>

<body>
    <button onclick="window.print()" class="no-print">Print this page</button>

    <div class="header">

        <h4>{{$company}}</h4>
        <h4>CEKLIST</h4>

    </div>
    <!-- <div class="sub1">1. JUDUL PENELITIAN</div> -->
    <!-- <div class="judul">ks</p>
    </div> -->
    <table class="inviseble">
        <tbody>
            <tr>
                <td>Nama Ceklist</td>
                <td>: <strong>{{$master->group->title}}</strong></td>
            </tr>
            <tr>
                <td>Nama Partner</td>
                <td>: <strong>{{$master->partner->name}}</strong></td>
            </tr>
            <tr>
                <td>Tanggal Input</td>
                <td>: <strong>{{date('d F Y H:i', strtotime($master->created_at))}}</strong></td>
            </tr>


        </tbody>
    </table>



    <table style="border-collapse: collapse; width: 100%;" class="border">
        <tbody>

            <tr>
                <td style="width:10%;"><strong>NO</strong></td>
                <td style="width:40%;"><strong>Jenis Pekerjaan</strong></td>
                <td><strong>Buruk</strong></td>
                <td><strong>Baik</strong></td>
                <td><strong>Baik Sekali</strong></td>

            </tr>

            @foreach ($master->group->question as $index => $item)
            <tr>
                <!-- <td></td> -->
                <td>{{$index+1}}</td>
                <td style="text-align:left">{{  $item->title }}</td>
                @foreach ($answer as $index2 => $item2)
                @if($item->id == $item2->question_id)
                <td>{{$item2->jawaban == '0' ? 'X' : '-'}}</td>
                <td>{{$item2->jawaban == '1' ? 'X' : '-'}}</td>
                <td>{{$item2->jawaban == '2' ? 'X' : '-'}}</td>
                @if($item2->note != null)
                @php
                $note = $item2->note;
                @endphp
                @endif
                @endif
                @endforeach

            </tr>
            @isset($note)
            <td colspan="5" style="text-align:left">Tindak Lanjut : {{$note}}</td>

            @endisset
            @php
            unset($note);
            @endphp


            @endforeach





        </tbody>
    </table>
    <br />

    <table class="judul3">
        <tbody>
            <tr>
                <td style="width:20%;"><strong>Area Pekerjaan</strong></td>
                <td> : {{ $master->area_pekerjaan }}</td>
                <!-- <td>Penguji</td> -->

            </tr>
            <tr>
                <td style="width:10%;"><strong>Permasalahan</strong></td>
                <td> : {{ $master->masalah }}</td>
                <!-- <td>Penguji</td> -->

            </tr>
            <!-- @isset($master->tindaklanjut)
            <tr>
                <td style="width:10%;"><strong>Tindak Lanjut</strong></td>
                <td> : {{ $master->tindaklanjut }}</td>

            </tr>
            @endisset -->

            <tr>
                <td style="width:10%;"><strong>Catatan Tambahan</strong></td>
                <td> {!! $master->catatan_panjang !!}</td>
                <!-- <td>Penguji</td> -->

            </tr>




        </tbody>
    </table>





    <table style="border-collapse: collapse; width: 100%;" class="no_border">
        <tbody>
            <tr>
                <!-- <td style="width:20%;"><img src="{{ public_path('logo.png') }}" width="80" height="80"
                        alt="User"></td> -->
                <td style="width:50%;">
                    <div
                        style="background-image: url('{{ public_path('logo.png') }}'); background-size: 100px 100px; background-repeat: no-repeat; opacity: 0.5; background-position: center;">
                        <h4>Direktur Operasional</h4>
                        <br /><br /><br />
                        <br /><br /><br />
                        _____________________
                    </div>

                </td>


                <td>

                    Kediri, {{date('d F Y', strtotime($master->created_at)) }}
                    <br />
                    {{$signature->signature->jabatan ?? ''}}
                    <div>

                        @isset($signature->signature)
                        <img src="{{{public_path('signature_file/'.$signature->signature->file)}}}" width="120"
                            height="120" alt="User">
                        @endisset

                    </div>

                    @isset($signature->signature)

                    <!-- <u><strong>{{$master->user->name ?? ''}}</strong></u> -->
                    <u><strong>{{{$signature->signature->name}}}</strong></u>
                    @endisset



                </td>

            </tr>


        </tbody>
    </table>








</body>

</html>