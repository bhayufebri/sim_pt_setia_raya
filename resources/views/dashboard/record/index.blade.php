@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreate">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <!-- <th>ID</th> -->
                                <th>Nama</th>
                                <th>Division</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    List
                </h2>

            </div>
            <div class="body">

                <form action="{{ route('record.generateRekap') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="row clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <!-- <input type="text" class="form-control" placeholder="Email Address"> -->
                                    <select class="form-control select2_new" name="group_id" id="group_id">
                                        <option value="">-- Pilih Ceklist --</option>
                                        @foreach($group as $key => $item)
                                        <option value="{{$item->id}}">{{$item->title}}</option>
                                        @endforeach
                                        <!-- <option value="2">Baik Sekali</option>
                                        <option value="1">Baik</option>
                                        <option value="0">Buruk</option> -->


                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <select class="form-control select2_new" name="partner_id" id="partner_id">
                                        <option value="">-- Pilih Partner --</option>
                                        @foreach($partner as $key => $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                        <!-- <option value="2">Baik Sekali</option>
                                        <option value="1">Baik</option>
                                        <option value="0">Buruk</option> -->


                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control daterange" name="date_range"
                                        id="date_filter">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <!-- <input type="checkbox" id="remember_me_4" class="filled-in">
                            <label for="remember_me_4">Remember Me</label> -->
                            <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">Generate</button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example2" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Group</th>
                                <th>Partner</th>
                                <th>Action</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection
@section('customjs')

<script>
$(document).ready(function() {

    $('.daterange').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        },
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('record.indexData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [
            // {
            //     data: 'id',
            //     name: 'id'
            // },
            {
                data: 'title',
                name: 'title'
            },
            {
                data: 'division.name',
                name: 'division.name',
                defaultContent: ""

            },




        ],


    });

    var oTable2 = $('#example2').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('record.masterData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                d.group_id = $('#group_id').val();
                d.partner_id = $('#partner_id').val();
                d.date = $('#date_filter').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'created_at',
                name: 'created_at',
                render: function(data, type, row) {
                    if (type === "sort" || type === "type") {
                        return data;
                    }
                    return moment(data).format("DD MM YYYY HH:mm");
                }
            },
            {
                data: 'group.title',
                name: 'group.title',
                defaultContent: ""

            },
            {
                data: 'partner.name',
                name: 'partner.name',
                defaultContent: ""

            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searchable: false
            },




        ],


    });

    $('#group_id').on('change', function() {
        oTable2.ajax.reload();
    });
    $('#partner_id').on('change', function() {
        oTable2.ajax.reload();
    });
    $('.daterange').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD hh:mm:ss') + ' - ' + picker.endDate.format(
            'YYYY-MM-DD hh:mm:ss'));

        oTable2.ajax.reload();

    });

    $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        oTable2.ajax.reload();

    });


});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('ceklist.look') !!}",
        success: function(data) {
            console.log(data);

            $('#title').val(data['title']);
            // $('#description').val(data['description']);
            $('#id').val(data['id']);
            $('#is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});
</script>

@endsection