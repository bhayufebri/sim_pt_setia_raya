<div class="row">



    <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalEditPermintaan"
        class="btn bg-yellow btn-circle waves-effect waves-circle waves-float edit_permintaan"><i
            class="material-icons">mode_edit</i></button>
    <a href="{{ route('permintaanbarang.cetak', $data->id) }}" target="_blank"><i
            class="material-icons">file_download</i></a>

    <!-- <a href="{{ route('barangmitra.destroy', $data->id) }}" onclick="return confirm('Are you sure?')"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i
            class="material-icons">delete</i></a> -->

    <button type="button" value="{{$data->id}}"
        class="btn bg-blue btn-circle waves-effect waves-circle waves-float setujui"><i
            class="material-icons">done</i></button>

    <button type="button" value="{{$data->id}}"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float tolak"><i
            class="material-icons">clear</i></button>

</div>