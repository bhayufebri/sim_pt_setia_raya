@extends('layouts.app')

@section('content')
<style type="text/css">
table.dataTable td {
    font-size: 0.8em;
}
</style>




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreate">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-3">
                        <p>
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button"
                                aria-expanded="false" aria-controls="collapseExample">
                                Updoad data
                            </a>

                        </p>
                    </div>
                    <div class="col-md-7">
                    </div>
                    <div class="col-md-2">

                        <p>
                            <a class="btn btn-primary" data-toggle="collapse" href="#filter" role="button"
                                aria-expanded="false" aria-controls="filter">
                                Filter
                            </a>

                        </p>
                    </div>
                </div>
                <div class="collapse" id="collapseExample">
                    <form action="{{ route('member_sdm.import') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}


                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <a href="{{ route('member.download_fileex') }}" type="download"
                                    class="btn btn-primary btn-md m-l-15 waves-effect">download contoh format</a>

                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" class="form-control" name="file">
                                        <label class="form-label">Berkas</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

                                <button type="submit" class="btn btn-primary btn-md m-l-15 waves-effect">Upload</button>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="collapse" id="filter">

                    <form action="{{ route('member_sdm.export_pdf') }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}

                        <div class="row clearfix">

                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">Penempatan</label>
                                <div class="col-md-9">
                                    <div class="form-line">
                                        <select name="partner_id_filter" class="form-control select2"
                                            id="partner_id_filter">
                                            <option value="">-- Semua --</option>
                                            @forelse ($partner as $item)

                                            <option value="{{$item['id']}}">
                                                {{$item['name']}}</option>

                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">Divisi</label>
                                <div class="col-sm-9">
                                    <div class="form-line">
                                        <select name="division_id_filter" class="form-control select2"
                                            id="division_id_filter">
                                            <option value="">-- Semua --</option>
                                            @forelse ($division as $item)

                                            <option value="{{$item['id']}}">
                                                {{$item['name']}}</option>

                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-9">
                                    <div class="form-line">
                                        <select name="status_id_filter" class="form-control select2"
                                            id="status_id_filter">
                                            <option value="">-- Semua --</option>

                                            <option value="1">Aktif</option>
                                            <option value="-">Non Aktif</option>


                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-9">
                                    <div class="form-line">
                                        <select name="status_id_filter_new" class="form-control select2"
                                            id="status_id_filter_new">
                                            <option value="">-- Semua --</option>
                                            @forelse ($status as $item)

                                            <option value="{{$item['id']}}">
                                                {{$item['name']}}</option>

                                            @empty
                                            @endforelse


                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn bg-orange waves-effect">
                                <i class="material-icons">picture_as_pdf</i>
                            </button>

                        </div>
                    </form>



                </div>

                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Penempatan</th>
                                <th>Keaktifan</th>
                                <th>Status</th>

                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>

            </div>


        </div>
    </div>


</div>









@endsection
@section('customjs')
<script src="{{ asset('bsb/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('member_sdm.indexData') !!}",
            type: 'GET',
            data: function(d) {
                console.log(d);
                d.partner_id = $('#partner_id_filter').val();
                d.division_id = $('#division_id_filter').val();
                d.status = $('#status_id_filter').val();
                d.status_new = $('#status_id_filter_new').val();

                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'role.name',
                name: 'role.name',
                defaultContent: "",

            },
            {
                data: 'partner.name',
                name: 'partner.name',
                defaultContent: "",

            },
            {
                data: 'is_active',
                name: 'is_active',
                defaultContent: "",

            },
            {
                data: 'status.name',
                name: 'status.name',
                defaultContent: "",

            },
            {
                data: 'action',
                name: 'action'
            },



        ],
        columnDefs: [


            {
                targets: 3,
                className: 'text-center',
                "orderable": false
            },
            {
                targets: 4,
                className: 'text-center',
                "orderable": false
            },
            {
                targets: 5,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 6,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 7,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],


    });

    $('#partner_id_filter').on('change', function() {
        // console.log('aaa');
        oTable.ajax.reload();
    });
    $('#division_id_filter').on('change', function() {
        // console.log('aaa');
        oTable.ajax.reload();
    });
    $('#status_id_filter').on('change', function() {
        // console.log('aaa');
        oTable.ajax.reload();
    });
    $('#status_id_filter_new').on('change', function() {
        oTable.ajax.reload();
    });

});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('member_sdm.look') !!}",
        success: function(data) {
            // console.log(data);

            $('#name').val(data['name']);
            $('#email').val(data['email']);
            // $('#role_id').val(data['role_id']).change();
            // $('#company_id').val(data['company_id']).change();
            $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});



$(document).ready(function() {

    // $(document).on('change', '#division_id', function() {
    $('#division_id_change').on('change', function() {

        // $('.edit_folder').on('click', function() {
        // console.log('asdasdas');

        var data = $(this).val();
        console.log(data);
        // $('.code_edit').val(data['description_programm']);
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "data": data
            },
            dataType: 'JSON',
            url: "{!! route('member_sdm.look_position') !!}",
            success: function(data) {
                // $('#position_id_change').val(null).select2().trigger('change');
                $("#position_id_change").empty();
                $("#position_id_change").selectpicker('refresh');

                // console.log('xxxx');
                $("#position_id_change").append("<option value=''>-- Select --</option>");
                for (i = 0; i < data.length; i++) {
                    $("#position_id_change").append("<option value='" +
                        data[i]['id'] +
                        "'>" +
                        data[i]['name'] +
                        "</option>");
                    $("#position_id_change").selectpicker('refresh');

                };
                // console.log(mySelect);

                // $.each(data, function(i, item) {
                //     var mySelect = $('#position_id_change').append($('<option>', {
                //         value: item.id,
                //         text: item.name
                //     }));

                //     mySelect.selectpicker('refresh');
                // });


                // $('#name').val(data['name']);
                // $('#email').val(data['email']);
                // $('#role_id').val(data['role_id']).change();
                // $('#company_id').val(data['company_id']).change();
                // $('#id').val(data['id']);
                // $('.is_active').val(data['is_active']).change();
                // $('.class_id').val(data['id']);
            }
        });

    });
});




$(document).on('click', '#export_pdf', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    // var data = $(this).val();
    // console.log(data);
    var division_id = $('#division_id_filter').val();
    var partner_id = $('#partner_id_filter').val();
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "division_id": division_id,
            "partner_id": partner_id
        },
        dataType: 'JSON',
        url: "{!! route('member_sdm.export_pdf') !!}",
        success: function(data) {
            console.log(data);

            // $('#name').val(data['name']);
            // $('#email').val(data['email']);
            // // $('#role_id').val(data['role_id']).change();
            // // $('#company_id').val(data['company_id']).change();
            // $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    }).done(downloadFile);;

});

function downloadFile(response) {
    console.log('xx', response);
    var blob = new Blob([response], {
        type: 'application/pdf'
    })
    var url = URL.createObjectURL(blob);
    // location.assign(url);
}
</script>

@endsection