<div class="row">

    <!-- <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalEdit"
        class="btn bg-yellow btn-circle waves-effect waves-circle waves-float edit_folder"><i
            class="material-icons">mode_edit</i></button> -->

    <a type="button" href="{{ route('member_sdm.edit', $data->id) }}"
        class="btn bg-yellow btn-circle waves-effect waves-circle waves-float"><i
            class="material-icons">mode_edit</i></a>

    <a href="{{ route('member_sdm.destroy', $data->id) }}" onclick="return confirm('Are you sure?')"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i
            class="material-icons">delete</i></a>
</div>