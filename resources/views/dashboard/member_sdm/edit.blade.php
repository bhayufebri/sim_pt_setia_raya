@extends('layouts.app')

@section('content')

<div class="row clearfix">


    <div class="col-xs-12 col-sm-3">
        <div class="card profile-card">
            <div class="profile-header">&nbsp;</div>
            <div class="profile-body">
                <div class="image-area">
                    <img src="{{ asset('image/'.$user->image) }}"
                        onerror="this.onerror=null;this.src='{{ asset('bsb/images/user.png') }}';"
                        alt="AdminBSB - Profile Image" style="width:70%" />
                </div>
                <div class="content-area">
                    <h3>{{$user->name}}</h3>
                    <!-- <p>Web Software Developer</p> -->
                    <p>Member</p>
                </div>
            </div>
            <form action="{{ route('member_sdm.update_image') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="profile-footer">
                    <ul>
                        <li>
                            <input type="file" class="form-control" name="image">
                            <input type="hidden" class="form-control" name="id" value="{{$id}}">
                        </li>

                    </ul>
                    <button type="submit" class="btn btn-primary btn-lg waves-effect btn-block">Upload Foto</button>
                </div>
            </form>

        </div>


    </div>

    <div class="col-xs-12 col-sm-9">
        <div class="card">
            <div class="body">
                <div>
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active"><a href="#profile_settings" aria-controls="settings"
                                role="tab" data-toggle="tab"><i class="material-icons">accessibility</i>Profile</a></li>
                        <li role="presentation"><a href="#status" aria-controls="settings" role="tab"
                                data-toggle="tab"><i class="material-icons">contact_mail</i> Status Peg.</a></li>
                        <li role="presentation"><a href="#penempatan" aria-controls="settings" role="tab"
                                data-toggle="tab"><i class="material-icons">person_pin_circle</i>Penempatan</a></li>
                        <li role="presentation"><a href="#berkas" aria-controls="settings" role="tab"
                                data-toggle="tab"><i class="material-icons">attach_file</i>Berkas</a></li>
                        <li role="presentation"><a href="#change_password_settings" aria-controls="settings" role="tab"
                                data-toggle="tab"><i class="material-icons">vpn_key</i>Password</a></li>
                    </ul>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="profile_settings">
                            <form class="form-horizontal" action="{{ route('member_sdm.update') }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <input type="hidden" class="form-control" name="id" value="{{$id}}">

                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Name</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" placeholder="Name"
                                                value="{{$user->name}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">NIP</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nip" placeholder="NIP"
                                                value="{{$user->nip}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Nama Ibu</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nama_ibu_kandung"
                                                placeholder="Nama Ibu Kandung" value="{{$user->nama_ibu_kandung}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">No Handphone</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="no_handpone"
                                                placeholder="081XXXX" value="{{$user->no_handpone}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Alamat</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="alamat" placeholder="Alamat"
                                                value="{{$user->alamat}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">NIK</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nik" placeholder="NIK"
                                                value="{{$user->nik}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tempat Lahir</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="tempat_lahir"
                                                placeholder="Tempat Lahir" value="{{$user->tempat_lahir}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tanggal Lahir</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="date" class="form-control" name="tanggal_lahir"
                                                placeholder="Tanggal Lahir" value="{{$user->tanggal_lahir}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tanggal Masuk</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="date" class="form-control" name="awal_masuk"
                                                placeholder="Tanggal Masuk" value="{{$user->awal_masuk}}">
                                        </div>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Divisi</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select name="division_id" class="form-control select2">
                                                <option value="">-- Select --</option>
                                                @forelse ($division as $item)

                                                <option value="{{$item['id']}}"
                                                    {{$user->division_id == $item['id'] ? 'selected' : ''}}>
                                                    {{$item['name']}}</option>

                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Posisi</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select name="position_id" class="form-control select2">
                                                <option value="">-- Select --</option>
                                                @forelse ($position as $item)

                                                <option value="{{$item['id']}}"
                                                    {{$user->position_id == $item['id'] ? 'selected' : ''}}>
                                                    {{$item['name']}}</option>

                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Keaktifan</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select name="is_active" class="form-control select2">
                                                <option value="">-- Select --</option>


                                                <option value="1" {{$user->is_active == 1 ? 'selected' : ''}}>
                                                    AKTIF</option>
                                                <option value="0" {{$user->is_active == 0 ? 'selected' : ''}}>
                                                    NON AKTIF</option>


                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">BPJS
                                        Kesehatan</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select name="is_bpjskes" class="form-control select2">
                                                <option value="">-- Select --</option>


                                                <option value="1" {{$user->is_bpjskes == 1 ? 'selected' : ''}}>
                                                    Ada</option>
                                                <option value="0" {{$user->is_bpjskes == 0 ? 'selected' : ''}}>
                                                    Tidak Ada</option>


                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">BPJS
                                        TK</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select name="is_bpjstk" class="form-control select2">
                                                <option value="">-- Select --</option>


                                                <option value="1" {{$user->is_bpjstk == 1 ? 'selected' : ''}}>
                                                    Ada</option>
                                                <option value="0" {{$user->is_bpjstk == 0 ? 'selected' : ''}}>
                                                    Tidak Ada</option>


                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Metode Pembayaran</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <select name="is_payroll" class="form-control select2">
                                                <option value="">-- Select --</option>


                                                <option value="1" {{$user->is_payroll == 1 ? 'selected' : ''}}>
                                                    Payroll</option>
                                                <option value="0" {{$user->is_payroll == 0 ? 'selected' : ''}}>
                                                    Manual</option>


                                            </select>
                                        </div>
                                    </div>
                                </div>




                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-danger">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="change_password_settings">
                            <form class="form-horizontal" action="{{ route('member_sdm.update_password') }}"
                                method="post">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <input type="hidden" class="form-control" name="id" value="{{$id}}">

                                <div class="form-group">
                                    <label for="NewPassword" class="col-sm-3 control-label">New Password</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="NewPassword" name="password"
                                                placeholder="New Password" required>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-danger">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div role="tabpanel" class="tab-pane fade in" id="berkas">

                            <form action="{{ route('member_sdm.store_file') }}" method="post"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <input type="hidden" class="form-control" name="id" value="{{$id}}">

                                <div class="row clearfix">
                                    <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="file" class="form-control" name="berkas">
                                                <label class="form-label">Berkas</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="name">
                                                <label class="form-label">Keterangan</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">

                                        <button type="submit"
                                            class="btn btn-primary btn-lg m-l-15 waves-effect">Upload</button>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <!-- <table id="example" class="display" style="width:100%"> -->
                                <table id="example" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nama</th>


                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>


                                        @foreach ($file as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <!-- <td><h6>{{ $item->tanggal }}</h6></td> -->

                                            <td><a href="{{ URL::to('berkas/', $item->berkas) }}" target="_blank">
                                                    {{ $item->name }}</a>

                                            </td>



                                            <td>

                                                <form action="{{ route('member_sdm.delete_file') }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('POST') }}

                                                    <input type="hidden" name="user_id" value="{{ $id }}" />
                                                    <input type="hidden" name="id" value="{{ $item->id }}" />

                                                    <button
                                                        class="btn btn-danger btn-circle waves-effect waves-circle waves-float delete-confirm"
                                                        type="submit" data-toggle="tooltip" data-placement="top"
                                                        onclick="return confirm('Are you sure?')" title="Hapus"><i
                                                            class="material-icons">delete</i></button>
                                                    <button type="button" value="{{$item->id}}"
                                                        class="btn btn-primary view" data-toggle="modal"
                                                        data-target="#myModal">
                                                        Preview
                                                    </button>
                                                </form>

                                            </td>



                                        </tr>
                                        @endforeach

                                    </tbody>

                                </table>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade in" id="status">
                            <form class="form-horizontal" action="{{ route('member_sdm.update_pkwt') }}"
                                enctype="multipart/form-data" method="post">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <input type="hidden" class="form-control" name="id" value="{{$id}}">

                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Status</label>
                                    <div class="col-sm-6">
                                        <div class="form-line">
                                            <select name="status_id" class="form-control select2">
                                                <option value="">-- Select --</option>
                                                @forelse ($status as $item)

                                                <option value="{{$item['id']}}"
                                                    {{$user->status_id == $item['id'] ? 'selected' : ''}}>
                                                    {{$item['name']}}</option>

                                                @empty
                                                @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="tanggal_pkwt"
                                                placeholder="Tanggal PKWT"
                                                value="{{date('d F Y', strtotime($user->tanggal_pkwt)) ?? ''}}"
                                                disabled>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tanggal PKWT</label>
                                    <div class="col-sm-4">
                                        <div class="form-line">
                                            <input type="date" class="form-control" name="start_date"
                                                placeholder="Tanggal PKWT" value="" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-line">
                                            <input type="date" class="form-control" name="end_date"
                                                placeholder="Tanggal PKWT" value="" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Berkas</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="file" class="form-control" name="berkas">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-danger">SUBMIT</button>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive">
                                <!-- <table id="example" class="display" style="width:100%"> -->
                                <table id="example3" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nama</th>
                                            <th>Tanggal</th>


                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>


                                        @foreach ($contract as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <!-- <td><h6>{{ $item->tanggal }}</h6></td> -->

                                            <td><a href="{{ URL::to('contracts/', $item->berkas) }}" target="_blank">
                                                    {{ $item->description }}</a>

                                            </td>
                                            <td>
                                                {{date('d F Y', strtotime($item->start_date)) ?? ''}} -
                                                {{date('d F Y', strtotime($item->end_date)) ?? ''}}

                                            </td>



                                            <td>

                                                <form action="{{ route('member_sdm.delete_pkwt') }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('POST') }}

                                                    <input type="hidden" name="user_id" value="{{ $id }}" />
                                                    <input type="hidden" name="id" value="{{ $item->id }}" />

                                                    <button
                                                        class="btn btn-danger btn-circle waves-effect waves-circle waves-float delete-confirm"
                                                        type="submit" data-toggle="tooltip" data-placement="top"
                                                        onclick="return confirm('Are you sure?')" title="Hapus"><i
                                                            class="material-icons">delete</i></button>
                                                    <button type="button" value="{{$item->id}}"
                                                        class="btn btn-primary pkwt" data-toggle="modal"
                                                        data-target="#pkwt">
                                                        Preview
                                                    </button>
                                                </form>

                                            </td>



                                        </tr>
                                        @endforeach

                                    </tbody>

                                </table>
                            </div>
                        </div>


                        <div role="tabpanel" class="tab-pane fade in" id="penempatan">

                            <form action="{{ route('member_sdm.update_partner') }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <input type="hidden" class="form-control" name="id" value="{{$id}}">

                                <div class="row clearfix">

                                    <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <!-- <input type="text" class="form-control" name="name"> -->
                                                <select name="partner_id" class="form-control select2">
                                                    <option value="">-- Select --</option>
                                                    @forelse ($partner as $item)

                                                    <option value="{{$item['id']}}"
                                                        {{$user->partner_id == $item['id'] ? 'selected' : ''}}>
                                                        {{$item['name']}}</option>

                                                    @empty
                                                    @endforelse
                                                </select>
                                                <label class="form-label">Keterangan</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="date" class="form-control" name="tanggal">
                                                <label class="form-label">Tanggal</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">

                                        <button type="submit"
                                            class="btn btn-primary btn-lg m-l-15 waves-effect">Ok</button>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <!-- <table id="example" class="display" style="width:100%"> -->
                                <table id="example2" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nama</th>


                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>


                                        @foreach ($history as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <!-- <td><h6>{{ $item->tanggal }}</h6></td> -->

                                            <td>
                                                {{ $item->description }}
                                                ({{date('d F Y', strtotime($item->tanggal))}})
                                            </td>


                                            <td>
                                                <form action="{{ route('member_sdm.delete_history') }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('POST') }}

                                                    <input type="hidden" name="user_id" value="{{ $id }}" />
                                                    <input type="hidden" name="id" value="{{ $item->id }}" />

                                                    <button
                                                        class="btn btn-danger btn-circle waves-effect waves-circle waves-float delete-confirm"
                                                        type="submit" data-toggle="tooltip" data-placement="top"
                                                        onclick="return confirm('Are you sure?')" title="Hapus"><i
                                                            class="material-icons">delete</i></button>
                                                </form>


                                            </td>



                                        </tr>
                                        @endforeach

                                    </tbody>

                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- The Modal -->
<div class="modal" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">berkas</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <embed frameborder="0" src="" width="100%" height="400px" id="file">
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="pkwt">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">berkas</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <embed frameborder="0" src="" width="100%" height="400px" id="pkwt_view">
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>

@endsection
@section('customjs')

<script>
$(document).ready(function() {

    var user_id = '{!! json_encode($id) !!}';

    // console.log(user_id);
    $('#example').DataTable();
    $('#example2').DataTable();
    $('#example3').DataTable();


});



$(document).on('click', '.view', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');
    var url = "{{URL::to('berkas')}}/";

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('member_sdm.look_file') !!}",
        success: function(data) {
            // console.log(data);

            $('#file').attr('src', url + data['berkas']);

            // $('#name').val(data['name']);
            // $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});


$(document).on('click', '.pkwt', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');
    var url = "{{URL::to('contracts')}}/";

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('member_sdm.look_pkwt') !!}",
        success: function(data) {
            console.log(data);

            $('#pkwt_view').attr('src', url + data['berkas']);

            // $('#name').val(data['name']);
            // $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});
</script>

@endsection