@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Partner</th>
                                <th>Pesan</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                                <th>Aksi</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>






@endsection
@section('customjs')

<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('service.indexData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'partner.name',
                name: 'partner.name',
                defaultContent: ""
            },
            {
                data: 'description',
                name: 'description'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'status',
                name: 'status'
            },
            {
                data: 'action',
                name: 'action'
            },



        ],
        columnDefs: [

            {
                targets: 5,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },


        ],


    });


    $(document).on('click', '.proses', function() {
        var datax = $(this).val();
        // console.log(datax);
        Swal.fire({
            title: 'Anda Yakin',
            text: "Memproses ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, saya setuju!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    data: {
                        "_token": "{!! csrf_token() !!}",
                        "data": datax
                    },
                    dataType: 'JSON',
                    url: "{!! route('service.proses') !!}",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            'Berhasil!',
                            'Data telah diproses.',
                            'success'
                        )
                        oTable.ajax.reload();

                    }
                });

            }
        })

        // $('.code_edit').val(data['description_programm']);


    });
    $(document).on('click', '.selesai', function() {
        var datax = $(this).val();
        // console.log(datax);
        Swal.fire({
            title: 'Anda Yakin',
            text: "Menyelesai ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, saya setuju!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    data: {
                        "_token": "{!! csrf_token() !!}",
                        "data": datax
                    },
                    dataType: 'JSON',
                    url: "{!! route('service.selesai') !!}",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            'Berhasil!',
                            'Data telah diselesaikan.',
                            'success'
                        )
                        oTable.ajax.reload();

                    }
                });

            }
        })

        // $('.code_edit').val(data['description_programm']);


    });


});
</script>

@endsection