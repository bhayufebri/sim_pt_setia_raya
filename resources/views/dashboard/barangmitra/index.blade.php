@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreateCustom">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Mitra</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                                <th>Action</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

<div class="modal fade bd-example-modal-lg" id="ModalCreateCustom" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="form_validation6" action="{{ route($form_action) }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Tambah</h4>
                </div>
                <div class="modal-body">



                    @forelse ($form as $item)


                    @if( $item['type'] == 'select')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control ">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>
                    @elseif($item['type'] == 'date')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="date" name="{{$item['name']}}" class="form-control" autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif($item['type'] == 'select2')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2_new">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>
                    @elseif($item['type'] == 'daterange')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="{{$item['name']}}" class="form-control daterange"
                                        autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="{{$item['type']}}" name="{{$item['name']}}" class="form-control"
                                        autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>
                                </div>
                            </div>
                        </div>
                    </div>


                    @endif


                    @empty
                    @endforelse



                    <div class="row clearfix">
                        <!-- <div class="col-sm-2">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="kode[]" class="form-control" autocomplete="off">
                                    <label class="form-label">Kode Barang</label>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-sm-5">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <!-- <input type="text" name="nama_barang[]" class="form-control" autocomplete="off"> -->
                                    <label class="form-label">Nama Barang</label>

                                    <select class="form-control select2_new" id="partner_id_filter">
                                        <option value="">-- Nama Barang --</option>
                                        @forelse ($itemdata as $item)

                                        <option value="{{$item['id']}}">
                                            {{$item['name']}}</option>

                                        @empty
                                        @endforelse
                                    </select>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-1">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="number" class="form-control" autocomplete="off" id="jumlah">
                                    <label class="form-label">Jumlah</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" class="form-control" autocomplete="off" id="keterangan">
                                    <label class="form-label">Keterangan</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn bg-light-blue waves-effect" id="rowAdder">
                                <i class="material-icons">note_add</i>
                            </button>
                        </div>
                    </div>


                    <div id="newinput"></div>



                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="ModalDetail" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="judul"></h4>
            </div>
            <div class="modal-body">

                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>

                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>




@endsection
@section('customjs')

<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('barangmitra.indexData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'partner.name',
                name: 'partner.name',
                defaultContent: ""
            },
            {
                data: 'created_at',
                name: 'created_at',
                defaultContent: ""
            },
            {
                data: 'status',
                name: 'status'
            },

            {
                data: 'action',
                name: 'action'
            }



        ],
        columnDefs: [

            {
                targets: 3,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 4,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],


    });


});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('ceklist.look') !!}",
        success: function(data) {
            console.log(data);

            $('#title').val(data['title']);
            // $('#description').val(data['description']);
            $('#id').val(data['id']);
            $('#is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});


$("#rowAdder").click(function() {

    var nama_barang = $('#partner_id_filter').val();
    var jumlah = $('#jumlah').val();
    var keterangan = $('#keterangan').val();
    console.log(nama_barang);

    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": nama_barang
        },
        dataType: 'JSON',
        url: "{!! route('barangmitra.look_item') !!}",
        success: function(data) {
            // console.log(data);

            // $('#title').val(data['title']);
            // $('#description').val(data['description']);
            // $('#id').val(data['id']);
            // $('#is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);

            newRowAdd = '<div class="row clearfix" id="row">' +
                '<div class="col-sm-2">' +
                '<div class="form-group form-float">' +
                '<div class="form-line focused">' +
                '<input type="text" name="kode[]" class="form-control" value="' + data['code'] +
                '" autocomplete="off" readonly>' +
                '<label class="form-label">Kode</label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-3">' +
                '<div class="form-group form-float">' +
                '<div class="form-line focused">' +
                '<input type="text" name="nama_barang[]" class="form-control" value="' + data[
                    'name'] + '" autocomplete="off" readonly>' +
                '<label class="form-label">Nama barang</label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-1">' +
                '<div class="form-group form-float">' +
                '<div class="form-line focused">' +
                '<input type="text" name="satuan[]" class="form-control" value="' + data[
                    'satuan'] + '" autocomplete="off" readonly>' +
                '<label class="form-label">Satuan</label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-1">' +
                '<div class="form-group form-float">' +
                '<div class="form-line focused">' +
                '<input type="number" name="jumlah[]" class="form-control" value="' + jumlah +
                '" autocomplete="off" readonly>' +
                '<label class="form-label">Jumlah</label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-3">' +
                '<div class="form-group form-float">' +
                '<div class="form-line focused">' +
                '<input type="text" name="keterangan[]" class="form-control"  value="' +
                keterangan + '" autocomplete="off" readonly>' +
                '<label class="form-label">Keterangan</label>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-sm-1">' +
                '<button type="button" class="btn bg-danger waves-effect" id="DeleteRow">' +
                '<i class="material-icons">delete</i>' +
                '</button>' +
                '</div>' +
                '</div>';

            $('#newinput').append(newRowAdd);
        }
    });



});

$(document).on("click", "#DeleteRow", function() {
    $(this).parents("#row").remove();
});





$(document).on('click', '.edit_detail', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');
    $('#myTable').empty();

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('barangmitra.look') !!}",
        success: function(data) {
            // console.log(data);

            // $('#judul').val(data.order.partner.name);
            document.getElementById("judul").innerHTML = data.order.partner.name;
            for (i = 0; i < data.detail.length; i++) {
                $('#myTable').append(
                    '<tr>' +
                    '<th scope="row">' + (i + 1) + '</th>' +
                    '<td>' + data.detail[i].kode + '</td>' +
                    '<td>' + data.detail[i].nama + '</td>' +
                    '<td>' + data.detail[i].jumlah + '</td>' +
                    '<td>' + data.detail[i].satuan + '</td>' +
                    '<td><a href="barangmitra/destroy_detail/' + data.detail[i].id +
                    '" onclick="return confirm(`Are you sure?`)" class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i class="material-icons">delete</i></a></td>' +
                    '</tr>'
                );

            }
        }
    });

});
</script>

@endsection