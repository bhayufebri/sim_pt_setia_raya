<div class="row">



    <!-- <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalEdit"
        class="btn bg-yellow btn-circle waves-effect waves-circle waves-float edit_folder"><i
            class="material-icons">mode_edit</i></button> -->
    @if($data->status != 1)
    <a href="{{ route('barangmitra.destroy', $data->id) }}" onclick="return confirm('Are you sure?')"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i
            class="material-icons">delete</i></a>
    @endif

    <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalDetail"
        class="btn bg-lime btn-circle waves-effect waves-circle waves-float edit_detail"><i
            class="material-icons">list</i></button>

</div>