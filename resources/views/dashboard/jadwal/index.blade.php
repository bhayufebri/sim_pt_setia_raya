@extends('layouts.app')

@section('content')
<style type="text/css">
#map {
    height: 280px;
}
</style>

<div class="row clearfix">





    <div class="col-xs-12 col-md-12">
        <div class="card">
            <div class="body">
                <div>




                    <div role="tabpanel" class="tab-pane fade in" id="change_password_settings">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-line">
                                    <select class="form-control select2_new" id="partner_id">
                                        <option value="">-- Pilih Partner --</option>
                                        @forelse ($partner as $item)

                                        <option value="{{$item['id']}}">
                                            {{$item['name']}}</option>

                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-line">
                                    <select class="form-control select2_new" id="user_id">
                                        <option value="">-- Pilih user --</option>

                                    </select>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-sm-1 control-label">Judul</label>
                            <div class="col-sm-3">
                                <div class="form-line">
                                    <input type="text" class="form-control" name="title" id="title" placeholder="Uraian"
                                        value="" required>
                                </div>
                            </div>

                            <label class="col-sm-2 control-label">Waktu</label>
                            <div class="col-sm-2">
                                <div class="form-line">
                                    <input type="time" class="form-control" name="start_time" id="start_time">
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-line">
                                    <input type="time" class="form-control" name="end_time" id="end_time">
                                </div>
                            </div>
                            <div class="col-sm-2">

                                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"
                                    id="set">Simpan</button>
                            </div>
                        </div>



                        <!-- <span class="badge bg-cyan">99 Unread</span> -->

                        <!-- <div id="tgl"></div> -->
                        <input type="text" id="tags" class="form-control" data-role="tagsinput">


                        <div id='calendar'></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- <div id="calendarModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span
                        class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Judul</label>
                    <div class="col-sm-9">
                        <div class="form-line">
                            <input type="text" class="form-control" name="title" id="title" placeholder="Uraian"
                                value="" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Waktu</label>
                    <div class="col-sm-5">
                        <div class="form-line">
                            <input type="time" class="form-control" name="start_time" id="start_time">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-line">
                            <input type="time" class="form-control" name="end_time" id="end_time">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="set">SUBMIT</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->

<div id="editTime" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span
                        class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal Absen</label>
                    <div class="col-sm-9">
                        <div class="form-line">
                            <input type="text" class="form-control daterange" id="date_absen" placeholder="Uraian">
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="batal_pulang">Batal
                    Pulang</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="ubah">SUBMIT</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('customjs')
<!-- <script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8MuHKzO_b90qb09_Nmity9rCcN-Jlrpg&callback=initMap&v=weekly"
    defer></script> -->

<!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB8MuHKzO_b90qb09_Nmity9rCcN-Jlrpg&callback=initMap">
</script> -->

<!-- <script>
// $(document).ready(function() {

// Initialize and add the map
function initMap() {
    // The location of Uluru
    const uluru = {
        lat: -25.344,
        lng: 131.031
    };
    // The map, centered at Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 4,
        center: uluru,
    });
    // The marker, positioned at Uluru
    const marker = new google.maps.Marker({
        position: uluru,
        map: map,
    });
}

window.initMap = initMap;

// });
</script> -->
<!-- <script src="{{ asset('bsb/plugins/multi-select/js/jquery.multi-select.js') }}"></script> -->
<!-- <script src="{{ asset('bsb/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script> -->



<script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
    integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>

<script>
$(document).ready(function() {


    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        eventLimit: true,
        views: {
            month: {
                eventLimit: 3
            }
        },
        // editable: true,
        // events: "{!! route('partner.indexAjax') !!}",
        // events: {
        //     url: "{!! route('partner.indexAjax') !!}",
        //     type: 'POST',
        //     data: {
        //         "_token": "{!! csrf_token() !!}",
        //         "start": "",
        //         "end": "",
        //         "user_id": $('#user_id').val(),
        //         // "xxx": "vvv",
        //     },
        //     error: function() {
        //         alert('there was an error while fetching events!');
        //     },
        //     success: function(data) {
        //         console.log(data)
        //     }
        // },

        events: function(start, end, timezone, callback) {
            jQuery.ajax({
                url: "{!! route('createAbsen.indexAjax') !!}",
                type: 'POST',
                dataType: 'json',
                data: {
                    start: start.format(),
                    end: end.format(),
                    _token: "{!! csrf_token() !!}",
                    user_id: $('#user_id').val(),

                },
                success: function(doc) {
                    // console.log(doc);
                    var events = [];
                    // if (!!doc.result) {
                    $.map(doc, function(r) {
                        events.push({
                            id: r.id,
                            title: r.user ? r.user.name : '' + ' (' + r
                                .title +
                                ')',
                            start: r.start,
                            end: r.end,
                            description: r.start_plot,
                            end_time: r.end_plot
                        });
                    });
                    // }

                    // $(doc).find('event').each(function() {
                    //     events.push({
                    //         title: $(this).attr('title'),
                    //         start: $(this).attr('start'),
                    //         end: $(this).attr('end')
                    //     });
                    // });

                    callback(events);
                }
            });
        },
        displayEventTime: false,
        editable: false,
        // eventRender: function(event, element, view) {
        //     if (event.allDay === 'true') {
        //         event.allDay = true;
        //     } else {
        //         event.allDay = false;
        //     }
        // },
        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
            if ($('#user_id').val()) {
                // $('#calendarModal').modal();
                // var title = prompt('Event Title:');

                // var start = $.fullCalendar.formatDate(start, "Y-MM-DD 00:00:00");
                // var end = $.fullCalendar.formatDate(end, "Y-MM-DD 00:00:00");
                var start2 = $.fullCalendar.formatDate(start, "D MMMM Y");
                var end2 = $.fullCalendar.formatDate(end, "Y-MM-DD 00:00:00");
                // console.log([start, end]);


                // $("#tgl").append(
                //     '<span class="badge bg-cyan">' + start2 +
                //     '</span><br/>'
                // );
                $('#tags').tagsinput('add', start2);
                // $('#tags').tagsinput('refresh');

                // event.setDates(start, end, allDay);
                // calendar.fullCalendar('event', start, end);
                // $.map(start, function(r) {
                //     console.log(start);
                //     events.push({
                //         // id: r.id,
                //         title: 'mak',
                //         start: r.start,
                //         end: r.end
                //     });
                // });






                // $(document).on('click', '#set', function() {
                // $('#set').click(function() {
                $('#set').unbind('click').bind('click', function(e) {
                    // if (title) {
                    var title = $('#title').val();
                    var start_time = $('#start_time').val();
                    var end_time = $('#end_time').val();
                    var user_id = $('#user_id').val();
                    var partner_id = $('#partner_id').val();
                    var tags = $('#tags').val();

                    // console.log(tags);
                    if (title) {


                        $.ajax({
                            url: "{!! route('createAbsen.ajax') !!}",
                            data: {
                                title: title,
                                // start: start,
                                // end: end,
                                tags: tags,
                                start_time: start_time,
                                end_time: end_time,
                                user_id: user_id,
                                partner_id: partner_id,
                                type: 'add',
                                "_token": "{!! csrf_token() !!}"
                            },
                            type: "POST",
                            success: function(data, start, end) {
                                $('#tags').tagsinput('removeAll');

                                // console.log(data);
                                displayMessage("Event Created Successfully");
                                // location.reload();
                                // calendar.fullCalendar('removeEvents');
                                $('#title').val('');
                                $('#start_time').val('');
                                $('#end_time').val('');
                                // $('#user_id').val('');
                                // calendar.fullCalendar('removeEvents');
                                // calendar.fullCalendar('updateEvent', events);
                                // calendar.fullCalendar('unselect');
                                // calendar.fullCalendar('removeEvents', start);
                                // calendar.fullCalendar('removeEvents', end);
                                calendar.fullCalendar('refetchEvents');


                                // calendar.fullCalendar('renderEvent', {
                                //     id: data.id,
                                //     title: title,
                                //     start: start,
                                //     end: end,
                                //     allDay: allDay
                                // }, true);

                                // calendar.fullCalendar('unselect');
                                calendar.fullCalendar('unselect');
                                // $('#title').val('');
                            }
                        });
                    }

                    // }

                    calendar.fullCalendar('unselect');

                });
            } else {
                displayMessage("User not selected");
            }
        },
        // eventDrop: function(event, delta) {
        //     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD");
        //     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD");

        //     $.ajax({
        //         url: "{!! route('partner.ajax') !!}",
        //         data: {
        //             title: event.title,
        //             start: start,
        //             end: end,
        //             id: event.id,
        //             type: 'update',
        //             "_token": "{!! csrf_token() !!}",
        //         },
        //         type: "POST",
        //         success: function(response) {
        //             displayMessage("Event Updated Successfully");
        //         }
        //     });
        // },
        eventClick: function(event) {
            var deleteMsg = confirm("Do you really want to delete?");
            if (deleteMsg) {
                $.ajax({
                    type: "POST",
                    url: "{!! route('createAbsen.ajax') !!}",
                    data: {
                        id: event.id,
                        type: 'delete',
                        "_token": "{!! csrf_token() !!}",
                    },
                    success: function(response) {
                        calendar.fullCalendar('removeEvents', event.id);
                        displayMessage("Event Deleted Successfully");
                    }
                });
            }
        },
        eventRender: function(event, element) {
            // console.log(event);
            element.find('.fc-title').append("<br/>" + new Date(event.description).getHours() +
                ':' + new Date(event.description).getMinutes() +
                '-' + new Date(event.end_time).getHours() + ':' + new Date(event.end_time)
                .getMinutes());
            element.find('.fc-title').attr('data-toggle', 'tooltip');

            element.find('.fc-title').attr('title', event.title);

        }

    });

    $('#user_id').on('change', function(events) {
        // console.log('aaa');
        // oTable.ajax.reload();
        // console.log($('#user_id').val());
        calendar.fullCalendar('refetchEvents');
        // $('#user_id_import').val($('#user_id').val());
        // $('#calendar').fullCalendar('removeEvents');
        // $('#calendar').fullCalendar('addEventSource', events);
        // $('#calendar').fullCalendar('rerenderEvents');

    });

    $('#partner_id').on('change', function(events) {
        $("#user_id").empty().trigger('change')
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "partner_id": $('#partner_id').val()
            },
            dataType: 'JSON',
            url: "{!! route('createAbsen.lookUser') !!}",
            success: function(data) {
                console.log(data);
                // $("#user_id").select2('data', {
                //     id: 'newID',
                //     text: 'newText'
                // });
                const tambah = {
                    id: '',
                    text: '---pilih---'
                };
                data.unshift(tambah);

                $('#user_id').select2({
                    data
                });

            }
        });





        // calendar2.fullCalendar('refetchEvents');
    });



    $('[data-toggle="tooltip"]').tooltip();
});



function displayMessage(message) {
    toastr.success(message, 'Event');
}
</script>

@endsection