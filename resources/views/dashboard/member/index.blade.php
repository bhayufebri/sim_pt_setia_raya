@extends('layouts.app')

@section('content')
<style type="text/css">
table.dataTable td {
    font-size: 0.8em;
}
</style>


<script src="https://cdn.jsdelivr.net/npm/xlsx@0.18.0/dist/xlsx.full.min.js"></script>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreate">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-md-3">
                        <p>
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button"
                                aria-expanded="false" aria-controls="collapseExample">
                                Updoad data
                            </a>

                        </p>
                    </div>
                    <div class="col-md-7">
                    </div>
                    <div class="col-md-2">

                        <p>
                            <a class="btn btn-primary" data-toggle="collapse" href="#filter" role="button"
                                aria-expanded="false" aria-controls="filter">
                                Filter
                            </a>

                        </p>
                    </div>
                </div>
                <div class="collapse" id="collapseExample">

                    <!-- <div class="w3-col w3-half">
                        <div class="form-group">
                            <input id="fileupload" type=file name="files[]">
                        </div>
                    </div> -->



                    <form action="{{ route('member.import') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}


                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                <a href="{{ route('member.download_fileex') }}" type="download"
                                    class="btn btn-primary btn-md m-l-15 waves-effect">download contoh format</a>

                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="file" class="form-control" id="fileupload" name="file">
                                        <label class="form-label">Berkas</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

                                <button type="submit" class="btn btn-primary btn-md m-l-15 waves-effect">Upload</button>
                            </div>
                        </div>
                    </form>
                    <h2>Cek Data</h2>
                    <div class="table-responsive">

                        <table id="tblItems" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Position_id</th>
                                    <th>Division_id</th>
                                    <th>Status_id</th>
                                    <th>Partner_id</th>
                                    <th>Tanggal_partner</th>
                                    <th>is_bpjskes</th>
                                    <th>is_bpjstk</th>
                                    <th>awal_masuk</th>
                                    <th>Nama Ibu Kandung</th>
                                    <th>no handphone</th>
                                    <th>Alamat</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Tempat Lahir</th>
                                    <th>NIK</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                </div>
                <div class="collapse" id="filter">

                    <form action="{{ route('member.export_pdf') }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}

                        <div class="row clearfix">

                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">Penempatan</label>
                                <div class="col-md-9">
                                    <div class="form-line">
                                        <select name="partner_id_filter" class="form-control select2"
                                            id="partner_id_filter">
                                            <option value="">-- Semua --</option>
                                            @forelse ($partner as $item)

                                            <option value="{{$item['id']}}">
                                                {{$item['name']}}</option>

                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">Divisi</label>
                                <div class="col-sm-9">
                                    <div class="form-line">
                                        <select name="division_id_filter" class="form-control select2"
                                            id="division_id_filter">
                                            <option value="">-- Semua --</option>
                                            @forelse ($division as $item)

                                            <option value="{{$item['id']}}">
                                                {{$item['name']}}</option>

                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">Keaktifan</label>
                                <div class="col-sm-9">
                                    <div class="form-line">
                                        <select name="status_id_filter" class="form-control select2"
                                            id="status_id_filter">
                                            <option value="">-- Semua --</option>

                                            <option value="1">Aktif</option>
                                            <option value="-">Non Aktif</option>


                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">Status</label>
                                <div class="col-sm-9">
                                    <div class="form-line">
                                        <select name="status_id_filter_new" class="form-control select2"
                                            id="status_id_filter_new">
                                            <option value="">-- Semua --</option>
                                            @forelse ($status as $item)

                                            <option value="{{$item['id']}}">
                                                {{$item['name']}}</option>

                                            @empty
                                            @endforelse


                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn bg-orange waves-effect">
                                <i class="material-icons">picture_as_pdf</i>
                            </button>

                        </div>
                    </form>



                </div>

                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Penempatan</th>
                                <th>Keaktifan</th>
                                <th>Status</th>

                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>

            </div>


        </div>
    </div>


</div>









@endsection
@section('customjs')
<script src="{{ asset('bsb/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('member.indexData') !!}",
            type: 'GET',
            data: function(d) {
                console.log(d);
                d.partner_id = $('#partner_id_filter').val();
                d.division_id = $('#division_id_filter').val();
                d.status = $('#status_id_filter').val();
                d.status_new = $('#status_id_filter_new').val();

                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'email',
                name: 'email'
            },
            {
                data: 'role.name',
                name: 'role.name',
                defaultContent: "",

            },
            {
                data: 'partner.name',
                name: 'partner.name',
                defaultContent: "",

            },
            {
                data: 'is_active',
                name: 'is_active',
                defaultContent: "",

            },
            {
                data: 'status.name',
                name: 'status.name',
                defaultContent: "",

            },
            {
                data: 'action',
                name: 'action'
            },



        ],
        columnDefs: [


            {
                targets: 3,
                className: 'text-center',
                "orderable": false
            },
            {
                targets: 4,
                className: 'text-center',
                "orderable": false
            },
            {
                targets: 5,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 6,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 7,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],


    });

    $('#partner_id_filter').on('change', function() {
        // console.log('aaa');
        oTable.ajax.reload();
    });
    $('#division_id_filter').on('change', function() {
        // console.log('aaa');
        oTable.ajax.reload();
    });
    $('#status_id_filter').on('change', function() {
        // console.log('aaa');
        oTable.ajax.reload();
    });
    $('#status_id_filter_new').on('change', function() {
        oTable.ajax.reload();
    });

});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('member.look') !!}",
        success: function(data) {
            // console.log(data);

            $('#name').val(data['name']);
            $('#email').val(data['email']);
            // $('#role_id').val(data['role_id']).change();
            // $('#company_id').val(data['company_id']).change();
            $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});



$(document).ready(function() {

    // $(document).on('change', '#division_id', function() {
    $('#division_id_change').on('change', function() {

        // $('.edit_folder').on('click', function() {
        // console.log('asdasdas');

        var data = $(this).val();
        console.log(data);
        // $('.code_edit').val(data['description_programm']);
        $.ajax({
            type: 'POST',
            data: {
                "_token": "{!! csrf_token() !!}",
                "data": data
            },
            dataType: 'JSON',
            url: "{!! route('member.look_position') !!}",
            success: function(data) {
                // $('#position_id_change').val(null).select2().trigger('change');
                $("#position_id_change").empty();
                $("#position_id_change").selectpicker('refresh');

                // console.log('xxxx');
                $("#position_id_change").append("<option value=''>-- Select --</option>");
                for (i = 0; i < data.length; i++) {
                    $("#position_id_change").append("<option value='" +
                        data[i]['id'] +
                        "'>" +
                        data[i]['name'] +
                        "</option>");
                    $("#position_id_change").selectpicker('refresh');

                };
                // console.log(mySelect);

                // $.each(data, function(i, item) {
                //     var mySelect = $('#position_id_change').append($('<option>', {
                //         value: item.id,
                //         text: item.name
                //     }));

                //     mySelect.selectpicker('refresh');
                // });


                // $('#name').val(data['name']);
                // $('#email').val(data['email']);
                // $('#role_id').val(data['role_id']).change();
                // $('#company_id').val(data['company_id']).change();
                // $('#id').val(data['id']);
                // $('.is_active').val(data['is_active']).change();
                // $('.class_id').val(data['id']);
            }
        });

    });
});




$(document).on('click', '#export_pdf', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    // var data = $(this).val();
    // console.log(data);
    var division_id = $('#division_id_filter').val();
    var partner_id = $('#partner_id_filter').val();
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "division_id": division_id,
            "partner_id": partner_id
        },
        dataType: 'JSON',
        url: "{!! route('member.export_pdf') !!}",
        success: function(data) {
            console.log(data);

            // $('#name').val(data['name']);
            // $('#email').val(data['email']);
            // // $('#role_id').val(data['role_id']).change();
            // // $('#company_id').val(data['company_id']).change();
            // $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    }).done(downloadFile);;

});

function downloadFile(response) {
    console.log('xx', response);
    var blob = new Blob([response], {
        type: 'application/pdf'
    })
    var url = URL.createObjectURL(blob);
    // location.assign(url);
}
</script>
<script>
var ExcelToJSON = function() {
    this.parseExcel = function(file) {
        var reader = new FileReader();

        reader.onload = function(e) {
            var data = e.target.result;
            var workbook = XLSX.read(data, {
                type: 'binary',
                cellDates: true,
                dateNF: 'dd/mm/yyyy'
            });

            // workbook.SheetNames.forEach(function(sheetName) {
            let sheet = workbook.Sheets[workbook.SheetNames[0]];

            // var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[
            //     sheetName]);
            var XL_row_object = XLSX.utils.sheet_to_row_object_array(sheet, {
                raw: false
            });

            var productList = JSON.parse(JSON.stringify(XL_row_object));

            var rows = $('#tblItems tbody');
            var date_regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
            console.log(productList)
            for (i = 0; i < productList.length; i++) {

                // var columns = Object.values(productList[i]);
                // rows.append(`
                //         <tr>
                //             <td>${columns[0]}</td>
                //             <td>${columns[1]}</td>
                //             <td>${columns[2]}</td>
                //             <td>${columns[3]}</td>
                //             <td>${columns[4]}</td>
                //             <td>${columns[5]}</td>
                //             <td>${columns[6]}</td>
                //             <td>${columns[7]}</td>
                //             <td>${columns[8]}</td>
                //             <td>${columns[9]}</td>
                //             <td>${columns[10]}</td>
                //             <td>${columns[11]}</td>
                //             <td>${columns[12]}</td>
                //             <td>${productList[i]['nama_ibu_kandung']}</td>
                //             <td>${columns[14]}</td>
                //             <td>${columns[15]}</td>
                //             <td>${columns[16]}</td>
                //         </tr>
                //     `);


                rows.append(`
                        <tr>
                            <td>${productList[i]['name'] ?? ''}</td>
                            <td>${productList[i]['email'] ?? ''}</td>
                            <td>${productList[i]['password'] ?? ''}</td>
                            <td>${productList[i]['position_id'] ?? ''}</td>
                            <td>${productList[i]['division_id'] ?? ''}</td>
                            <td>${productList[i]['status_id'] ?? ''}</td>
                            <td>${productList[i]['partner_id'] ?? ''}</td>
                            <td>${productList[i]['tanggal_partner'] ?? ''}</td>
                            <td>${productList[i]['is_bpjskes'] ?? ''}</td>
                            <td>${productList[i]['is_bpjstk'] ?? ''}</td>
                            <td>${productList[i]['awal_masuk'] ?? ''}</td>
                            <td>${productList[i]['nama_ibu_kandung'] ?? ''}</td>
                            <td>${productList[i]['no_handpone'] ?? ''}</td>
                            <td>${productList[i]['alamat'] ?? ''}</td>
                            <td>${productList[i]['tempat_lahir'] ?? ''}</td>
                            <td>${productList[i]['tanggal_lahir'] ?? ''}</td>
                            <td>${productList[i]['nik'] ?? ''}</td>
                        </tr>
                    `);
            }

            // })
        };
        reader.onerror = function(ex) {
            console.log(ex);
        };

        reader.readAsBinaryString(file);
    };
};

function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    var xl2json = new ExcelToJSON();
    xl2json.parseExcel(files[0]);
}

document.getElementById('fileupload').addEventListener('change', handleFileSelect, false);
</script>


@endsection