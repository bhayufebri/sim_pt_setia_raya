<!DOCTYPE html>
<html>

<head>
    <title>Akuntansi</title>
    <style>
    .header {
        text-align: center;
        margin-top: -40px;
        line-height: 0.2;

    }

    .judul {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        /* page-break-before: always; */


    }

    .judul2 {
        width: 700px;
        /* margin: auto; */
        height: 10px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .ttd {
        width: 300px;
        /* margin: auto; */
        height: auto;
        border: 0px;
        padding: 5px;
        margin-left: 400px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;

    }

    .sub1 {
        font-size: 12px;
        font-weight: bold;

    }

    .uraian1 {
        font-size: 12px;
    }

    .inviseble table {
        border-collapse: collapse;
        /* width: 100%; */
        /* padding-top: -15px; */

    }

    .inviseble th {
        height: 30px;
        text-align: center;
    }

    .inviseble table,
    td,
    th {
        /* border: 1px solid black; */
        height: 14px;
        font-size: 12px;
        /* text-align: left; */
    }

    .inviseble td {
        text-align: left;

    }

    table.border td,
    th {
        border: 1px solid black;
        text-align: center;



    }

    table.no_border td,
    th {
        /* border: 1px solid black; */
        text-align: center;
    }

    div.page_break+div.page_break {
        /* page-break-before: always; */
        page-break-inside: auto;
    }

    .no-print {
        display: none;
    }
    </style>

    <style type="text/css" media="print">
    .no-print {
        display: none;
    }
    </style>
</head>

<body>
    <button onclick="window.print()" class="no-print">Print this page</button>

    <div class="header">

        <img src="{{ public_path('logo.png') }}" width="48" height="48" alt="User">
        <h4>{{$company}}</h4>
        <h4>LAPORAN AKUNTANSI</h4>

    </div>
    <!-- <div class="sub1">1. JUDUL PENELITIAN</div> -->
    <!-- <div class="judul">ks</p>
    </div> -->
    <table class="inviseble">
        <tbody>

            <tr>
                <td>Laporan tanggal</td>


                <td>: <strong>{{date('d F Y', strtotime($report->start))}} -
                        {{date('d F Y', strtotime($report->end))}}</strong></td>
            </tr>


        </tbody>
    </table>
    <br />


    <table style="border-collapse: collapse; width: 100%;" class="border">
        <tbody>

            <tr>
                <td style="width:10%;"><strong>NO</strong></td>
                <td style="width:60%;"><strong>URAIAN</strong></td>
                <td style="width:20%;"><strong>DEBET</strong></td>
                <td style="width:20%;"><strong>KREDIT</strong></td>

                <!-- <td style="width:50%;"> </td> -->
                <!-- <td></td> -->

            </tr>
            <!-- <tr>
                <td colspan="3">
                    <strong>________________________________________________________________________________________________________</strong>
                </td>


            </tr> -->
            @foreach ($report->report_detail as $index => $item)
            <tr>
                <td>{{$index + 1}}</td>
                <td style="text-align:left">{{  $item->description }}
                    @if($item->is_cash == '1')
                    <strong>(Cash)</strong>
                    @elseif($item->is_cash == '0')
                    <strong>(Transfer)</strong>

                    @endif


                </td>
                @if($item->jenis == 'debet')
                <td style="text-align:right">Rp. {{  number_format($item->jumlah,0,",",".") }}</td>
                <td style="text-align:right">-</td>

                @else
                <td style="text-align:right">-</td>
                <td style="text-align:right">Rp. {{  number_format($item->jumlah,0,",",".") }}</td>
                @endif

                <!-- <td> </td> -->
                <!-- <td></td> -->

            </tr>
            @endforeach
            <tr>
                <td colspan="4"></td>


            </tr>
            <tr>
                <td colspan="2"><strong>Subtotal</strong></td>
                <!-- <td></td> -->
                <td style="text-align:right"><strong>Rp. {{  number_format($report->total_debet,0,",",".") }}</strong>
                </td>
                <td style="text-align:right"><strong>Rp. {{  number_format($report->total_kredit,0,",",".") }}</strong>
                </td>


            </tr>
            <tr>
                <td colspan="3"><strong>Total Saldo<strong></td>
                <!-- <td></td> -->
                <!-- <td>Rp. {{  number_format($report->total_debet,0,",",".") }}</td> -->
                <td style="text-align:right"><strong>Rp. {{  number_format($report->saldo,0,",",".") }}</strong></td>


            </tr>







        </tbody>
    </table>
    <br />


    <!-- <div class=" ttd">
        Kediri, {{date('d F Y', strtotime($report->created_at)) }}
        <br />

        Admin
        <br />
        <br />
        <br />
        <br />
        <br />
        <u><strong>{{$admin}}</strong></u>
    </div> -->




    <table style="border-collapse: collapse; width: 100%;" class="no_border">
        <tbody>
            <tr>
                <!-- <td style="width:20%;"><img src="{{ public_path('logo.png') }}" width="80" height="80"
                        alt="User"></td> -->
                <td style="width:50%;">

                    <h4>Keuangan</h4>
                    <br /><br /><br />
                    <br /><br /><br />
                    _____________________

                </td>


                <td>

                    Kediri, {{date('d F Y', strtotime($report->created_at)) }}
                    <br />
                    {{$signature->signature->jabatan ?? ''}}
                    <div
                        style="background-image: url('{{ public_path('logo.png') }}'); background-size: 100px 100px; background-repeat: no-repeat; opacity: 0.5; background-position: center;">

                        @isset($signature->signature)
                        <img src="{{{public_path('signature_file/'.$signature->signature->file)}}}" width="120"
                            height="120" alt="User">
                        @endisset

                    </div>

                    @isset($signature->signature)

                    <!-- <u><strong>{{$master->user->name ?? ''}}</strong></u> -->
                    <u><strong>{{{$signature->signature->name}}}</strong></u>
                    @endisset



                </td>

            </tr>


        </tbody>
    </table>





</body>

</html>