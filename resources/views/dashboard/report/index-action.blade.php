<div class="row">

    <!-- <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalEdit"
        class="btn bg-yellow btn-circle waves-effect waves-circle waves-float edit_folder"><i
            class="material-icons">mode_edit</i></button> -->

    <a href="{{ route('report.destroy', $data->id) }}" onclick="return confirm('Are you sure?')"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i
            class="material-icons">delete</i></a>
    <a href="{{ route('report.cetak_pdf', $data->id) }}" target="_blank" class="btn btn-primary waves-effect"><i
            class="material-icons">file_download</i></a>
    <a href="{{ route('report.custom_pdf', $data->id) }}" class="btn btn-warning waves-effect"
        target="_blank">Compare</a>
</div>