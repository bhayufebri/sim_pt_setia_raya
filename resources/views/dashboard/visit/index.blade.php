@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreate">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-line">
                        <select class="form-control select2_new" id="user_id">
                            <option value="">-- Semua Korlap --</option>
                            @forelse ($user as $item)

                            <option value="{{$item['id']}}">
                                {{$item['name']}}</option>

                            @empty
                            @endforelse
                        </select>
                    </div>
                </div>
                <div id='calendar'></div>
            </div>
        </div>
    </div>


</div>






@endsection
@section('customjs')

<script>
var calendar = $('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
    },
    // editable: true,
    // events: "{!! route('partner.indexAjax') !!}",
    // events: {
    //     url: "{!! route('partner.indexAjax') !!}",
    //     type: 'POST',
    //     data: {
    //         "_token": "{!! csrf_token() !!}",
    //         "start": "",
    //         "end": "",
    //         "user_id": $('#user_id').val(),
    //         // "xxx": "vvv",
    //     },
    //     error: function() {
    //         alert('there was an error while fetching events!');
    //     },
    //     success: function(data) {
    //         console.log(data)
    //     }
    // },

    events: function(start, end, timezone, callback) {
        jQuery.ajax({
            url: "{!! route('visit.indexAjax') !!}",
            type: 'POST',
            dataType: 'json',
            data: {
                start: start.format(),
                end: end.format(),
                user_id: $('#user_id').val(),
                _token: "{!! csrf_token() !!}"
            },
            success: function(doc) {
                // console.log(doc.result);
                var events = [];
                // if (!!doc.result) {
                $.map(doc, function(r) {
                    events.push({
                        id: r.id,
                        title: r.user.name,
                        start: r.start,
                        end: r.end,
                        description: r.partner.name,
                        end_time: r.end
                    });
                });
                // }

                // $(doc).find('event').each(function() {
                //     events.push({
                //         title: $(this).attr('title'),
                //         start: $(this).attr('start'),
                //         end: $(this).attr('end')
                //     });
                // });

                callback(events);
            }
        });
    },
    displayEventTime: false,
    editable: false,
    // eventRender: function(event, element, view) {
    //     if (event.allDay === 'true') {
    //         event.allDay = true;
    //     } else {
    //         event.allDay = false;
    //     }
    // },
    selectable: true,
    selectHelper: true,

    // eventDrop: function(event, delta) {
    //     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD");
    //     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD");

    //     $.ajax({
    //         url: "{!! route('partner.ajax') !!}",
    //         data: {
    //             title: event.title,
    //             start: start,
    //             end: end,
    //             id: event.id,
    //             type: 'update',
    //             "_token": "{!! csrf_token() !!}",
    //         },
    //         type: "POST",
    //         success: function(response) {
    //             displayMessage("Event Updated Successfully");
    //         }
    //     });
    // },

    eventClick: function(event) {
        Swal.fire({
            title: 'Anda yakin?',
            text: "Anda menghapus data " + event.title,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Iya!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    data: {
                        "_token": "{!! csrf_token() !!}",
                        "data": event.id
                    },
                    dataType: 'JSON',
                    url: "{!! route('visit.destroy') !!}",
                    success: function(data) {
                        console.log(data);
                        calendar.fullCalendar('removeEvents', event.id);
                        Swal.fire(
                            'Terhapus!',
                            'Data Anda Terhapus.',
                            'success'
                        )

                    }
                });

            }
        });






    },

    eventRender: function(event, element) {
        // console.log(event);
        element.find('.fc-title').append("<br/>" + event.description);


    }

});

$('#user_id').on('change', function(events) {
    calendar.fullCalendar('refetchEvents');
});
</script>

@endsection