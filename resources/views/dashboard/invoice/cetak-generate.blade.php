<!DOCTYPE html>
<html>

<head>
    <title>BUKTI PERMINTAAN / PENYERAHAN BARANG</title>
    <style>
    .header {
        text-align: center;
        margin-top: -40px;
        line-height: 0.2;

    }

    .judul {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        /* page-break-before: always; */


    }

    .judul2 {
        width: 700px;
        /* margin: auto; */
        height: 10px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    /* .judul3 td {
        border: 1px solid black;

    } */

    .ttd {
        width: 300px;
        /* margin: auto; */
        height: auto;
        border: 0px;
        padding: 5px;
        margin-left: 400px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;

    }

    .sub1 {
        font-size: 12px;
        font-weight: bold;

    }

    .uraian1 {
        font-size: 12px;
    }

    .inviseble table {
        border-collapse: collapse;
        /* width: 100%; */
        /* padding-top: -15px; */

    }

    .inviseble th {
        height: 30px;
        text-align: center;
    }

    .inviseble table,
    td,
    th {
        /* border: 1px solid black; */
        height: 14px;
        font-size: 12px;
        /* text-align: left; */
    }

    .inviseble td {
        text-align: left;

    }

    table.border td,
    th {
        border: 1px solid black;
        text-align: center;



    }

    table.no_border td,
    th {
        /* border: 1px solid black; */
        text-align: center;
    }

    div.page_break+div.page_break {
        /* page-break-before: always; */
        page-break-inside: auto;
    }

    .no-print {
        display: none;
    }
    </style>

    <style type="text/css" media="print">
    .no-print {
        display: none;
    }
    </style>
</head>

<body>
    <button onclick="window.print()" class="no-print">Print this page</button>

    <div class="header">

        <h4>{{$company}}</h4>
        <h4>INVOICE</h4>

    </div>
    <!-- <div class="sub1">1. JUDUL PENELITIAN</div> -->
    <!-- <div class="judul">ks</p>
    </div> -->
    <table class="inviseble">
        <tbody>
            <!-- <tr>
                <td>Nama Partner</td>
                <td>: <strong>{{$order->partner->name ?? '-'}}</strong></td>
            </tr> -->

            <tr>
                <td>Tanggal </td>
                <td>: <strong>{{date('d F Y', strtotime($start))}}- {{date('d F Y', strtotime($end))}}</strong></td>
            </tr>


        </tbody>
    </table>



    <table style="border-collapse: collapse; width: 100%;" class="border">
        <tbody>

            <tr>
                <td style="width:5%;"><strong>NO</strong></td>
                <td style="width:15%;"><strong>Invoice</strong></td>
                <td style="width:20%;"><strong>Partner</strong></td>
                <td style="width:10%;"><strong>Expired</strong></td>
                <td style="width:10%;"><strong>Status</strong></td>
                <td style="width:12%;"><strong>Jumlah</strong></td>
                <!-- <td style="width:10%;"><strong>Satuan</strong></td> -->
                <td><strong>Keterangan</strong></td>

            </tr>

            @foreach ($invoice as $index => $item)
            <tr>
                <!-- <td></td> -->
                <td>{{$index+1}}</td>
                <td>{{  $item->invoice_number }}</td>
                <td style="text-align:left">{{  $item->partner->name }}</td>
                <td style="text-align:left">{{date('d F Y', strtotime($item->expired)) }}</td>

                <td style="text-align:left">
                    @if($item->status == 1)
                    Diajukan
                    @elseif($item->status == 2)
                    Disetujui
                    @elseif($item->status == 3)
                    Ditolak
                    @else
                    Tagihan
                    @endif

                </td>


                <td style="text-align:left">Rp. {{  number_format($item->jumlah,0,",",".") }}</td>
                <td style="text-align:left">
                    @foreach ($item->invoice_detail as $index2 => $item2)
                    {{  $index2+1 }}. {{  $item2->description }} (Rp.
                    {{  number_format($item2->jumlah_detail,0,",",".") }}) </br />
                    @endforeach

                </td>
                <!-- <td style="text-align:left">{{  $item->jumlah }}</td> -->


            </tr>



            @endforeach





        </tbody>
    </table>
    <br />
















</body>

</html>