<div class="row">
    @if($data->status != 2)

    <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalEdit"
        class="btn bg-yellow btn-circle waves-effect waves-circle waves-float edit_folder"><i
            class="material-icons">mode_edit</i></button>

    <a href="{{ route('invoice.destroy', $data->id) }}" onclick="return confirm('Are you sure?')"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i
            class="material-icons">delete</i></a>

    <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalDetail"
        class="btn bg-lime btn-circle waves-effect waves-circle waves-float edit_detail"><i
            class="material-icons">list</i></button>
    @endif

    <a href="{{ route('invoice.cetak_pdf', $data->id) }}" target="_blank"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float"><i class="material-icons">assignment</i></a>
    @if($data->status == 2)
    <a href="{{ route('invoice.cetak_bill', $data->id) }}" target="_blank"
        class="btn bg-info btn-circle waves-effect waves-circle waves-float"><i
            class="material-icons">card_giftcard</i></a>
    @endif


    @if($data->file)
    <a href="{{ URL::to('invoice_file/', $data->file) }}" target="_blank"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i
            class="material-icons">remove_red_eye</i></a>
    @endif


</div>