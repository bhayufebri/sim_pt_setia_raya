<!DOCTYPE html>
<html>

<head>
    <title>Invoice</title>
    <style>
    .header {
        text-align: center;
        margin-top: -40px;
        line-height: 0.2;

    }

    .judul {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        /* page-break-before: always; */


    }

    .judul2 {
        width: 700px;
        /* margin: auto; */
        height: 10px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .ttd {
        width: 300px;
        /* margin: auto; */
        height: auto;
        border: 0px;
        padding: 5px;
        margin-left: 400px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;

    }

    .sub1 {
        font-size: 12px;
        font-weight: bold;

    }

    .uraian1 {
        font-size: 12px;
    }

    .inviseble table {
        border-collapse: collapse;
        /* width: 100%; */
        /* padding-top: -15px; */

    }

    .inviseble th {
        height: 30px;
        text-align: center;
    }

    .inviseble table,
    td,
    th {
        /* border: 1px solid black; */
        height: 14px;
        font-size: 12px;
        /* text-align: left; */
    }

    .inviseble td {
        text-align: left;

    }

    table.border td,
    th {
        border: 1px solid black;
        text-align: center;



    }

    table.no_border td,
    th {
        /* border: 1px solid black; */
        text-align: center;
    }

    div.page_break+div.page_break {
        /* page-break-before: always; */
        page-break-inside: auto;
    }

    .no-print {
        display: none;
    }
    </style>

    <style type="text/css" media="print">
    .no-print {
        display: none;
    }
    </style>
</head>

<body>
    <button onclick="window.print()" class="no-print">Print this page</button>

    <div class="header">

        <!-- <img src="{{ public_path('logo.png') }}" width="48" height="48" alt="User">
        <h4>{{$company}}</h4> -->
        <h4>INVOICE</h4>

    </div>
    <!-- <div class="sub1">1. JUDUL PENELITIAN</div> -->
    <!-- <div class="judul">ks</p>
    </div> -->

    <br />


    <table style="border-collapse: collapse; width: 100%;" class="border">
        <tbody>

            <tr>
                <!-- <td style="width:40%;"><strong>DISCRIPTION</strong></td>
                <td style="width:30%;"><strong>PRICE</strong></td>
                <td style="width:10%;"><strong>QTY</strong></td>
                <td><strong>TOTAL</strong></td> -->
                <td colspan="3" style="text-align:left"><strong>{{$company}}</strong>
                    <br />
                    {{$description??''}}
                </td>

                <td colspan="2" style="text-align:left"><strong>Kediri,
                        {{date('d F Y', strtotime($invoice->created_at)) }}</strong>
                    <br />
                    Invoice No. : {{$invoice->invoice_number}}
                    <br />
                    Jatuh Tempo : {{date('d F Y', strtotime($invoice->expired))}}
                </td>
                <!-- <td style="width:10%;"><strong>QTY</strong></td>
                <td><strong>TOTAL</strong></td> -->

                <!-- <td style="width:50%;"> </td> -->
                <!-- <td></td> -->

            </tr>
            <tr>

                <td colspan="5" style="text-align:left">
                    <br />

                    <strong>Kepada : {{$invoice->partner->name}} </strong>
                    {{$invoice->partner->discription ?? '-'}}
                    <br />
                    <strong>Atas Nama : {{$invoice->atas_nama}} </strong>

                    <br />
                    <br />
                </td>

            </tr>
            <tr>

                <td style="width:10%;"><strong>No</strong></td>
                <td colspan="2"><strong>Keterangan</strong></td>
                <td colspan="2"><strong>Jumlah</strong></td>

            </tr>

            @foreach($invoice->invoice_detail as $index => $item)
            <tr>

                <td>{{$index +1}}
                    <br />
                    <br />

                </td>


                <td colspan="2" style="text-align:left">{{$item->description}}
                    <br />
                    <br />

                </td>
                <td colspan="2" style="text-align:right">
                    Rp. {{  number_format($item->jumlah_detail,0,",",".") }}
                    <br />
                    <br />

                </td>

            </tr>
            @endforeach



            <tr>

                <td></td>
                <td colspan="2" style="text-align:left">Sub Total</td>
                <td colspan="2" style="text-align:right">Rp. {{  number_format($invoice->jumlah,0,",",".") }} </td>

            </tr>

            <tr>

                <td></td>
                <td colspan="2" style="text-align:left">PPN (+)</td>
                <td colspan="2" style="text-align:right">Rp. {{  number_format($invoice->ppn,0,",",".") }} </td>

            </tr>
            <tr>

                <td></td>
                <td colspan="2" style="text-align:left">PPh (-)</td>
                <td colspan="2" style="text-align:right">Rp. {{  number_format($invoice->pph,0,",",".") }}</td>

            </tr>
            <tr>

                <td></td>
                <td colspan="2" style="text-align:left"><strong>Total</strong></td>
                <td colspan="2" style="text-align:right"><strong>Rp.
                        {{  number_format($invoice->total,0,",",".") }}</strong></td>

            </tr>






        </tbody>
    </table>
    <br />

    <table class="inviseble">
        <tbody>
            <tr>
                <td>1.</td>
                <td>Pembayaran dengan Cek/Biliyet dianggap sah setelah uang tersebut diuangkan (Clearing).</td>
            </tr>
            <!-- <tr>
                <td>2.</td>
                <td>Invoice ini berlaku sebagai kwitansi apabila pembayaran sudah terlunasi.</td>
            </tr> -->
            <tr>
                <td>2.</td>
                <td>Pembayaran harap dibayarkan atas nama <strong>{{$rekening}}</strong>.</td>
            </tr>




        </tbody>
    </table>

    <br />
    <br />

    <div class=" ttd">
        Kediri, {{date('d F Y', strtotime($invoice->created_at)) }}
        <br />
        {{$signature->signature->jabatan}}
        <div
            style="background-image: url('{{ public_path('logo.png') }}'); background-size: 100px 100px; background-repeat: no-repeat; opacity: 0.5;">

            <!-- <img src="{{ public_path('logo.png') }}" width="80" height="80" style="opacity: 0.5;" alt="User"> -->
            <img src="{{ public_path('signature_file/'.$signature->signature->file) }}" width="120" height="120"
                alt="User">
        </div>
        <u><strong>{{$signature->signature->name}}</strong></u>
    </div>








</body>

</html>