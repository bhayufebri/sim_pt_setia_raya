<!DOCTYPE html>
<html>

<head>
    <title>Invoice</title>
    <style>
    .header {
        text-align: center;
        margin-top: -40px;
        line-height: 0.2;

    }

    .judul {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        /* page-break-before: always; */


    }

    .judul2 {
        width: 700px;
        /* margin: auto; */
        height: 10px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .ttd {
        width: 300px;
        /* margin: auto; */
        height: auto;
        border: 0px;
        padding: 5px;
        margin-left: 400px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;

    }

    .sub1 {
        font-size: 12px;
        font-weight: bold;

    }

    .uraian1 {
        font-size: 12px;
    }

    .inviseble table {
        border-collapse: collapse;
        /* width: 100%; */
        /* padding-top: -15px; */

    }

    .inviseble th {
        height: 30px;
        text-align: center;
    }

    .inviseble table,
    td,
    th {
        /* border: 1px solid black; */
        height: 14px;
        font-size: 12px;
        /* text-align: left; */
    }

    .inviseble td {
        text-align: left;

    }

    table.border td,
    th {
        border: 1px solid black;
        text-align: center;



    }


    table.no_border td,
    th {
        /* border: 1px solid black; */
        text-align: center;
    }



    div.page_break+div.page_break {
        /* page-break-before: always; */
        page-break-inside: auto;
    }

    .no-print {
        display: none;
    }
    </style>

    <style type="text/css" media="print">
    .no-print {
        display: none;
    }
    </style>

    <style>
    table {
        border: 1px solid black;
    }

    /* table td,
    th {
        border: 1px solid black;
        text-align: center;
    } */

    tr.in-border td {
        /* border-bottom: none; */
        border: 1px solid black;
    }

    .footer {
        font-size: 8px;
    }
    </style>




</head>

<body>
    <button onclick="window.print()" class="no-print">Print this page</button>

    <div class="header">

        <!-- <img src="{{ public_path('logo.png') }}" width="48" height="48" alt="User">
        <h4>{{$company}}</h4> -->
        <!-- <h4>INVOICE</h4> -->

    </div>
    <!-- <div class="sub1">1. JUDUL PENELITIAN</div> -->
    <!-- <div class="judul">ks</p>
    </div> -->

    <br />


    <table style="border-collapse: collapse; width: 100%;">
        <tbody>

            <tr class="in-border">
                <!-- <td style="width:40%;"><strong>DISCRIPTION</strong></td>
                <td style="width:30%;"><strong>PRICE</strong></td>
                <td style="width:10%;"><strong>QTY</strong></td>
                <td><strong>TOTAL</strong></td> -->
                <td colspan="2" style="text-align:center; font-size: 40px; background: #fa0f23;">
                    <!-- <strong>{{$company}}</strong>
                    <br />
                    {{$description}} -->
                    SRN
                </td>

                <td colspan="3" style="text-align:left; background: #f2f224;">
                    <strong>{{$company}}</strong>
                    <br />
                    {{$description}}


                    <!-- <strong>Kediri,
                        {{date('d F Y', strtotime($invoice->created_at)) }}</strong>
                    <br />
                    Invoice No. : {{$invoice->invoice_number}}
                    <br />
                    Jatuh Tempo : {{date('d F Y', strtotime($invoice->expired))}} -->
                </td>
                <!-- <td style="width:10%;"><strong>QTY</strong></td>
                <td><strong>TOTAL</strong></td> -->

                <!-- <td style="width:50%;"> </td> -->
                <!-- <td></td> -->

            </tr>
            <tr>

                <td colspan="5" style="text-align:center; font-size: 20px;">

                    <u><strong>KWITANSI</strong></u>

                </td>

            </tr>
            <tr>

                <td colspan=" 5" style="text-align:left">
                    Invoice No. : {{$invoice->invoice_number}}
                    <!-- <br />

                    <strong>Kepada : {{$invoice->partner->name}} </strong>
                    {{$invoice->partner->discription ?? '-'}}
                    <br />
                    <strong>Atas Nama : {{$invoice->atas_nama}} </strong> -->

                    <br />
                    <br />
                </td>

            </tr>
            <tr>

                <td style="width:20%; text-align:left;"><strong>Telah Terima Dari</strong></td>
                <td colspan="4" style="text-align:left">: <strong>{{$invoice->partner->name}}</strong></td>

            </tr>
            <tr>

                <td style="text-align:left"><strong>Atas Nama</strong>
                </td>
                <td colspan="4" style="text-align:left">: <strong>{{$invoice->atas_nama}}</strong>
                </td>


            </tr>
            <tr>

                <td style="text-align:left"><strong>Uang Sejumlah</strong>
                </td>
                <td colspan="4" style="text-align:left">: <strong>
                        {{  $angka }}</strong>
                </td>


            </tr>
            <tr>

                <td style="text-align:left;  vertical-align: top;"><strong>Untuk Pembayaran</strong>
                </td>
                <td colspan="4" style="text-align:left;">

                    @foreach ($invoice->invoice_detail as $index => $item)
                    {{$index == 0 ? ":" : '_' }}

                    <strong>
                        {{$index +1}}. {{  $item->description }}</strong><br />

                    @endforeach

                </td>


            </tr>



            <tr>

                <td style="text-align:left"></td>
                <td style="text-align:left font-size: 40px;">-----------------------------------<br />
                    <strong style="font-size: 20px;">&nbsp;Rp.
                        {{  number_format($invoice->total,0,",",".") }}</strong><br />-----------------------------------
                </td>
                <td style="text-align:left"></td>

                <td colspan="2" style="text-align:left">
                    <br />

                    Kediri, {{date('d F Y', strtotime($invoice->updated_at)) }}

                    <br />
                    {{$signature->signature->jabatan}}
                    <div
                        style="background-image: url('{{ public_path('logo.png') }}'); background-size: 100px 100px; background-repeat: no-repeat; opacity: 0.5;">

                        <!-- <img src="{{ public_path('logo.png') }}" width="80" height="80" style="opacity: 0.5;" alt="User"> -->
                        <img src="{{ public_path('signature_file/'.$signature->signature->file) }}" width="120"
                            height="120" alt="User">
                    </div>
                    <u><strong>{{$signature->signature->name}}</strong></u>
                </td>


            </tr>
            <tr>

                <td colspan="5" style="text-align:left">

                    <div class="footer">
                        PT. Setia Raya Nanda menyatakan kuitansi ini sebagai bukti pembayaran yang Sah.
                    </div>

                </td>

            </tr>





        </tbody>
    </table>
    <br />

    <!-- <table class="inviseble">
        <tbody>

            <tr>
                <td style="width:30%;"></td>
                <td>____________________</td>


            </tr>
            <tr>
                <td></td>
                <td><strong>Rp.
                        {{  number_format($invoice->total,0,",",".") }}</strong></td>
            </tr>
            <tr>
                <td></td>

                <td>___________________</td>
            </tr>




        </tbody>
    </table> -->










</body>

</html>