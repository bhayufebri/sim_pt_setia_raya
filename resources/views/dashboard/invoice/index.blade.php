@extends('layouts.app')

@section('content')




<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreateCustom">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">

                <form action="{{ route('invoice.generateRekapInvoice') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="row clearfix">

                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                            <label for="email_address_2">Rentang</label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-3 col-xs-6">
                            <div class="form-group">
                                <div class="form-line">

                                    <input type="text" class="form-control daterange" name="date_range"
                                        id="date_filter">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <!-- <input type="checkbox" id="remember_me_4" class="filled-in">
                            <label for="remember_me_4">Remember Me</label> -->
                            <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect">Generate</button>
                        </div>
                    </div>
                </form>

                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Invoice</th>
                                <th>Partner</th>
                                <th>Expired</th>
                                <th>status</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ModalCreateCustom" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="form_validation6" action="{{ route($form_action) }}" method="post" autocomplete="off">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel">Tambah</h4>
                </div>
                <div class="modal-body">

                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="nama_tagihan[]" class="form-control" autocomplete="off"
                                        id="nama_tagihan_create">
                                    <label class="form-label">Nama Tagihan</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="jumlah_detail[]" class="form-control" autocomplete="off"
                                        id="jumlah_detail_create">
                                    <label class="form-label">Jumlah</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="button" class="btn bg-light-blue waves-effect" id="rowAdder">
                                <i class="material-icons">note_add</i>
                            </button>
                        </div>
                    </div>


                    <div id="newinput"></div>


                    @forelse ($form as $item)


                    @if( $item['type'] == 'select')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select name="{{$item['name']}}" class="form-control select2">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>
                    @elseif($item['type'] == 'date')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="date" name="{{$item['name']}}" class="form-control" autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @elseif($item['type'] == 'select2')

                    <label class="form-label">{{$item['label']}}</label>
                    <div class="form-group ">
                        <div class="form-line">
                            <select id="modalKhusus" name="{{$item['name']}}"
                                class="form-control select2_new_modal_custom">
                                <option value="">-- {{$item['label']}} --</option>
                                @forelse ($item['option'] as $item)

                                <option value="{{$item['id']}}">{{$item['name']}}</option>

                                @empty
                                @endforelse
                            </select>

                        </div>
                    </div>
                    @elseif($item['type'] == 'daterange')


                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="{{$item['name']}}" class="form-control daterange"
                                        autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>

                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row clearfix">
                        <div class="col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input id="{{$item['name']}}_create" type="{{$item['type']}}"
                                        name="{{$item['name']}}" class="form-control" autocomplete="off">
                                    <label class="form-label">{{$item['label']}}</label>
                                </div>
                            </div>
                        </div>
                    </div>


                    @endif


                    @empty
                    @endforelse






                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link waves-effect">SAVE</button>
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>





<div class="modal fade" id="ModalDetail" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <form id="form_validation6" action="{{ route('invoice.store_detail') }}" method="post"
                    autocomplete="off">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}

                    <input type="hidden" name="id_invoice" id="id_invoice">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="nama_tagihan" id="nama_tagihan" class="form-control"
                                        autocomplete="off">
                                    <label class="form-label">Nama Tagihan</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="jumlah_detail" id="jumlah_detail" class="form-control"
                                        autocomplete="off">
                                    <label class="form-label">Jumlah</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <button type="submit" class="btn bg-light-blue waves-effect" id="addDetail">
                                <i class="material-icons">note_add</i>
                            </button>
                        </div>
                    </div>
                </form>
                <div class="body table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Description</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">


                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>



@endsection
@section('customjs')

<script>
$(document).ready(function() {

    $('.daterange').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        },
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('invoice.indexData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();
                d.date = $('#date_filter').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'invoice_number',
                name: 'invoice_number',
                defaultContent: ""
            },
            {
                data: 'partner.name',
                name: 'partner.name',
                defaultContent: ""
            },
            {
                data: 'expired',
                name: 'expired'
            },
            {
                data: 'status',
                name: 'status',
                render: function(data) {
                    if (data == 1) {
                        return '<span class="badge bg-orange">diajukan</span>';
                    } else if (data == 2) {
                        return '<span class="badge bg-green">disetujui</span>';
                    } else if (data == 3) {
                        return '<span class="badge bg-red">ditolak</span>';
                    } else {
                        return '<span class="badge bg-cyan">tagihan</span>';

                    }
                }
            },
            {
                data: 'action',
                name: 'action'
            },



        ],
        columnDefs: [

            {
                targets: 5,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],


    });

    $('.daterange').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format(
            'YYYY-MM-DD'));

        oTable.ajax.reload();

    });

    $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        oTable.ajax.reload();

    });
});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('invoice.look') !!}",
        success: function(data) {
            console.log(data);
            var today = moment(data['expired']).format('YYYY-MM-DD');
            $('#partner_id').val(data['partner_id']).change();
            $('#expired').val(today);
            $('#description').val(data['description']);
            $('#jumlah').val(data['jumlah']);
            $('#ppn').val(data['ppn']);
            $('#pph').val(data['pph']);
            $('#atas_nama').val(data['atas_nama']).closest(".form-line").addClass("focused");
            $('#invoice_number').val(data['invoice_number']).closest(".form-line").addClass(
                "focused");
            $('#status').val(data['status']).change();
            $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});


$("#rowAdder").click(function() {
    newRowAdd = '<div class="row clearfix" id="row">' +
        '<div class="col-sm-6">' +
        '<div class="form-group form-float">' +
        '<div class="form-line focused">' +
        '<input type="text" name="nama_tagihan[]" class="form-control" autocomplete="off">' +
        '<label class="form-label">Nama Tagihan</label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-4">' +
        '<div class="form-group form-float">' +
        '<div class="form-line focused">' +
        '<input type="text" name="jumlah_detail[]" class="form-control" autocomplete="off">' +
        '<label class="form-label">Jumlah</label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-1">' +
        '<button type="button" class="btn bg-danger waves-effect" id="DeleteRow">' +
        '<i class="material-icons">delete</i>' +
        '</button>' +
        '</div>' +
        '</div>';

    $('#newinput').append(newRowAdd);
});

$(document).on("click", "#DeleteRow", function() {
    $(this).parents("#row").remove();
})




$(document).on('click', '.edit_detail', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');
    $('#myTable').empty();

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('invoice.look') !!}",
        success: function(data) {
            // console.log(data.invoice_detail);

            $('#id_invoice').val(data.id);
            for (i = 0; i < data.invoice_detail.length; i++) {
                $('#myTable').append(
                    '<tr>' +
                    '<th scope="row">' + (i + 1) + '</th>' +
                    '<td>' + data.invoice_detail[i].description + '</td>' +
                    '<td>' + data.invoice_detail[i].jumlah_detail + '</td>' +
                    '<td><a href="invoice/destroy_detail/' + data.invoice_detail[i].id +
                    '" onclick="return confirm(`Are you sure?`)" class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i class="material-icons">delete</i></a></td>' +
                    '</tr>'
                );

            }
        }
    });

});



$("#modalKhusus").change(function() {
    // alert($(this).val());

    var partner_id = $(this).val();

    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "partner_id": partner_id
        },
        dataType: 'JSON',
        url: "{!! route('invoice.lookInvoice') !!}",
        success: function(data) {
            // console.log(data);
            $('#newinput').empty();
            $('#nama_tagihan_create').val('');
            $('#jumlah_detail_create').val('');
            if (data !== null) {
                $('#pph_create').val(data.pph).closest(".form-line").addClass("focused");
                $('#ppn_create').val(data.ppn).closest(".form-line").addClass("focused");
                $('#atas_nama_create').val(data.atas_nama).closest(".form-line").addClass(
                    "focused");
                $('#invoice_number_create').val(data.invoice_number).closest(".form-line").addClass(
                    "focused");
                for (i = 0; i < data.invoice_detail.length; i++) {

                    if (i == 0) {
                        $('#nama_tagihan_create').val(data.invoice_detail[i].description).closest(
                            ".form-line").addClass("focused");
                        $('#jumlah_detail_create').val(data.invoice_detail[i].jumlah_detail)
                            .closest(
                                ".form-line").addClass("focused");
                    } else {
                        newRowAdd = '<div class="row clearfix" id="row">' +
                            '<div class="col-sm-6">' +
                            '<div class="form-group form-float">' +
                            '<div class="form-line focused">' +
                            '<input type="text" name="nama_tagihan[]" class="form-control" value="' +
                            data.invoice_detail[i].description + '" autocomplete="off">' +
                            '<label class="form-label">Nama Tagihan</label>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="form-group form-float">' +
                            '<div class="form-line focused">' +
                            '<input type="text" name="jumlah_detail[]" class="form-control" value="' +
                            data.invoice_detail[i].jumlah_detail + '" autocomplete="off">' +
                            '<label class="form-label">Jumlah</label>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-sm-1">' +
                            '<button type="button" class="btn bg-danger waves-effect" id="DeleteRow">' +
                            '<i class="material-icons">delete</i>' +
                            '</button>' +
                            '</div>' +
                            '</div>';

                        $('#newinput').append(newRowAdd);
                    }




                };


                Swal.fire(
                    'Berhasil!',
                    'Data terakir ditemukan.',
                    'success'
                )
            } else {
                $('#pph_create').val('');
                $('#ppn_create').val('');
                $('#atas_nama_create').val('');
                $('#invoice_number_create').val('');
                $('#newinput').empty();

            }


        }
    });



});
</script>

@endsection