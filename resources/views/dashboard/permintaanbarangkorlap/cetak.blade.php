<!DOCTYPE html>
<html>

<head>
    <title>BON PERMINTAAN BARANG</title>
    <style>
    .header {
        text-align: center;
        margin-top: -40px;
        line-height: 0.2;

    }

    .judul {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        /* page-break-before: always; */


    }

    .judul2 {
        width: 700px;
        /* margin: auto; */
        height: 10px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    /* .judul3 td {
        border: 1px solid black;

    } */

    .ttd {
        width: 300px;
        /* margin: auto; */
        height: auto;
        border: 0px;
        padding: 5px;
        margin-left: 400px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;

    }

    .sub1 {
        font-size: 12px;
        font-weight: bold;

    }

    .uraian1 {
        font-size: 12px;
    }

    .inviseble table {
        border-collapse: collapse;
        /* width: 100%; */
        /* padding-top: -15px; */

    }

    .inviseble th {
        height: 30px;
        text-align: center;
    }

    .inviseble table,
    td,
    th {
        /* border: 1px solid black; */
        height: 14px;
        font-size: 12px;
        /* text-align: left; */
    }

    .inviseble td {
        text-align: left;

    }

    table.border td,
    th {
        border: 1px solid black;
        text-align: center;



    }

    table.no_border td,
    th {
        /* border: 1px solid black; */
        text-align: center;
    }

    div.page_break+div.page_break {
        /* page-break-before: always; */
        page-break-inside: auto;
    }

    .no-print {
        display: none;
    }
    </style>

    <style type="text/css" media="print">
    .no-print {
        display: none;
    }
    </style>
</head>

<body>
    <button onclick="window.print()" class="no-print">Print this page</button>

    <div class="header">

        <h4>{{$company}}</h4>
        <h4>BUKTI PERMINTAAN / PENYERAHAN BARANG</h4>

    </div>
    <!-- <div class="sub1">1. JUDUL PENELITIAN</div> -->
    <!-- <div class="judul">ks</p>
    </div> -->
    <table class="inviseble">
        <tbody>
            <tr>
                <td>Nama Partner</td>
                <td>: <strong>{{$order->partner->name ?? '-'}}</strong></td>
            </tr>

            <tr>
                <td>Tanggal </td>
                <td>: <strong>{{date('d F Y H:i', strtotime($order->created_at))}}</strong></td>
            </tr>


        </tbody>
    </table>



    <table style="border-collapse: collapse; width: 100%;" class="border">
        <tbody>

            <tr>
                <td style="width:5%;"><strong>NO</strong></td>
                <td style="width:10%;"><strong>Kode</strong></td>
                <td style="width:40%;"><strong>Nama</strong></td>
                <td style="width:10%;"><strong>Jumlah</strong></td>
                <td style="width:10%;"><strong>Satuan</strong></td>
                <td><strong>Keterangan</strong></td>

            </tr>

            @foreach ($detail as $index => $item)
            <tr>
                <!-- <td></td> -->
                <td>{{$index+1}}</td>
                <td>{{  $item->kode }}</td>
                <td style="text-align:left">{{  $item->nama }}</td>
                <td>{{  $item->jumlah }}</td>
                <td style="text-align:left">{{  $item->satuan }}</td>
                <td style="text-align:left">{{  $item->keterangan }}</td>


            </tr>



            @endforeach





        </tbody>
    </table>
    <br />







    <table style="border-collapse: collapse; width: 100%;" class="no_border">
        <tbody>
            <tr>
                <!-- <td style="width:20%;"><img src="{{ public_path('logo.png') }}" width="80" height="80"
                        alt="User"></td> -->
                <td style="width:30%;">
                    <div
                        style="background-image: url('{{ public_path('logo.png') }}'); background-size: 100px 100px; background-repeat: no-repeat; opacity: 0.5; background-position: center;">
                        <h4>Direktur Operasional</h4>
                        <br /><br /><br />
                        <br /><br /><br />
                        _____________________
                    </div>

                </td>

                <td>

                    Penerima,
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />

                    <u><strong>_____________________________</strong></u>




                </td>


                <td>

                    Kediri, {{date('d F Y', strtotime($order->created_at)) }}
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />

                    <u><strong>{{{$order->user->name}}}</strong></u>




                </td>

            </tr>


        </tbody>
    </table>








</body>

</html>