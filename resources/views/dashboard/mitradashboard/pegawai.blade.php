@extends('layouts.app2')

@section('content')

<style type="text/css">
table.dataTable td {
    font-size: 0.8em;
}
</style>



<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}x
                </h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Jabatan</th>
                                <th>Handphone</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>






@endsection
@section('customjs')

<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('dashboard.dataPegawai') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        dom: 'Blfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ],
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'nip',
                name: 'nip'
            },
            {
                data: 'name',
                name: 'name'
            },
            {
                data: 'alamat',
                name: 'alamat'
            },
            {
                data: 'division.name',
                name: 'division.name',
                defaultContent: ""
            },
            {
                data: 'no_handpone',
                name: 'no_handpone'
            },




        ],



    });
});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('dashboard.look') !!}",
        success: function(data) {
            // console.log(data);

            // $('#name').val(data['name']);
            // $('#description').val(data['description']);
            $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});
</script>

@endsection