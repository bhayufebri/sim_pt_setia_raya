@extends('layouts.app2')

@section('content')




<div class="card">
    <div class="header">
        <h2>{{$pageTitle}}</h2>

    </div>
    <div class="body">
        <h2 class="card-inside-title">Keterangan</h2>
        <form action="{{ route('dashboard.contact_store') }}" method="post">

            {{ csrf_field() }}
            {{ method_field('POST') }}
            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="form-line">
                            <textarea rows="4" class="form-control no-resize" name="description"
                                placeholder="Please type what you want..."></textarea>
                        </div>
                    </div>
                </div>
            </div>



            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SAVE</button>

        </form>
    </div>
</div>


<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Partner</th>
                                <th>Pesan</th>
                                <th>Tanggal</th>
                                <th>Status</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>






@endsection
@section('customjs')

<script>
$(document).ready(function() {


    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('dashboard.serviceData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'partner.name',
                name: 'partner.name',
                defaultContent: ""
            },
            {
                data: 'description',
                name: 'description'
            },
            {
                data: 'created_at',
                name: 'created_at'
            },
            {
                data: 'status',
                name: 'status'
            }




        ],


    });
});


$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('dashboard.look') !!}",
        success: function(data) {
            // console.log(data);

            // $('#name').val(data['name']);
            // $('#description').val(data['description']);
            $('#id').val(data['id']);
            // $('.is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});
</script>

@endsection