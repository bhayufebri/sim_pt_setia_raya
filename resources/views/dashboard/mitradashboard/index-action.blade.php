<div class="row">
    @if($data->status != 2)
    <button type="button" value="{{$data->id}}" data-toggle="modal" data-target="#ModalEdit"
        class="btn bg-yellow btn-circle waves-effect waves-circle waves-float edit_folder"><i
            class="material-icons">mode_edit</i></button>
    @endif
    <a href="{{ route('dashboard.cetak_pdf', $data->id) }}" target="_blank"
        class="btn bg-cyan btn-circle waves-effect waves-circle waves-float"><i
            class="material-icons">assignment</i></a>
    @if($data->file)
    <a href="{{ URL::to('invoice_file', $data->file) }}" target="_blank"
        class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i
            class="material-icons">remove_red_eye</i></a>
    @endif


</div>