@extends('layouts.app2')

@section('content')
<style type="text/css">
table.dataTable td {
    font-size: 0.8em;
}
</style>



<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>

            </div>
            <div class="body">

                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active"><a href="#rekap" aria-controls="settings" role="tab"
                            data-toggle="tab"><i class="material-icons">list</i>Rekap</a></li>
                    <li role="presentation"><a href="#calender" aria-controls="settings" role="tab" data-toggle="tab"><i
                                class="material-icons">date_range</i>Kalendar</a></li>

                </ul>


                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane fade in " id="calender">

                        <div class="col-md-12">
                            <div class="form-line">
                                <select class="form-control select2_new" id="user_id">
                                    <option value="">-- Pilih Pegawai --</option>
                                    @forelse ($member as $item)

                                    <option value="{{$item['id']}}">
                                        {{$item['name']}}</option>

                                    @empty
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div id='calendar'></div>
                        <span class="badge bg-red">Belum Absen</span>
                        <span class="badge bg-blue">Sudah Absen</span>
                        <span class="badge bg-green">Belum Absen Pulang</span>
                        <span class="badge bg-orange">Lembur</span>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in active" id="rekap">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">User</label>
                                <div class="col-sm-9">
                                    <div class="form-line">
                                        <select name="user_id_filter" class="form-control select2_new"
                                            id="user_id_filter">
                                            <option value="">-- Semua --</option>
                                            @forelse ($member as $item)

                                            <option value="{{$item['id']}}">
                                                {{$item['name']}}</option>

                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-3 col-xs-6">
                                <label for="NameSurname" class="col-sm-3 control-label">Tanggal</label>
                                <div class="col-sm-9">
                                    <div class="form-line">
                                        <input class="form-control daterange" name="date_filter" type="text"
                                            id="date_filter">

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="table-responsive">
                                <!-- <table id="example" class="display" style="width:100%"> -->
                                <table id="example" class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <!-- <th>ID</th> -->
                                            <th>Nama</th>
                                            <!-- <th>Partner</th> -->
                                            <th>Divisi</th>
                                            <th>Absen</th>
                                            <th>Berangkat</th>
                                            <th>Pulang</th>
                                            <th>Keterangan</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


</div>






@endsection
@section('customjs')

<script>
$(document).ready(function() {







    $('.daterange').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        },
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });






    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('dashboard.dataAbsen') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);

                d.user_id = $('#user_id_filter').val();
                d.date = $('#date_filter').val();



            }
        },
        dom: 'Blfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ],
        lengthMenu: [
            [10, 25, 50, -1],
            ['10 rows', '25 rows', '50 rows', 'Show all']
        ],
        order: [
            [2, 'desc']
        ],
        columns: [
            // {
            //     data: 'id',
            //     name: 'id'
            // },
            {
                data: 'user.name',
                name: 'user.name',
                defaultContent: ""

            },

            {
                data: 'user.division.name',
                name: 'user.division.name',
                defaultContent: ""

            },
            {
                data: 'start_plot',
                name: 'start_plot'
            },
            {
                data: 'start_absen',
                name: 'start_absen'
            },
            {
                data: 'end_absen',
                name: 'end_absen'
            },
            {
                data: 'lembur',
                name: 'lembur',
                orderable: false,
                searchable: false
            }



        ],


    });


    $('#user_id_filter').on('change', function() {
        oTable.ajax.reload();
    });



    $('.daterange').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format(
            'YYYY-MM-DD'));

        oTable.ajax.reload();

    });

    $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        oTable.ajax.reload();

    });





    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        color: 'yellow',
        // editable: true,
        // events: "{!! route('partner.indexAjax') !!}",
        // events: {
        //     url: "{!! route('partner.indexAjax') !!}",
        //     type: 'POST',
        //     data: {
        //         "_token": "{!! csrf_token() !!}",
        //         "start": "",
        //         "end": "",
        //         "user_id": $('#user_id').val(),
        //         // "xxx": "vvv",
        //     },
        //     error: function() {
        //         alert('there was an error while fetching events!');
        //     },
        //     success: function(data) {
        //         console.log(data)
        //     }
        // },

        events: function(start, end, timezone, callback) {
            jQuery.ajax({
                url: "{!! route('dashboard.indexAjax2') !!}",
                type: 'POST',
                dataType: 'json',
                data: {
                    start: start.format(),
                    end: end.format(),
                    _token: "{!! csrf_token() !!}",
                    user_id: $('#user_id').val(),
                },
                success: function(doc) {
                    // console.log(doc);
                    var events = [];
                    // if (!!doc.result) {
                    $.map(doc, function(r) {
                        if (r.start_absen == null) {

                            events.push({
                                id: r.id,
                                title: r.user.name + ' (' + r.title +
                                    ')',
                                start: r.start,
                                end: r.end,
                                description: r.start_plot,
                                end_time: r.end_plot,
                                color: 'red'
                            });
                        } else if (r.start_absen != null && r.end_absen ==
                            null) {
                            events.push({
                                id: r.id,
                                title: r.user.name + ' (' + r.title +
                                    ')',
                                start: r.start,
                                end: r.end,
                                description: r.start_plot,
                                end_time: r.end_plot,
                                color: 'green'
                            });


                        } else {
                            // console.log(r);
                            if (r.end_absen > r.end_time) {
                                events.push({
                                    id: r.id,
                                    title: r.user.name + ' (' + r
                                        .title + ')',
                                    start: r.start,
                                    end: r.end,
                                    description: r.start_plot,
                                    end_time: r.end_plot,
                                    color: 'orange'
                                });
                            } else {
                                events.push({
                                    id: r.id,
                                    title: r.user.name + ' (' + r
                                        .title + ')',
                                    start: r.start,
                                    end: r.end,
                                    description: r.start_plot,
                                    end_time: r.end_plot
                                });
                            }
                        }
                    });
                    // }

                    // $(doc).find('event').each(function() {
                    //     events.push({
                    //         title: $(this).attr('title'),
                    //         start: $(this).attr('start'),
                    //         end: $(this).attr('end')
                    //     });
                    // });

                    callback(events);
                }
            });
        },
        displayEventTime: false,
        editable: false,
        // eventRender: function(event, element, view) {
        //     if (event.allDay === 'true') {
        //         event.allDay = true;
        //     } else {
        //         event.allDay = false;
        //     }
        // },
        selectable: true,
        selectHelper: true,

        eventRender: function(event, element) {
            // console.log(event);
            element.find('.fc-title').append("<br/>" + new Date(event.description).getHours() +
                ':' + new Date(event.description).getMinutes() +
                '-' + new Date(event.end_time).getHours() + ':' + new Date(event.end_time)
                .getMinutes());
            element.find('.fc-title').attr('data-toggle', 'tooltip');

            element.find('.fc-title').attr('title', event.title);

        }

    });

    $('#user_id').on('change', function(events) {

        calendar.fullCalendar('refetchEvents');

    });
});
</script>

@endsection