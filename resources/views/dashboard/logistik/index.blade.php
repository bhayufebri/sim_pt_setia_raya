@extends('layouts.app')

@section('content')

<style type="text/css">
table.dataTable td {
    font-size: 0.8em;
}
</style>



<div class="row clearfix">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {{$pageTitle}}
                </h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>User</th>
                                <th>Mitra</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                                <th>Action</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Master Barang
                </h2>
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>

                        <ul class="dropdown-menu pull-right">
                            <li><button data-toggle="modal" data-target="#ModalCreate">Add</button></li>
                            <!-- <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li> -->
                        </ul>

                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <!-- <table id="example" class="display" style="width:100%"> -->
                    <table id="example2" class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Kode</th>
                                <th>Nama</th>
                                <th>Satuan</th>
                                <th>Action</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>


</div>








@endsection
@section('customjs')

<script>
$(document).ready(function() {




    var oTable = $('#example').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('logistik.indexData') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'partner.name',
                name: 'partner.name',
                defaultContent: ""
            },
            {
                data: 'user.name',
                name: 'user.name',
                defaultContent: ""
            },
            {
                data: 'created_at',
                name: 'created_at',
                defaultContent: ""
            },
            {
                data: 'status',
                name: 'status',
                defaultContent: ""
            },
            {
                data: 'action',
                name: 'action'
            },



        ],
        columnDefs: [

            {
                targets: 4,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            },
            {
                targets: 5,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],


    });


    var oTable2 = $('#example2').DataTable({

        processing: true,
        serverSide: true,
        ajax: {
            url: "{!! route('logistik.dataItem') !!}",
            type: 'GET',
            data: function(d) {
                // console.log(d);
                //   d.status_user = $('#status_user').val();
                //   d.banned_user = $('#banned_user').val();
                //   d.type_user = $('#type_user').val();


            }
        },
        order: [
            [0, 'desc']
        ],
        columns: [{
                data: 'id',
                name: 'id'
            },
            {
                data: 'code',
                name: 'code',
                defaultContent: ""
            },
            {
                data: 'name',
                name: 'name',
                defaultContent: ""
            },
            {
                data: 'satuan',
                name: 'satuan',
                defaultContent: ""
            },

            {
                data: 'action',
                name: 'action'
            },



        ],
        columnDefs: [

            {
                targets: 4,
                className: 'text-center',
                "orderable": false,
                "bSearchable": false
            }

        ],


    });


    $(document).on('click', '.setujui', function() {
        var datax = $(this).val();
        // console.log(datax);
        Swal.fire({
            title: 'Anda Yakin',
            text: "Menyetujui ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, saya setuju!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    data: {
                        "_token": "{!! csrf_token() !!}",
                        "data": datax
                    },
                    dataType: 'JSON',
                    url: "{!! route('logistik.setujui') !!}",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            'Berhasil!',
                            'Data telah disetujui.',
                            'success'
                        )
                        oTable.ajax.reload();

                    }
                });

            }
        })

        // $('.code_edit').val(data['description_programm']);


    });

    $(document).on('click', '.tolak', function() {
        var datax = $(this).val();
        // console.log(datax);
        Swal.fire({
            title: 'Anda Yakin?',
            text: "Anda menolak data ini!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, saya menolak!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    type: 'POST',
                    data: {
                        "_token": "{!! csrf_token() !!}",
                        "data": datax
                    },
                    dataType: 'JSON',
                    url: "{!! route('logistik.tolak') !!}",
                    success: function(data) {
                        console.log(data);
                        Swal.fire(
                            'Ditolak!',
                            'Anda telah menolak data ini.',
                            'success'
                        )
                        oTable.ajax.reload();

                    }
                });

            }
        })

        // $('.code_edit').val(data['description_programm']);


    });

});




$(document).on('click', '.edit_folder', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('logistik.look') !!}",
        success: function(data) {
            // console.log(data);

            $('#name').val(data['name']);
            $('#code').val(data['code']);
            $('#satuan').val(data['satuan']);
            $('#id').val(data['id']);
            // $('#is_active').val(data['is_active']).change();
            // $('.class_id').val(data['id']);
        }
    });

});


$("#rowAdder").click(function() {
    newRowAdd = '<div class="row clearfix" id="row">' +
        '<div class="col-sm-2">' +
        '<div class="form-group form-float">' +
        '<div class="form-line focused">' +
        '<input type="text" name="kode[]" class="form-control" autocomplete="off">' +
        '<label class="form-label">Kode</label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-3">' +
        '<div class="form-group form-float">' +
        '<div class="form-line focused">' +
        '<input type="text" name="nama_barang[]" class="form-control" autocomplete="off">' +
        '<label class="form-label">Nama barang</label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-1">' +
        '<div class="form-group form-float">' +
        '<div class="form-line focused">' +
        '<input type="text" name="satuan[]" class="form-control" autocomplete="off">' +
        '<label class="form-label">Satuan</label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-1">' +
        '<div class="form-group form-float">' +
        '<div class="form-line focused">' +
        '<input type="number" name="jumlah[]" class="form-control" autocomplete="off">' +
        '<label class="form-label">Jumlah</label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-3">' +
        '<div class="form-group form-float">' +
        '<div class="form-line focused">' +
        '<input type="text" name="keterangan[]" class="form-control" autocomplete="off">' +
        '<label class="form-label">Keterangan</label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="col-sm-1">' +
        '<button type="button" class="btn bg-danger waves-effect" id="DeleteRow">' +
        '<i class="material-icons">delete</i>' +
        '</button>' +
        '</div>' +
        '</div>';

    $('#newinput').append(newRowAdd);
});

$(document).on("click", "#DeleteRow", function() {
    $(this).parents("#row").remove();
});





$(document).on('click', '.edit_detail', function() {

    // $('.edit_folder').on('click', function() {
    // console.log('asdasdas');
    $('#myTable').empty();

    var data = $(this).val();
    // console.log(data);
    // $('.code_edit').val(data['description_programm']);
    $.ajax({
        type: 'POST',
        data: {
            "_token": "{!! csrf_token() !!}",
            "data": data
        },
        dataType: 'JSON',
        url: "{!! route('barangmitra.look') !!}",
        success: function(data) {
            // console.log(data);

            // $('#judul').val(data.order.partner.name);
            document.getElementById("judul").innerHTML = data.order.partner.name;
            for (i = 0; i < data.detail.length; i++) {
                $('#myTable').append(
                    '<tr>' +
                    '<th scope="row">' + (i + 1) + '</th>' +
                    '<td>' + data.detail[i].kode + '</td>' +
                    '<td>' + data.detail[i].nama + '</td>' +
                    '<td>' + data.detail[i].jumlah + '</td>' +
                    '<td>' + data.detail[i].satuan + '</td>' +
                    '<td><a href="barangmitra/destroy_detail/' + data.detail[i].id +
                    '" onclick="return confirm(`Are you sure?`)" class="btn bg-red btn-circle waves-effect waves-circle waves-float" role="button"><i class="material-icons">delete</i></a></td>' +
                    '</tr>'
                );

            }
        }
    });

});
</script>

@endsection