<!DOCTYPE html>
<html>

<head>
    <title>Absensi</title>
    <style>
    .header {
        text-align: center;
        margin-top: -40px;
        line-height: 0.2;

    }

    .judul {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        /* page-break-before: always; */


    }

    .judul2 {
        width: 700px;
        /* margin: auto; */
        height: 10px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .judul3 {
        width: 700px;
        /* margin: auto; */
        height: auto;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;
        page-break-inside: avoid;


    }

    .ttd {
        width: 300px;
        /* margin: auto; */
        height: auto;
        border: 0px;
        padding: 5px;
        margin-left: 700px;
        overflow: auto;
        /* margin:auto; */
        font-size: 12px;

    }

    .sub1 {
        font-size: 12px;
        font-weight: bold;

    }

    .uraian1 {
        font-size: 12px;
    }

    .inviseble table {
        border-collapse: collapse;
        /* width: 100%; */
        /* padding-top: -15px; */

    }

    .inviseble th {
        height: 30px;
        text-align: center;
    }

    .inviseble table,
    td,
    th {
        /* border: 1px solid black; */
        height: 14px;
        font-size: 12px;
        /* text-align: left; */
    }

    .inviseble td {
        text-align: left;

    }

    table.border td,
    th {
        border: 1px solid black;
        text-align: center;



    }

    table.no_border td,
    th {
        /* border: 1px solid black; */
        text-align: center;
    }

    div.page_break+div.page_break {
        /* page-break-before: always; */
        page-break-inside: auto;
    }

    .no-print {
        display: none;
    }
    </style>

    <style type="text/css" media="print">
    .no-print {
        display: none;
    }
    </style>


</head>

<body>
    <button onclick="window.print()" class="no-print">Print this page</button>

    <div class="header">

        <img src="{{ public_path('logo.png') }}" width="48" height="48" alt="User">
        <h4>{{$company}}</h4>
        <h4>LAPORAN ABSEN</h4>

    </div>
    <!-- <div class="sub1">1. JUDUL PENELITIAN</div> -->
    <!-- <div class="judul">ks</p>
    </div> -->
    <table class="inviseble">
        <tbody>

            <tr>
                <!-- <td>Laporan tanggal</td>


                <td>: ksd</td> -->
            </tr>


        </tbody>
    </table>
    <br />


    <table style="border-collapse: collapse; width: 100%;" class="border">
        <thead>


            <th style="width:5%;"><strong>NO</strong></th>
            <th style="width:10%;"><strong>NAMA</strong></th>
            <th style="width:10%;"><strong>PARTNER</strong></th>
            <th style="width:10%;"><strong>DIVISI</strong></th>
            <th style="width:30%;"><strong>Absen plot</strong></th>
            <th><strong>BERANGKAT</strong></th>
            <th><strong>PULANG</strong></th>
            <th><strong>KETERANGAN</strong></th>

            <!-- <td style="width:50%;"> </td> -->
            <!-- <td></td> -->


        </thead>

        <tbody>
            <!-- <tr>
                <td colspan="3">
                    <strong>________________________________________________________________________________________________________</strong>
                </td>


            </tr> -->
            @foreach ($absen as $index => $item)
            <tr>
                <td>{{$index + 1}}</td>

                <td style="text-align:left"> {{  $item->user['name'] ?? '' }}</td>
                <td style="text-align:left"> {{  $item->partner['name'] ?? '' }}</td>
                <td style="text-align:left"> {{  $item->user['division']['name'] ?? '' }}</td>
                <td style="text-align:left">
                    {{ $item->start_plot ? date('d F Y H:i', strtotime($item->start_plot)) : '' }} -
                    {{ $item->end_plot ? date('d F Y H:i', strtotime($item->end_plot)) : ''}}</td>
                <td style="text-align:right">
                    {{  $item->start_absen ? date('d F Y H:i', strtotime($item->start_absen)) : '' }}</td>
                <td style="text-align:right">
                    {{  $item->end_absen ? date('d F Y H:i', strtotime($item->end_absen)) : '' }}
                </td>
                <td style="text-align:right">

                    @if($item->end_plot < $item->end_absen)
                        <!-- {{$plot = \Carbon\Carbon::parse($item->end_plot)}}
                        {{$absen = \Carbon\Carbon::parse($item->end_absen)}} -->
                        <?php
                        $plot = \Carbon\Carbon::parse($item->end_plot);
                        $absen = \Carbon\Carbon::parse($item->end_absen);
                        ?>
                        {{
                            
                            $lembur = $plot->diff($absen)->format('%H Jam, %I Menit')}}
                        @else
                        {{$lembur = "-"}}
                        @endif

                        <!-- {{  $lembur }} -->
                </td>




                <!-- <td> </td> -->
                <!-- <td></td> -->

            </tr>
            @endforeach









        </tbody>
    </table>
    <br />




    <div class=" ttd">
        Kediri, {{date('d F Y', strtotime($now)) }}
        <br />

        <div
            style="background-image: url('{{ public_path('logo.png') }}'); background-size: 100px 100px; background-repeat: no-repeat; opacity: 0.5;">


            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
        <u><strong>{{$admin}}</strong></u>
    </div>








</body>

</html>