@extends('layouts.app')

@section('content')
<style type="text/css">
#map {
    height: 280px;
}
</style>

<div class="row clearfix">


    <div class="col-xs-12 col-sm-3">
        <div class="card profile-card">
            <div class="profile-header">&nbsp;</div>
            <div class="profile-body">
                <div class="image-area">
                    <img src="{{ asset('image/') }}"
                        onerror="this.onerror=null;this.src='{{ asset('bsb/images/user.png') }}';"
                        alt="AdminBSB - Profile Image" style="width:70%" />
                </div>
                <div class="content-area">
                    <h6>{{$partner->name}}</h6>
                    <!-- <p>Web Software Developer</p> -->
                    <p>{{$partner->description}}</p>
                    {!! $qr_code !!}
                </div>
            </div>

            <div class="profile-footer">

                <a href="{{ route('partnersdm.cetak_qr', $partner->id) }}" target="_blank"
                    class="btn bg-light-blue waves-effect">
                    <i class=" material-icons">print</i>
                </a>
            </div>
        </div>
    </div>


    <div class="col-xs-12 col-sm-9">
        <div class="card">
            <div class="body">
                <div>
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active"><a href="#profile_settings" aria-controls="settings"
                                role="tab" data-toggle="tab"><i class="material-icons">home</i>Lokasi</a></li>
                        <li role="presentation"><a href="#jarak" aria-controls="settings" role="tab"
                                data-toggle="tab"><i class="material-icons">settings_input_antenna</i>Range Absen</a>
                        </li>
                        <li role="presentation"><a href="#change_password_settings" aria-controls="settings" role="tab"
                                data-toggle="tab"><i class="material-icons">alarm</i>Jadwal</a></li>
                        <li role="presentation"><a href="#status" aria-controls="settings" role="tab"
                                data-toggle="tab"><i class="material-icons">mode_edit</i>Edit Absen Peg.</a></li>
                        <!-- <li role="presentation"><a href="#penempatan" aria-controls="settings" role="tab"
                                data-toggle="tab">Penempatan</a></li>
                        <li role="presentation"><a href="#berkas" aria-controls="settings" role="tab"
                                data-toggle="tab">Berkas</a></li> -->
                    </ul>

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="profile_settings">


                            <form class="form-horizontal" action="{{ route('partnersdm.location_update') }}"
                                method="post">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <input type="hidden" class="form-control" name="id" value="{{$partner->id}}">

                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Lattude</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="latitude" id="latitude"
                                                placeholder="Latitude" value="{{$partner->latitude ?? ''}}" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Longitude</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="longitude" id="longitude"
                                                placeholder="Langitude" value="{{$partner->longitude ?? ''}}" required>
                                        </div>
                                    </div>
                                </div>











                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-danger">SUBMIT</button>
                                    </div>
                                </div>
                            </form>
                            <div id="map"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="jarak">


                            <form class="form-horizontal" action="{{ route('partnersdm.jarak_update') }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('POST') }}
                                <input type="hidden" class="form-control" name="id" value="{{$partner->id}}">

                                <div class="form-group">
                                    <label for="NameSurname" class="col-sm-3 control-label">Jarak (meter)</label>
                                    <div class="col-sm-9">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="jarak" id="jarak"
                                                placeholder="Kosongkan jika menggunakan jarak default {{$jarak}} m"
                                                value="{{$partner->jarak ? $partner->jarak*1000 : '' }}">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <button type="submit" class="btn btn-danger">SUBMIT</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="change_password_settings">
                            <div class="row">

                                <div class="col-md-8">
                                    <div class="form-line">
                                        <select class="form-control select2_new" id="user_id">
                                            <option value="">-- Semua Pegawai --</option>
                                            @forelse ($member as $item)

                                            <option value="{{$item['id']}}">
                                                {{$item['name']}}</option>

                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample"
                                        role="button" aria-expanded="false" aria-controls="collapseExample">
                                        Updoad data
                                    </a>
                                </div>
                            </div>
                            <div class="collapse" id="collapseExample">
                                <form action="{{ route('partnersdm.import_schedule') }}" method="post"
                                    enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('POST') }}
                                    <input type="hidden" class="form-control" name="partner_id"
                                        value="{{$partner->id}}">
                                    <!-- <input type="hidden" class="form-control" name="user_id" id="user_id_import"> -->



                                    <div class="row clearfix">
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                            <a href="{{ route('partnersdm.download_berkas') }}" type="download"
                                                class="btn btn-primary btn-sm m-l-15 waves-effect"><i
                                                    class="material-icons">file_download</i>
                                                format</a>

                                        </div>


                                        <div class="col-lg-5 col-md-5 col-sm-3 col-xs-6">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <select class="form-control show-tick select2_new" name="user_id[]"
                                                        multiple>
                                                        @forelse ($member as $item)

                                                        <option value="{{$item['id']}}">
                                                            {{$item['name']}}</option>

                                                        @empty
                                                        @endforelse
                                                    </select>
                                                    <label class="form-label">Berkas</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                            <div class="form-group form-float">
                                                <div class="form-line">
                                                    <input type="file" class="form-control" name="file">
                                                    <label class="form-label">Berkas</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">

                                            <button type="submit"
                                                class="btn btn-primary btn-md m-l-15 waves-effect">Upload</button>
                                        </div>
                                    </div>
                                </form>

                            </div>


                            <div id='calendar'></div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade in" id="berkas">


                        </div>

                        <div role="tabpanel" class="tab-pane fade in" id="status">
                            <div class="col-md-12">
                                <div class="form-line">
                                    <select class="form-control select2_new" id="user_id_edit">
                                        <option value="">-- Pilih Pegawai --</option>
                                        @forelse ($member as $item)

                                        <option value="{{$item['id']}}">
                                            {{$item['name']}}</option>

                                        @empty
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                            <div id='calendar2'></div>
                            <span class="badge bg-red">Belum Absen</span>
                            <span class="badge bg-blue">Sudah Absen</span>
                            <span class="badge bg-green">Belum Absen Pulang</span>
                            <span class="badge bg-orange">Lembur</span>


                        </div>


                        <div role="tabpanel" class="tab-pane fade in" id="penempatan">



                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="calendarModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span
                        class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Judul</label>
                    <div class="col-sm-9">
                        <div class="form-line">
                            <input type="text" class="form-control" name="title" id="title" placeholder="Uraian"
                                value="" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Waktu</label>
                    <div class="col-sm-5">
                        <div class="form-line">
                            <input type="time" class="form-control" name="start_time" id="start_time">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-line">
                            <input type="time" class="form-control" name="end_time" id="end_time">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="set">SUBMIT</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="editTime" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span
                        class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal Absen</label>
                    <div class="col-sm-9">
                        <div class="form-line">
                            <input type="text" class="form-control daterange" id="date_absen" placeholder="Uraian">
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="batal_pulang">Batal
                    Pulang</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="ubah">SUBMIT</button>

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('customjs')


<script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
    integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>

<script>
$(document).ready(function() {
    var lat = $('#latitude').val();
    var long = $('#longitude').val();

    var map = L.map('map').setView([lat, long], 15);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    var marker = L.marker([lat, long]).addTo(map);

    var popup = L.popup();

    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("You clicked the map at " + e.latlng.toString())
            .openOn(map);

        // console.log(e.latlng.toString());
        $('#latitude').val(e.latlng.lat.toString());
        $('#longitude').val(e.latlng.lng.toString());
    }

    map.on('click', onMapClick);

    // $('.daterange').daterangepicker();
    $('.daterange').daterangepicker({
        timePicker: true,
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(32, 'hour'),
        locale: {
            format: 'YYYY/MM/DD hh:mm A'
        }
    });
    // $('select').selectpicker();


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var user_id = $('#user_id').val();
    // console.log('part' + partner_id);
    // $('#user_id').val();

    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        eventLimit: true,
        views: {
            month: {
                eventLimit: 3
            }
        },


        events: function(start, end, timezone, callback) {
            jQuery.ajax({
                url: "{!! route('partnersdm.indexAjax') !!}",
                type: 'POST',
                dataType: 'json',
                data: {
                    start: start.format(),
                    end: end.format(),
                    _token: "{!! csrf_token() !!}",
                    user_id: $('#user_id').val(),
                    partner_id: "{!! json_encode($partner->id) !!}",
                },
                success: function(doc) {
                    // console.log(doc.result);
                    var events = [];
                    // if (!!doc.result) {
                    $.map(doc, function(r) {
                        events.push({
                            id: r.id,
                            title: r.user ? r.user.name : '' + ' (' + r
                                .title + ')',
                            start: r.start,
                            end: r.end,
                            description: r.start_plot,
                            end_time: r.end_plot
                        });
                    });

                    callback(events);
                }
            });
        },
        displayEventTime: false,
        editable: false,
        // eventRender: function(event, element, view) {
        //     if (event.allDay === 'true') {
        //         event.allDay = true;
        //     } else {
        //         event.allDay = false;
        //     }
        // },
        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
            if ($('#user_id').val()) {
                $('#calendarModal').modal();
                // var title = prompt('Event Title:');

                // console.log(start);
                var start = $.fullCalendar.formatDate(start, "Y-MM-DD");
                var end = $.fullCalendar.formatDate(end, "Y-MM-DD");
                // $(document).on('click', '#set', function() {
                // $('#set').click(function() {
                $('#set').unbind('click').bind('click', function(e) {
                    // if (title) {
                    var title = $('#title').val();
                    var start_time = $('#start_time').val();
                    var end_time = $('#end_time').val();
                    var user_id = $('#user_id').val();
                    var partner_id = "{!! json_encode($partner->id) !!}";
                    // console.log('pencet');
                    if (title) {


                        $.ajax({
                            url: "{!! route('partnersdm.ajax') !!}",
                            data: {
                                title: title,
                                start: start,
                                end: end,
                                start_time: start_time,
                                end_time: end_time,
                                user_id: user_id,
                                partner_id: partner_id,
                                type: 'add',
                                "_token": "{!! csrf_token() !!}"
                            },
                            type: "POST",
                            success: function(data, start, end) {
                                // console.log(data);
                                displayMessage("Event Created Successfully");
                                // location.reload();
                                // calendar.fullCalendar('removeEvents');
                                $('#title').val('');
                                $('#start_time').val('');
                                $('#end_time').val('');
                                // $('#user_id').val('');
                                // calendar.fullCalendar('removeEvents');
                                // calendar.fullCalendar('updateEvent', events);
                                // calendar.fullCalendar('unselect');
                                // calendar.fullCalendar('removeEvents', start);
                                // calendar.fullCalendar('removeEvents', end);
                                calendar.fullCalendar('refetchEvents');


                                // calendar.fullCalendar('renderEvent', {
                                //     id: data.id,
                                //     title: title,
                                //     start: start,
                                //     end: end,
                                //     allDay: allDay
                                // }, true);

                                // calendar.fullCalendar('unselect');
                                calendar.fullCalendar('unselect');
                                // $('#title').val('');
                            }
                        });
                    }

                    // }

                    calendar.fullCalendar('unselect');

                });
            } else {
                displayMessage("User not selected");
            }
        },

        eventClick: function(event) {
            var deleteMsg = confirm("Do you really want to delete?");
            if (deleteMsg) {
                $.ajax({
                    type: "POST",
                    url: "{!! route('partnersdm.ajax') !!}",
                    data: {
                        id: event.id,
                        type: 'delete',
                        "_token": "{!! csrf_token() !!}",
                    },
                    success: function(response) {
                        calendar.fullCalendar('removeEvents', event.id);
                        displayMessage("Event Deleted Successfully");
                    }
                });
            }
        },
        eventRender: function(event, element) {
            // console.log(event);
            element.find('.fc-title').append("<br/>" + new Date(event.description).getHours() +
                ':' + new Date(event.description).getMinutes() +
                '-' + new Date(event.end_time).getHours() + ':' + new Date(event.end_time)
                .getMinutes());
            element.find('.fc-title').attr('data-toggle', 'tooltip');

            element.find('.fc-title').attr('title', event.title);

        }

    });

    $('#user_id').on('change', function(events) {
        // console.log('aaa');
        // oTable.ajax.reload();
        // console.log($('#user_id').val());
        calendar.fullCalendar('refetchEvents');
        // $('#user_id_import').val($('#user_id').val());
        // $('#calendar').fullCalendar('removeEvents');
        // $('#calendar').fullCalendar('addEventSource', events);
        // $('#calendar').fullCalendar('rerenderEvents');

    });

    $('#user_id_edit').on('change', function(events) {
        calendar2.fullCalendar('refetchEvents');
    });



    var calendar2 = $('#calendar2').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        color: 'yellow',


        events: function(start, end, timezone, callback) {
            jQuery.ajax({
                url: "{!! route('partnersdm.indexAjax2') !!}",
                type: 'POST',
                dataType: 'json',
                data: {
                    start: start.format(),
                    end: end.format(),
                    _token: "{!! csrf_token() !!}",
                    user_id: $('#user_id_edit').val(),
                    partner_id: "{!! json_encode($partner->id) !!}",
                },
                success: function(doc) {
                    // console.log(doc);
                    var events = [];
                    // if (!!doc.result) {
                    $.map(doc, function(r) {
                        if (r.start_absen == null) {

                            events.push({
                                id: r.id,
                                title: r.user ? r.user.name : '' +
                                    ' (' + r.title +
                                    ')',
                                start: r.start,
                                end: r.end,
                                description: r.start_plot,
                                end_time: r.end_plot,
                                color: 'red'
                            });
                        } else if (r.start_absen != null && r.end_absen ==
                            null) {
                            events.push({
                                id: r.id,
                                title: r.user.name + ' (' + r.title +
                                    ')',
                                start: r.start,
                                end: r.end,
                                description: r.start_plot,
                                end_time: r.end_plot,
                                color: 'green'
                            });


                        } else {
                            // console.log(r);
                            if (r.end_absen > r.end_time) {
                                events.push({
                                    id: r.id,
                                    title: r.user.name + ' (' + r
                                        .title + ')',
                                    start: r.start,
                                    end: r.end,
                                    description: r.start_plot,
                                    end_time: r.end_plot,
                                    color: 'orange'
                                });
                            } else {
                                events.push({
                                    id: r.id,
                                    title: r.user.name + ' (' + r
                                        .title + ')',
                                    start: r.start,
                                    end: r.end,
                                    description: r.start_plot,
                                    end_time: r.end_plot
                                });
                            }
                        }
                    });


                    callback(events);
                }
            });
        },
        displayEventTime: false,
        editable: false,

        selectable: true,
        selectHelper: true,

        eventClick: function(event) {



            $('#editTime').modal();




            $.ajax({
                url: "{!! route('partnersdm.look_schedule') !!}",
                data: {
                    id: event.id,
                    "_token": "{!! csrf_token() !!}"
                },
                type: "POST",
                success: function(data, start, end) {
                    // console.log(data.start_plot);
                    $("#date_absen").data('daterangepicker').setStartDate(data
                        .start_plot);
                    $("#date_absen").data('daterangepicker').setEndDate(data.end_plot);

                }
            });


            $('#ubah').unbind('click').bind('click', function(e) {


                $.ajax({
                    url: "{!! route('partnersdm.sub_schedule') !!}",
                    data: {
                        id: event.id,
                        tanggal: $('#date_absen').val(),
                        "_token": "{!! csrf_token() !!}"
                    },
                    type: "POST",
                    success: function(data, start, end) {
                        // console.log(data);
                        displayMessage("Data diubah");

                        calendar2.fullCalendar('refetchEvents');

                        calendar2.fullCalendar('unselect');
                        // $('#title').val('');
                    }
                });
            });

            $('#batal_pulang').unbind('click').bind('click', function(e) {


                $.ajax({
                    url: "{!! route('partnersdm.batal_pulang') !!}",
                    data: {
                        id: event.id,
                        "_token": "{!! csrf_token() !!}"
                    },
                    type: "POST",
                    success: function(data, start, end) {
                        // console.log(data);
                        displayMessage("Batal Pulang");

                        calendar2.fullCalendar('refetchEvents');

                        calendar2.fullCalendar('unselect');
                        // $('#title').val('');
                    }
                });
            });

        },
        eventRender: function(event, element) {
            // console.log(event);
            element.find('.fc-title').append("<br/>" + new Date(event.description).getHours() +
                ':' + new Date(event.description).getMinutes() +
                '-' + new Date(event.end_time).getHours() + ':' + new Date(event.end_time)
                .getMinutes());
            element.find('.fc-title').attr('data-toggle', 'tooltip');

            element.find('.fc-title').attr('title', event.title);

        }

    });

    $('[data-toggle="tooltip"]').tooltip();
});



function displayMessage(message) {
    toastr.success(message, 'Event');
}
</script>

@endsection