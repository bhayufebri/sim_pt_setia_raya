<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- <title>Sign In | Bootstrap Based Admin Template - Material Design</title> -->
    <title>E PEGAWAI</title>

    <!-- Favicon-->
    <!-- <link rel="icon" href="{{ asset('bsb/favicon.ico') }}" type="image/x-icon"> -->
    <link rel="shortcut icon" href="{{ asset('logo.png') }}">


    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
        type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('bsb/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('bsb/plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('bsb/plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('bsb/css/style.css') }}" rel="stylesheet">

</head>

<body class="login-page">

    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">E <b>PEGAWAI</b></a>
            <small>SIM PEGAWAI</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="msg">Login</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control @error('email') is-invalid @enderror" name="email"
                                placeholder="email" required autofocus>
                        </div>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                name="password" placeholder="Password" required>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <!-- <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} class="filled-in chk-col-pink">
                            <label for="remember">Remember Me</label> -->
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <!-- <a href="sign-up.html">Register Now!</a> -->
                        </div>
                        <!-- @if (Route::has('password.request'))

                        <div class="col-xs-6 align-right">
                            <a href="{{ route('password.request') }}">Forgot Password?</a>
                        </div>
                                   
                                @endif -->

                    </div>
                </form>
            </div>
        </div>
    </div>



    <!-- Jquery Core Js -->
    <script src="{{ asset('bsb/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('bsb/plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('bsb/plugins/node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('bsb/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <!-- Bootstrap Notify Plugin Js -->
    <script src="{{ asset('bsb/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('bsb/js/admin.js') }}"></script>
    <script src="{{ asset('bsb/js/pages/examples/sign-in.js') }}"></script>
    <script src="{{ asset('bsb/js/pages/ui/notifications.js') }}"></script>
    <!-- <script src="{{ asset('js/custom.js') }}"></script> -->

    <script>
    // $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    //   $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    $(document).ready(function() {
        $('div.alert').not('.alert-important').fadeIn().delay(3000).fadeOut();
        $('.help-block').fadeIn().delay(1500).fadeOut();
    });
    </script>

    @include('layouts.notification')


</body>

</html>