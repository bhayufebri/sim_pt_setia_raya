<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'partner_id',
         'status', 
         'description',
         'file',
         'expired',
         'ppn',
         'atas_nama',
         'pph',
         'total',
         'invoice_number',
         'jumlah'
];

public function partner(){
    return $this->belongsTo('App\Partner');
}
public function invoice_detail(){
    return $this->hasMany('App\Invoice_detail');
}
}