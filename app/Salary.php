<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Salary extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'company_id',
        'user_id',
        'period_id',
        'pendapatan_all',
        'potongan_all',
        'total_gaji',
        'uraian',
        'partner_id',
        'jumlah_absen',
        'ketidakhadiran'
];
public function user(){
    return $this->belongsTo('App\User');
}
public function period(){
    return $this->belongsTo('App\Period');
}
public function partner(){
    return $this->belongsTo('App\Partner');
}
}