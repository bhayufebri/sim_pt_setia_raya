<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use LogsActivity;
    use Notifiable;
    use SoftDeletes;
    protected static $logName = 'Member';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
        'role_id',
        'company_id',
        'image',
        'nip',
        'is_active',
        'position_id',
        'division_id',
        'status_id',
        'partner_id',
        'is_bpjskes',
        'is_bpjstk',
        'awal_masuk',
        'nama_ibu_kandung',
        'no_handpone',
        'alamat',
        'tanggal_lahir',
        'tempat_lahir',
        'nik',
        'is_payroll',
        'gaji_pokok',
        'tunjangan_jabatan',
        'potongan_bpjs_kesehatan',
        'potongan_bpjs_ketenagakerjaan',
        'potongan_payroll'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static $logFillable = true;
    public function getDescriptionForEvent(string $eventName): string
        {
            return $this->name . " {$eventName} Oleh: " . Auth::user()->name;
        }


    public function role(){
        return $this->belongsTo('App\Role');
    }
    public function company(){
        return $this->belongsTo('App\Company');
    }
    public function partner(){
        return $this->belongsTo('App\Partner');
    }
    public function division(){
        return $this->belongsTo('App\Division');
    }
    public function position(){
        return $this->belongsTo('App\Position');
    }
    public function status(){
        return $this->belongsTo('App\Status');
    }
}