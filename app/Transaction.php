<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'company_id',
        'jumlah',
        'jenis',
        'description',
        'is_cash',
        'category_id'
];
}