<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\Auth;


class Partner extends Authenticatable
{
    use Notifiable;
    use LogsActivity;

    use SoftDeletes;
    protected static $logName = 'Partner';

    protected $fillable = [
        'name', 'description', 'company_id','slug','password','is_premium','jarak'
];

protected static $logFillable = true;
public function getDescriptionForEvent(string $eventName): string
    {
        return $this->name . " {$eventName} Oleh: " . Auth::user()->name;
    }
    
public function company(){
    return $this->belongsTo('App\Company');
}
}