<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report_detail extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'report_id',
        'description',
        'jenis',
        'is_cash',
        'jumlah'
];
}