<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Order_detail extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'order_id','kode','nama','satuan','jumlah','keterangan'
];

public function order(){
    return $this->belongsTo('App\Order');
}
}