<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Detail extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'salary_id',
        'sifat',
        'jumlah',
        'description'
];
}