<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class For_signature extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'signature_id'
];

public function signature(){
    return $this->belongsTo('App\Signature');
}
}