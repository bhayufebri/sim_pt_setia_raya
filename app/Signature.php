<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Signature extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'company_id',
        'name',
        'jabatan',
        'file'
];
}