<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'start',
        'end',
        'company_id',
        'report_id_sebelumnya',
        'total_debet',
        'total_kredit',
        'saldo'
];
public function report_detail(){
    return $this->hasMany('App\Report_detail');
}
}