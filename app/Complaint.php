<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\Auth;

class Complaint extends Model
{
    use LogsActivity;
    use SoftDeletes;
    protected static $logName = 'Contact Service';
    protected $fillable = [
        'user_id',
        'partner_id',
        'company_id',
        'description',
        'status'
];
protected static $logFillable = true;
public function getDescriptionForEvent(string $eventName): string
    {
        return $this->name . " {$eventName} Oleh: " . Auth::user()->name;
    }

    
public function partner(){
    return $this->belongsTo('App\Partner');
}


}