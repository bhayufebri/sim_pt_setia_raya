<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'start','end','title','user_id','partner_id','start_plot','end_plot','start_absen',
        'end_absen','radius_berangkat','radius_pulang','latlngBerangkat','latlngPulang'
];

public function user(){
    return $this->belongsTo('App\User');
}

public function partner(){
    return $this->belongsTo('App\Partner');
}
}