<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Component extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id',
        'variable_id',
        'name',
        'is_subject',
        'jumlah',
        'sifat'
];
}