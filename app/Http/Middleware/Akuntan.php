<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class Akuntan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        // dd(Auth::user()->role_id);
        if (Auth::user() &&  Auth::user()->role_id == 7 || Auth::user() &&  Auth::user()->role_id == 2 ) {
            return $next($request);
     }
        // return $next($request);
        return redirect('/home');
    }
}