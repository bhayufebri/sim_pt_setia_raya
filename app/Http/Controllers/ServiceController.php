<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Complaint;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Str;
use PDF;


class ServiceController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->pageTitle = 'Service';
        $this->serviceActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
       
        // return json_encode($data);
        return View::make('dashboard.service.index', $data);
    }
    public function indexData(Request $request){
        $data = Complaint::with('partner')->where('company_id', Auth::user()->company->id);
         return Datatables::of($data)
                ->editColumn('created_at', function ($data) {     
                
                return date('d F Y', strtotime($data->created_at));
                })
                ->editColumn('partner.name', function ($data) {     
                
                    return '<span class="badge bg-cyan">'.$data->partner->name.'</span>';
                    })
                ->editColumn('action', function ($data) {     
                
                        return '<a href="service/cetak_pdf/'.$data->id.'" target="_blank"><i
                        class="material-icons">file_download</i></a><button type="button" value="'.$data->id.'"
                        class="btn bg-orange waves-effect btn-xs proses">Proses</button></br><button type="button" value="'.$data->id.'"
                        class="btn bg-teal waves-effect btn-xs selesai">Selesai</button>';
                        })
                    
                ->editColumn('status', function ($data) {     
                    if($data->status == 1){
                        return '<span class="badge bg-orange">Sedang Proses</span>';
                    }elseif($data->status == 2){
                        return '<span class="badge bg-teal">Selesai</span>';
                    }else{
                        return '<span class="badge bg-pink">Pengaduan</span>';
                    };
                        })

                ->rawColumns(['created_at', 'partner.name', 'action','status'])
               ->make(true); 

   }
   public function cetak_pdf($id){

    // $data['id'] = $id;
    // $data['salary'] = Salary::with('user:name,id', )findOrFail($id);
    $complaint = Complaint::with('partner')->findOrFail($id);
    $data['complaint'] = $complaint;
    $data['company'] = strtoupper(Auth::user()->company->name);

    // return json_encode($master->group->division->name);
    

    $pdf = PDF::loadView('dashboard.service.cetak', $data)->setPaper('a4', 'potrait');
        return $pdf->stream('ceklist_harian.pdf');
        // return $pdf->download('ceklist_harian.pdf');

   }

   public function proses(Request $request)
    {
        // return json_encode($request);
        $data = Complaint::find($request->data);
        $data->status = '1';
        $data->save();
        return json_encode('success');

    }
    public function selesai(Request $request)
    {
        // return json_encode($request);
        $data = Complaint::find($request->data);
        $data->status = '2';
        $data->save();
        return json_encode('success');

    }
}