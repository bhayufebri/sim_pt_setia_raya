<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Order;
use App\Order_detail;
use DataTables, PDF;
use Illuminate\Support\Facades\Auth;
use App\For_signature;

class PermintaanBarangKorlapController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('admin');
        $this->middleware('korlap');
        $this->pageTitle = 'Permintaan Barang';
        $this->PermintaanbarangkorlapActive = 'active';
        // $this->divisionActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
        // $partner = Partner::all();

        // return json_encode($data);
        return View::make('dashboard.permintaanbarangkorlap.index', $data);
    }
    public function indexData(Request $request){

        $data = Order::with('partner','user');
         return Datatables::of($data)
         ->editColumn('action', function($data){ return view('dashboard.permintaanbarangkorlap.index-action', compact('data'));})
              
               ->editColumn('created_at', function ($data) {     
                
                return date('d F Y H:i', strtotime($data->created_at));
                })
                ->editColumn('status', function ($data) {     
                if($data->status == 1){
                    return '<span class="badge bg-teal">disetujui</span>';
                }elseif($data->status == 2){
                    return '<span class="badge bg-pink">ditolak</span>';
                }else{
                    return '<span class="badge bg-orange">diajukan</span>';
                };
                    })
               ->rawColumns(['created_at','status','action'])
               ->make(true); 

   }

   public function cetak($id){

    // $data['id'] = $id;
    // $data['salary'] = Salary::with('user:name,id', )findOrFail($id);

    $data['order'] = Order::with('partner', 'user')->find($id);
       
    $data['detail'] = Order_detail::where('order_id', $id)->get();
    $data['company'] = strtoupper(Auth::user()->company->name);
    // $data['user'] = strtoupper(Auth::user()->name);
    // $data['admin'] = 'Irawan Pramudhita, SE';
        $data['signature'] = For_signature::with('signature')->where('name', 'Cek List Security')->first();
   
    $pdf = PDF::loadView('dashboard.permintaanbarangkorlap.cetak', $data)->setPaper('a4', 'potrait');
        return $pdf->stream('permintaanbarang.pdf');
        // return $pdf->download('ceklist_harian.pdf');

   }
}