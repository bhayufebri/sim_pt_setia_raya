<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Position;
use App\Division;
use Illuminate\Support\Facades\Validator;
use DataTables;

class PositionController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->pageTitle = 'Position';
        $this->masterActive = 'active';
        $this->positionActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
        $division = Division::all();

        $data['form'] = [
            [ 'name' => 'name', 'label' => 'Nama Posisi', 'type' => 'text'],
            ['name' => 'division_id', 'label' => 'Divisi', 'type'=>'select', 'option' =>$division],

            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "position.store";
        $data['form_update'] = "position.update";
        // return json_encode($data);
        return View::make('dashboard.position.index', $data);
    }
    public function indexData(Request $request){
        $data = Position::with('division');
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.position.index-action', compact('data'));})
               ->rawColumns(['action'])
               ->make(true); 

   }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'division_id' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $researche = Position::create([
            'name' => $request->name,
            'division_id' => $request->division_id

            ]);

        session()->put('success','Berhasil Input.');   
        return redirect('/position');
        
        // return json_encode($request->all());
    }

    public function destroy($id)
    {
        $data = Position::find($id);
        if($data->count() < 1){
        session()->put('error','Gagal hapus.');
            return redirect('/position');
        };
        $data->delete();
        session()->put('success','Berhasil Hapus.');
            return redirect('/position');
    }
    public function look(Request $request)
    {
       
        $data = Position::findOrFail($request->data);
        return json_encode($data);

    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'division_id' => 'required',
        ]);
        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $data = Position::find($request->id);
        $data->name = $request->name;
        $data->division_id = $request->division_id;
        $data->save();
        session()->put('success','Berhasi edit!');
        return redirect('/position');  
    }
}