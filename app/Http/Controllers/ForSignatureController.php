<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\For_signature;
use App\Signature;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Auth;


class ForSignatureController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->pageTitle = 'Penempatan Tandatangan';
        $this->masterActive = 'active';
        $this->forsignatureActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
        $signature = Signature::where('company_id', Auth::user()->company->id)->where('file', '!=', null)->get();
        // $data = For_signature::with('signature')->get();
        // return json_encode($data);
        $data['form'] = [
            [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
            ['name' => 'signature_id', 'label' => 'Penandatangan', 'type'=>'select2', 'option' =>$signature],

            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "forsignature.store";
        $data['form_update'] = "forsignature.update";
        // return json_encode($data);
        return View::make('dashboard.forsignature.index', $data);
    }
    public function indexData(Request $request){
        $data = For_signature::with('signature');
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.forsignature.index-action', compact('data'));})
               ->rawColumns(['action'])
               ->make(true); 

   }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'signature_id' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $researche = For_signature::create([
            'name' => $request->name,
            'signature_id' => $request->signature_id

            ]);

        session()->put('success','Berhasil Input.');   
        return redirect('/forsignature');
        
        // return json_encode($request->all());
    }

    public function destroy($id)
    {
        $data = For_signature::find($id);
        if($data->count() < 1){
        session()->put('error','Gagal hapus.');
            return redirect('/forsignature');
        };
        $data->delete();
        session()->put('success','Berhasil Hapus.');
            return redirect('/forsignature');
    }
    public function look(Request $request)
    {
       
        $data = For_signature::findOrFail($request->data);
        return json_encode($data);

    }
    public function update(Request $request)
    {
        // return json_encode($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $data = For_signature::find($request->id);
        $data->name = $request->name;
        $data->signature_id = $request->signature_id;
        $data->save();
        session()->put('success','Berhasi edit!');
        return redirect('/forsignature');  
    }
}