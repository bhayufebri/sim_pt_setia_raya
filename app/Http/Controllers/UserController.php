<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Role;
use App\User;
use App\Company;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Hash;



class UserController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('administrator');
        $this->pageTitle = 'User';
        $this->masterActive = 'active';
        $this->userActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;

        $role = Role::all();
        $company = Company::all();

        // $us = User::with('role', 'company')->get();
        // return json_encode($us);

        // $op=[];
        // foreach ($role as $p) {
        //     $op['name'] = $p->name;
        //     $op['id'] = $p->id;
        // }
        // return json_encode($role);
        $data['form'] = [
            [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
            [ 'name' => 'email', 'label' => 'Email', 'type' => 'text'],
            [ 'name' => 'password', 'label' => 'Password', 'type' => 'password'],
            ['name' => 'role_id', 'label' => 'Sebagai', 'type'=>'select', 'option' =>$role],
            ['name' => 'company_id', 'label' => 'Perusahaan', 'type'=>'select', 'option' =>$company]  
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "user.store";
        $data['form_update'] = "user.update";
        // return json_encode($data);
        return View::make('dashboard.user.index', $data);
    }
    public function indexData(Request $request){
        $data = User::with('role','company');
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.user.index-action', compact('data'));})
               ->rawColumns(['action'])
               ->make(true); 

   }

    public function store(Request $request)
    {
        // return json_encode($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'role_id' => 'required',
            'company_id' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $researche = User::create([
            'name' => $request->name,
            'role_id' => $request->role_id,
            'company_id' => $request->company_id,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            ]);

        session()->put('success','Berhasil Input.');   
        return redirect('/user');
        
    }

    public function destroy($id)
    {
        $data = User::find($id);
        if($data->count() < 1){
        session()->put('error','Gagal hapus.');
            return redirect('/user');
        };
        $data->delete();
        session()->put('success','Berhasil Hapus.');
            return redirect('/user');
    }
    public function look(Request $request)
    {
       
        $data = User::findOrFail($request->data);
        return json_encode($data);

    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'company_id' => 'required',
            'role_id' => 'required',
        ]);
        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $data = User::find($request->id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->role_id = $request->role_id;
        $data->company_id = $request->company_id;
        $data->save();
        session()->put('success','Berhasi edit!');
        return redirect('/user');  
    }

}