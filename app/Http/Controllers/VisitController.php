<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Visit;
use App\User;
use App\Partner;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;



class VisitController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->pageTitle = 'Rencana Kunjungan';
        // $this->visitActive = 'active';
        $this->ceklistActive = 'active';

    }
    public function index()
    {
        $this->rencanakunjunganceklistActive = 'active';
        $data = (array)$this;
        $user = User::where('role_id', 4)->get();
        $partner = Partner::all();
        $data['user'] = $user;

        $data['form'] = [
            [ 'name' => 'start', 'label' => 'Tanggal', 'type' => 'date'],
            ['name' => 'user_id', 'label' => 'Pilih Korlap', 'type'=>'select2', 'option' =>$user],
            ['name' => 'partner_id', 'label' => 'Partner', 'type'=>'select2', 'option' =>$partner],

            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "visit.store";
        // return json_encode($data);
        return View::make('dashboard.visit.index', $data);
    }

    public function store(Request $request)
    {
        // return json_encode($request->all());
        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'user_id' => 'required',
            'partner_id' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $date = Carbon::createFromFormat('Y-m-d', $request->start);
        $date = $date->modify('+1 day');
        // return json_encode($date->format("Y-m-d").' 00:00:00');
        // return json_encode($request->start);
        $researche = Visit::create([
            'start' => $request->start,
            'end' => $date->format("Y-m-d").' 00:00:00',
            'company_id' => Auth::user()->company->id,
            'user_id' => $request->user_id,
            'partner_id' => $request->partner_id,
            ]);

        session()->put('success','Berhasil Input.');   
        return redirect('/visit');
        
        // return json_encode($request->all());
    }

    public function indexAjax(Request $request)
    {
        if($request->ajax()) {
             $data = Visit::with('user','partner')->where('company_id', Auth::user()->company->id)->get();
             if (!empty ($request->user_id) ){
                $data = $data->where('user_id', $request->user_id);
            };
             return response()->json($data);
             
        }
    }
    public function destroy(Request $request)
    {
        // return json_encode($request);
        // $data = Complaint::find($request->data);
        // $data->status = '2';
        // $data->save();
        // return json_encode('success');

        $data = Visit::find($request->data);
        $data->delete();
        return json_encode('success');


    }
}