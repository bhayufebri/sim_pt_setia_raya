<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Order;
use App\Order_detail;
use App\Partner;
use App\Item;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Auth;



class BarangMitraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('admin');
        $this->pageTitle = 'Barang Mitra';
        $this->barangmitraActive = 'active';
        // $this->divisionActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
        $partner = Partner::all();
        $data['itemdata'] = Item::all();

        $data['form'] = [
            // [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
            ['name' => 'partner_id', 'label' => 'Partner', 'type'=>'select2', 'option' =>$partner],

            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "barangmitra.store";
        // $data['form_update'] = "division.update";
        // return json_encode($data);
        return View::make('dashboard.barangmitra.index', $data);
    }
    public function store(Request $request)
   {
    //    return json_encode($request->all());
    //    return json_encode(array_sum($request->jumlah_detail));
       $validator = Validator::make($request->all(), [
            // 'description' => 'required',
            'kode' => 'required',
            'nama_barang' => 'required',
            'partner_id' => 'required'
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }

      
        // $jumlah = array_sum($request->jumlah_detail);
        
        // $total =  ($jumlah + $request->ppn) - $request->pph;
        $order = Order::create([
            // 'description' => $request->description,
            // 'expired' => $request->expired,
            'partner_id' => $request->partner_id,
            'company_id' => Auth::user()->company->id,
            'user_id' => Auth::user()->id

            ]);

            // foreach (array_combine($request->nama_tagihan, $request->jumlah_detail) as $item => $detail) {
                // foreach ($request->kode as $detail) {

             for($i = 0; $i < count($request->kode); ++$i){
                Order_detail::create([
                    'kode' => $request->kode[$i],
                    'nama' => $request->nama_barang[$i],
                    'satuan' => $request->satuan[$i],
                    'jumlah' => $request->jumlah[$i],
                    'keterangan' => $request->keterangan[$i],
                    'order_id' => $order->id

                    ]);
            // }
        };
        
        session()->put('success','Berhasil Input.');   
        return back();
        
    }

    public function indexData(Request $request){

        $data = Order::with('partner')->where('user_id', Auth::user()->id);
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.barangmitra.index-action', compact('data'));})
               ->editColumn('created_at', function ($data) {     
                
                return date('d F Y H:i', strtotime($data->created_at));
                })
            ->editColumn('status', function ($data) {     
                // return 'xxxx';
                    if($data->status == 1){
                        return '<span class="badge bg-teal">disetujui</span>';
                    }elseif($data->status == 2){
                        return '<span class="badge bg-pink">ditolak</span>';
                    }else{
                        return '<span class="badge bg-orange">diajukan</span>';
                    };
                        })
               ->rawColumns(['action','created_at','status'])
               ->make(true); 

   }

   public function destroy($id)
    {
        $data = Order::find($id);
        // return json_encode($data);
        if($data == null){
        session()->put('error','Gagal hapus.');
            return back();
        };
        $data->delete();

        $invoice_detail = Order_detail::where('order_id', $data->id)->delete();
        session()->put('success','Berhasil Hapus.');
            return back();
    }

    public function look(Request $request)
    {
        $data['order'] = Order::with('partner')->find($request->data);
       
        $data['detail'] = Order_detail::where('order_id', $request->data)->get();
        // return json_encode($request->all());
        return json_encode($data);

    }

    public function destroy_detail($id)
    {
        $invoice_detail = Order_detail::find($id);
        
        
        if($invoice_detail->count() < 1){
            session()->put('error','Gagal hapus.');
            return back();
        };
        $invoice_detail->delete();
        session()->put('success','Berhasil Hapus.');
            return back();

    }
    public function look_item(Request $request)
    {
       
        $data = Item::findOrFail($request->data);
        return json_encode($data);

    }
}