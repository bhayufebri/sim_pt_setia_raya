<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        // return json_encode($request->all());

        //  $user_sdm = User_sdm::all();
        // return json_encode($user_sdm);
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }


        // if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => 
        //     $request->password], $request->remember)) {
        //         $details = Auth::guard('web')->user();
        //     // return json_encode($details);
            
        //     return redirect()->intended(route('home'));
        // }

        // if (Auth::guard('dosen')->attempt(['email' => $request->email, 'password' => 
        //     $request->password], $request->remember)) {
        //         $details = Auth::guard('dosen')->user();
        //         // return json_encode($details);
        //     // return json_encode('bb');
        //     return redirect()->intended(route('homeDosen'));
        //     }
    // return json_encode('keluar');


        if ($this->attemptLogin($request)) {
        // dd('aa');
        // flash('Selamat datang!')->success();
        session()->put('success','Selamat datang.');

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        // dd('gg');
        // flash('Maap!')->success();
        session()->put('error','Login gagal.');

        return $this->sendFailedLoginResponse($request);
    }
}