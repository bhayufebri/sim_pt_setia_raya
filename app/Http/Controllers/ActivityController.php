<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Activity;
use DataTables;
use Illuminate\Support\Facades\Auth;

class ActivityController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->pageTitle = 'Activity';
        $this->activityActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
       
        // return json_encode($data);
        return View::make('dashboard.activity.index', $data);
    }
    public function indexData(Request $request){
        $data = Activity::all();
         return Datatables::of($data)
            //    ->editColumn('action', function($data){ return view('dashboard.division.index-action', compact('data'));})
                ->editColumn('created_at', function ($data) {     
                    
                    return date('d F Y H:i:s', strtotime($data->created_at));
                    })

                ->editColumn('action', function ($data) {     
                
                    return '<button type="button" value="'.$data->id.'"
                    class="btn bg-orange waves-effect btn-xs proses" data-toggle="modal" data-target="#ModalDetail"><i class="material-icons">search</i></button>';
                    })
               ->rawColumns(['created_at','action'])

               ->make(true); 

   }
   public function look(Request $request)
    {
       
        $data = Activity::findOrFail($request->data);
        return json_encode($data);

    }

}