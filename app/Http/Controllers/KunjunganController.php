<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Master;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Schedule;
use App\Visit;
use App\Partner;
use Carbon\Carbon;


class KunjunganController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('korlap');
        // $this->dataActive = 'active';
    }
    public function index()
    {
        $this->kunjunganActive = 'active';
        $this->pageTitle = 'Hasil Kunjungan';
        $data = (array)$this;
        
        return View::make('dashboard.kunjungan.index', $data);
    }

    public function indexAjax(Request $request)
    {
  
        // $data = Schedule::get(['id', 'title as mboh', 'start', 'end']);
        // return json_encode($data);
        // Log::info('masuk'. json_encode($request->all()));
        if($request->ajax()) {
       
             $data = Master::with('partner','group')->where('user_id', Auth::user()->id)
                    //    ->get(['id', 'title', 'start', 'end', DB::raw('CONCAT(start_plot,"-",end_plot) as description')]);
                       ->get();
                       
  
             return response()->json($data);
        }
    }
    public function visit()
    {
        $this->visitkunjunganActive = 'active';
        $this->pageTitle = 'Rencana Kunjungan';
        $data = (array)$this;
        $partner = Partner::all();
     

        $data['form'] = [
            [ 'name' => 'start', 'label' => 'Tanggal', 'type' => 'date'],
            ['name' => 'partner_id', 'label' => 'Partner', 'type'=>'select2', 'option' =>$partner],

            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "kunjungan.store";
        
        return View::make('dashboard.kunjungan.visit', $data);
    }

    public function visitAjax(Request $request)
    {
        if($request->ajax()) {
             $data = Visit::with('user','partner')->where('company_id', Auth::user()->company->id)->where('user_id', Auth::user()->id)->get();
             return response()->json($data);
        }
    }

    public function store(Request $request)
    {
        // return json_encode($request->all());
        $validator = Validator::make($request->all(), [
            'start' => 'required',
            'partner_id' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $date = Carbon::createFromFormat('Y-m-d', $request->start);
        $date = $date->modify('+1 day');
        // return json_encode($date->format("Y-m-d").' 00:00:00');
        // return json_encode($request->start);
        $researche = Visit::create([
            'start' => $request->start,
            'end' => $date->format("Y-m-d").' 00:00:00',
            'company_id' => Auth::user()->company->id,
            'user_id' => Auth::user()->id,
            'partner_id' => $request->partner_id,
            ]);

        session()->put('success','Berhasil Input.');   
        return back();
        
        // return json_encode($request->all());
    }
    public function destroy(Request $request)
    {
        $data = Visit::find($request->data);
        $data->delete();
        return json_encode('success');


    }
}