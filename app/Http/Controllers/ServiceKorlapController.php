<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Complaint;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Str;
use PDF;

class ServiceKorlapController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('admin');
        $this->middleware('korlap');
        $this->pageTitle = 'Service';
        $this->servicekorlapActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
       
        // return json_encode($data);
        return View::make('dashboard.servicekorlap.index', $data);
    }
    public function indexData(Request $request){
        $data = Complaint::with('partner')->where('company_id', Auth::user()->company->id);
         return Datatables::of($data)
                ->editColumn('created_at', function ($data) {     
                
                return date('d F Y', strtotime($data->created_at));
                })
                ->editColumn('partner.name', function ($data) {     
                
                    return '<span class="badge bg-cyan">'.$data->partner->name.'</span>';
                    })
               
                    
                ->editColumn('status', function ($data) {     
                    if($data->status == 1){
                        return '<span class="badge bg-orange">Sedang Proses</span>';
                    }elseif($data->status == 2){
                        return '<span class="badge bg-teal">Selesai</span>';
                    }else{
                        return '<span class="badge bg-pink">Pengaduan</span>';
                    };
                        })

                ->rawColumns(['created_at', 'partner.name','status'])
               ->make(true); 

   }
}