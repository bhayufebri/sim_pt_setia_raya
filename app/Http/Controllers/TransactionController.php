<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Transaction;
use App\Salary;
use App\Invoice;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Auth;


class TransactionController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('akuntan');
        $this->pageTitle = 'Transaksi';
        $this->akuntansiActive = 'active';
    }
    public function index()
    {
        // $data = Invoice::with('partner')
        //                     ->whereHas('partner', function ($query) {
        //                         $query->where('company_id', Auth::user()->company->id);
        //                         })
        //                     // ->where('partner.company_id', Auth::user()->company->id)
        //                     ->get();
        // return json_encode($data);

        $this->transaksiActive = 'active';
        $data = (array)$this;
        $data['form'] = [
            [ 'name' => 'description', 'label' => 'Nama Transaksi', 'type' => 'text'],
            [ 'name' => 'jumlah', 'label' => 'Jumlah', 'type' => 'number'],
            ['name' => 'jenis', 'label' => 'Jenis', 'type'=>'select', 'option' => [
                ['id'=>'debet', 'name'=>'Debet/Pemasukan'],
                ['id'=>'kredit', 'name'=>'Kredit/Pengeluaran']
                ]  
            ],
            ['name' => 'is_cash', 'label' => 'Tipe', 'type'=>'select', 'option' => [
                ['id'=>'1', 'name'=>'Cash'],
                ['id'=>'0', 'name'=>'Transfer']
                ]  
            ]
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "transaction.store";
        $data['form_update'] = "transaction.update";
        return View::make('dashboard.transaction.index', $data);

    }

    public function indexData(Request $request){
        $data = Transaction::where('company_id', Auth::user()->company->id);
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.transaction.index-action', compact('data'));})
               ->editColumn('description', function ($data) { 
                if($data->is_cash == '1'){
                            // return 'Rp. '.number_format($data->jumlah,0,",",".");
                            return $data->description.' <span class="badge bg-cyan">Cash</span>';
                        }else{
                            return $data->description.' <span class="badge bg-orange">TF</span>';
                        }             
                    })
                ->editColumn('debet', function ($data) { 
                        if($data->jenis == 'debet'){
                                    // return 'Rp. '.number_format($data->jumlah,0,",",".");
                                    return $data->jumlah;
                                }                
                            })
                ->editColumn('kredit', function ($data) { 
                    if($data->jenis == 'kredit'){
                                // return 'Rp. '. number_format($data->jumlah,0,",",".");
                                return $data->jumlah;
                            }                
                        })
                ->editColumn('created_at', function ($data) { 
                    
                                return date('d F Y', strtotime($data->created_at));
                                           
                        })
               ->rawColumns(['action', 'debet','kredit','created_at','description'])
               ->make(true); 

   }
   public function dataPenggajian(Request $request){
    $data = Salary::with('user')->where('company_id', Auth::user()->company->id);
     return Datatables::of($data)
           
           ->editColumn('debet', function ($data) { 
            
                        return '-';
                    
                })
            ->editColumn('kredit', function ($data) { 
                
                            // return 'Rp. '. number_format($data->total_gaji,0,",",".");
                            return $data->total_gaji;
   })
            ->editColumn('created_at', function ($data) { 
                
                            return date('d F Y', strtotime($data->created_at));
                                       
                    })
           ->rawColumns(['debet','kredit','created_at'])
           ->make(true); 

}

public function dataInvoice(Request $request){
    $data = Invoice::with('partner')->whereHas('partner', function ($query) {
        $query->where('company_id', Auth::user()->company->id);
        })->where('status', 2);
     return Datatables::of($data)
           
           ->editColumn('debet', function ($data) { 
            
                        // return '-';
                        // return 'Rp. '. number_format($data->jumlah,0,",",".");
                        return $data->jumlah;
                    
                })
            ->editColumn('kredit', function ($data) { 
                return '-';
                
                            // return 'Rp. '. number_format($data->total_gaji,0,",",".");
   })
            ->editColumn('created_at', function ($data) { 
                
                            return date('d F Y', strtotime($data->created_at));
                                       
                    })
           ->rawColumns(['debet','kredit','created_at'])
           ->make(true); 

}

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jumlah' => 'required',
            'description' => 'required',
            'jenis' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $researche = Transaction::create([
            'jumlah' => $request->jumlah,
            'description' => $request->description,
            'jenis' => $request->jenis,
            'is_cash' => $request->is_cash,
            'company_id' => Auth::user()->company->id

            ]);

        session()->put('success','Berhasil Input.');   
        return redirect('/transaction');
        
        // return json_encode($request->all());
    }

    public function destroy($id)
    {
        $data = Transaction::find($id);
        if($data->count() < 1){
        session()->put('error','Gagal hapus.');
            return redirect('/transaction');
        };
        $data->delete();
        session()->put('success','Berhasil Hapus.');
            return redirect('/transaction');
    }
    public function look(Request $request)
    {
       
        $data = Transaction::findOrFail($request->data);
        return json_encode($data);

    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jumlah' => 'required',
            'description' => 'required',
            'jenis' => 'required',
        ]);
        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $data = Transaction::find($request->id);
        $data->jumlah = $request->jumlah;
        $data->description = $request->description;
        $data->jenis = $request->jenis;
        $data->is_cash = $request->is_cash;
        $data->save();
        session()->put('success','Berhasi edit!');
        return redirect('/transaction');  
    }
}