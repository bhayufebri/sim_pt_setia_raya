<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partner;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Providers\RouteServiceProvider;




class MitraController extends Controller
{

    use AuthenticatesUsers;
    protected $redirectTo = RouteServiceProvider::HOME;

    public function index()
    {
        // $data = (array)$this;
        // $data['form'] = [
        //     [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
        //     [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
        //     // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        // ];
        // $data['form_action'] = "partner.store";
        // $data['form_update'] = "partner.update";
        $data['partner'] = Partner::all();

        // return json_encode($data);
        return View::make('dashboard.mitra.index', $data);
    }

    public function login(Request $request)
    {
        // return json_encode($request->all());

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }


        if (Auth::guard('mitra')->attempt(['id' => $request->email, 'password' => 
            $request->password], $request->remember)) {
                $details = Auth::guard('mitra')->user();
            // return json_encode($details);
            
            return redirect()->intended(route('dashboard.index'));
        }



        // if (Auth::guard('mitra')->attempt(['id' => $request->email, 'password' => 
        //     $request->password])) {
        //     // return redirect()->intended(route('admin.dashboard'));

        //     return json_encode('masuk');
        //     };
        session()->put('error','User tidak benar.');
            return back();



        // $data = [
        //     'id' => $request->email,
        //     'password' => $request->password,
        // ];

        // if (Auth::Attempt($data)) {
        //     // return redirect('home');
        // }else{
        //     // Session::flash('error', 'Email atau Password Salah');
        //     return back();
        // }


        // $data = Partner::findOrFail($request->data);

    }
    public function logout(Request $request)
    {
        Auth::logout();
 
        request()->session()->invalidate();
 
        request()->session()->regenerateToken();
 
        return redirect('/mitra');
    }
}