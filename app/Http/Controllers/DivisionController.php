<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Division;
use Illuminate\Support\Facades\Validator;
use DataTables;

class DivisionController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->pageTitle = 'Division';
        $this->masterActive = 'active';
        $this->divisionActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
        $data['form'] = [
            [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "division.store";
        $data['form_update'] = "division.update";
        // return json_encode($data);
        return View::make('dashboard.division.index', $data);
    }
    public function indexData(Request $request){
        $data = Division::all();
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.division.index-action', compact('data'));})
               ->rawColumns(['action'])
               ->make(true); 

   }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            // 'description' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $researche = Division::create([
            'name' => $request->name,
            // 'description' => $request->description

            ]);

        session()->put('success','Berhasil Input.');   
        return redirect('/division');
        
        // return json_encode($request->all());
    }

    public function destroy($id)
    {
        $data = Division::find($id);
        if($data->count() < 1){
        session()->put('error','Gagal hapus.');
            return redirect('/division');
        };
        $data->delete();
        session()->put('success','Berhasil Hapus.');
            return redirect('/division');
    }
    public function look(Request $request)
    {
       
        $data = Division::findOrFail($request->data);
        return json_encode($data);

    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $data = Division::find($request->id);
        $data->name = $request->name;
        // $data->description = $request->description;
        $data->save();
        session()->put('success','Berhasi edit!');
        return redirect('/division');  
    }
}