<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Order;
use App\Order_detail;
use App\Partner;
use App\Item;
use Illuminate\Support\Facades\Validator;
use DataTables, PDF;
use Illuminate\Support\Facades\Auth;
use App\For_signature;


class LogistikController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('logistik');
        $this->pageTitle = 'Permintaan Barang';
        $this->logistikActive = 'active';
        // $this->divisionActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
        // $partner = Partner::all();

        $data['form'] = [
            [ 'name' => 'code', 'label' => 'Kode', 'type' => 'text'],
            [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
            [ 'name' => 'satuan', 'label' => 'Satuan', 'type' => 'text'],
            // ['name' => 'partner_id', 'label' => 'Partner', 'type'=>'select', 'option' =>$partner],

            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "logistik.store";
        $data['form_update'] = "logistik.update";
        // return json_encode($data);
        return View::make('dashboard.logistik.index', $data);
    }
    public function indexData(Request $request){

        $data = Order::with('partner','user');
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.logistik.index-action', compact('data'));})
               ->editColumn('created_at', function ($data) {     
                
                return date('d F Y H:i', strtotime($data->created_at));
                })
                ->editColumn('status', function ($data) {     
                if($data->status == 1){
                    return '<span class="badge bg-teal">disetujui</span>';
                }elseif($data->status == 2){
                    return '<span class="badge bg-pink">ditolak</span>';
                }else{
                    return '<span class="badge bg-orange">diajukan</span>';
                };
                    })
               ->rawColumns(['action','created_at','status'])
               ->make(true); 

   }

   public function dataItem(Request $request){

    $data = Item::all();
     return Datatables::of($data)
           ->editColumn('action', function($data){ return view('dashboard.logistik.action', compact('data'));})
           
           ->rawColumns(['action'])
           ->make(true); 

}
   public function cetak($id){

    // $data['id'] = $id;
    // $data['salary'] = Salary::with('user:name,id', )findOrFail($id);

    $data['order'] = Order::with('partner', 'user')->find($id);
       
    $data['detail'] = Order_detail::where('order_id', $id)->get();


    $data['company'] = strtoupper(Auth::user()->company->name);
    $data['user'] = strtoupper(Auth::user()->name);
    $data['admin'] = 'Irawan Pramudhita, SE';

    // return json_encode($data);
   

        $data['signature'] = For_signature::with('signature')->where('name', 'Cek List Security')->first();
    


    $pdf = PDF::loadView('dashboard.logistik.cetak', $data)->setPaper('a4', 'potrait');
        return $pdf->stream('logistik.pdf');
        // return $pdf->download('ceklist_harian.pdf');

   }

   public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
            'satuan' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $researche = Item::create([
            'name' => $request->name,
            'code' => $request->code,
            'satuan' => $request->satuan,
            'company_id' => Auth::user()->company->id


            ]);

        session()->put('success','Berhasil Input.');   
        return back();
        
        // return json_encode($request->all());
    }

    public function destroy($id)
    {
        $data = Item::find($id);
        if($data->count() < 1){
        session()->put('error','Gagal hapus.');
            return back();
        };
        $data->delete();
        session()->put('success','Berhasil Hapus.');
            return back();
    }
    public function look(Request $request)
    {
       
        $data = Item::findOrFail($request->data);
        return json_encode($data);

    }
    public function setujui(Request $request)
    {
        // return json_encode($request);
        $data = Order::find($request->data);
        $data->status = '1';
        $data->save();
        return json_encode('success');

    }
    public function tolak(Request $request)
    {
        // return json_encode($request);
        $data = Order::find($request->data);
        $data->status = '2';
        $data->save();
        return json_encode('success');

    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'code' => 'required',
        ]);
        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $data = Item::find($request->id);
        $data->name = $request->name;
        $data->code = $request->code;
        $data->satuan = $request->satuan;
        $data->save();
        session()->put('success','Berhasi edit!');
        return back();  
    }
}