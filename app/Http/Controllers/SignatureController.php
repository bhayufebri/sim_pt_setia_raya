<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Signature;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Str;



class SignatureController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->pageTitle = 'Signature';
        $this->masterActive = 'active';
        $this->signatureActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
        $data['form'] = [
            [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
            [ 'name' => 'jabatan', 'label' => 'Jabatan', 'type' => 'text'],
            [ 'name' => 'file', 'label' => 'File', 'type' => 'file'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action_upload'] = "signature.store";
        $data['form_update_upload'] = "signature.update";
        // return json_encode($data);
        return View::make('dashboard.signature.index', $data);
    }
    public function indexData(Request $request){
        $data = Signature::all();
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.signature.index-action', compact('data'));})
               ->rawColumns(['action'])
               ->make(true); 

   }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'jabatan' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }

        if($request->hasFile('file')) {
            //    return json_encode('masuk');
                        // Upload path
                        $destinationPath = 'signature_file/';
                 
                        // Get file extension
                        $extension = $request->file('file')->getClientOriginalExtension();
                 
                        // Valid extensions
                        $validextensions = array("jpeg","jpg","png");
                 
                        // Check extension
                        if(in_array(strtolower($extension), $validextensions)){
              
                          do{
                              $dokumen = Str::random(60);
                              $cekuniq =  Signature::where('file', $dokumen.'.'.$extension);
                          }while(empty($cekuniq)); 
                 
                          // Rename file 
                          // $fileName = $request->file('file')->getClientOriginalName().time() .'.' . $extension;
                          $fileName = $request->file('file')->getClientOriginalName();
                          $fileReal = $dokumen . '.' . $extension;
                          // Uploading file to given path
                          $request->file('file')->move($destinationPath, $fileReal); 
                          
              
            
            
                        }
                 
                      }else{
                        $fileReal = null;
                      }


        $researche = Signature::create([
            'name' => $request->name,
            'jabatan' => $request->jabatan,
            'file' => $fileReal,
            'company_id' => Auth::user()->company->id

            ]);

        session()->put('success','Berhasil Input.');   
        return redirect('/signature');
        
        // return json_encode($request->all());
    }

    public function destroy($id)
    {
        $data = Signature::find($id);
        if($data->count() < 1){
        session()->put('error','Gagal hapus.');
            return redirect('/signature');
        };
        $data->delete();
        session()->put('success','Berhasil Hapus.');
            return redirect('/signature');
    }
    public function look(Request $request)
    {
       
        $data = Signature::findOrFail($request->data);
        return json_encode($data);

    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        if($request->hasFile('file')) {
            //    return json_encode('masuk');
                        // Upload path
                        $destinationPath = 'signature_file/';
                 
                        // Get file extension
                        $extension = $request->file('file')->getClientOriginalExtension();
                 
                        // Valid extensions
                        $validextensions = array("jpeg","jpg","png");
                 
                        // Check extension
                        if(in_array(strtolower($extension), $validextensions)){
              
                          do{
                              $dokumen = Str::random(60);
                              $cekuniq =  Signature::where('file', $dokumen.'.'.$extension);
                          }while(empty($cekuniq)); 
                 
                          // Rename file 
                          // $fileName = $request->file('file')->getClientOriginalName().time() .'.' . $extension;
                          $fileName = $request->file('file')->getClientOriginalName();
                          $fileReal = $dokumen . '.' . $extension;
                          // Uploading file to given path
                          $request->file('file')->move($destinationPath, $fileReal); 
                          
              
            
            
                        }
                 
                      }else{
                        $fileReal = null;
                      }
        $data = Signature::find($request->id);
        $data->name = $request->name;
        $data->jabatan = $request->jabatan;
        $data->file = $fileReal;
        $data->save();
        session()->put('success','Berhasi edit!');
        return redirect('/signature');  
    }
}