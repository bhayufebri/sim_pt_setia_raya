<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use App\Partner;
use App\Schedule;
use App\User;
use App\Invoice;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Str;
use DB;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\URL;
use App\Imports\ImportSchedule;
use DateTime;
use DatePeriod;
use DateInterval;
use Log;
use PDF;
use App\For_signature;
use App\Complaint;
use Carbon\Carbon;



class MitraDashboardController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('mitra');
        // $this->pageTitle = 'Partner';
        // $this->masterActive = 'active';
        // $this->partnerActive = 'active';
    }
    public function index()
    {
        $this->pageTitle = 'Home';
        $this->homeActive = 'active';
        $data = (array)$this;
        $data['form'] = [
            [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
            [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        $data['form_action'] = "partner.store";
        $data['form_update'] = "partner.update";
        // return json_encode($data);
        return View::make('dashboard.mitradashboard.index', $data);
    }
    public function indexData(Request $request){
        $data = Invoice::where('partner_id', Auth::guard('mitra')->user()->id);
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.mitradashboard.index-action', compact('data'));})
               ->rawColumns(['action'])
               ->make(true); 

   }
   public function tagihan()
    {
        $this->pageTitle = 'Tagihan';
        $this->tagihanActive = 'active';
        $data = (array)$this;
        $data['form'] = [
            [ 'name' => 'file', 'label' => 'Berkas', 'type' => 'file'],
            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        ];
        // $data['form_action'] = "partner.store";
        // $data['form_update'] = "partner.update";
        $data['form_update_upload'] = "dashboard.upload";
        // return json_encode($data);
        return View::make('dashboard.mitradashboard.tagihan', $data);
    }
    public function look(Request $request)
    {
       
        $data = Invoice::findOrFail($request->data);
        return json_encode($data);

    }
    public function upload(Request $request)
    {
        // return json_encode($request->all());
        if($request->hasFile('file')) {
            //    return json_encode('masuk');
                        // Upload path
                        $destinationPath = 'invoice_file/';
                 
                        // Get file extension
                        $extension = $request->file('file')->getClientOriginalExtension();
                 
                        // Valid extensions
                        $validextensions = array("jpeg","jpg","png");
                 
                        // Check extension
                        if(in_array(strtolower($extension), $validextensions)){
              
                          do{
                              $dokumen = Str::random(60);
                              $cekuniq =  Invoice::where('file', $dokumen.'.'.$extension);
                          }while(empty($cekuniq)); 
                 
                          // Rename file 
                          // $fileName = $request->file('file')->getClientOriginalName().time() .'.' . $extension;
                          $fileName = $request->file('file')->getClientOriginalName();
                          $fileReal = $dokumen . '.' . $extension;
                          // Uploading file to given path
                          $request->file('file')->move($destinationPath, $fileReal); 
                          
              
            
                          $data = Invoice::find($request->id);
                            $data->file = $fileReal;
                            $data->status = 1;
                            $data->save();
                            session()->put('success','Berhasi upload!');
                            return back(); 
            
                        }
                 
                      }
            
            
            
            
                        session()->put('error','Input gagal.');
                        return back();
    }
    public function cetak_pdf($id){

        $data['invoice'] = Invoice::with('partner.company')->findOrFail($id);
        // return json_encode(Auth::guard('mitra')->user()->company->name);
        $data['company'] = strtoupper($data['invoice']->partner->company->name);
        $data['description'] = strtoupper(Auth::guard('mitra')->user()->company->description);
        $data['admin'] = 'Irawan Pramudhita, SE';
        $data['signature'] = For_signature::with('signature')->where('name', 'Invoice')->first();
        $data['rekening'] = env('REKENING');
    
    
        $pdf = PDF::loadView('dashboard.invoice.cetak-prev', $data)->setPaper('a4', 'potrait');
            return $pdf->stream('invoice.pdf');
            // return $pdf->download('invoice.pdf');
    
       }
       public function pegawai()
    {
        $this->pageTitle = 'Pegawai';
        $this->pegawaiActive = 'active';
        $data = (array)$this;
        // $data['form'] = [
        //     [ 'name' => 'file', 'label' => 'Berkas', 'type' => 'file'],
            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        // ];
        // $data['form_action'] = "partner.store";
        // $data['form_update'] = "partner.update";
        // $data['form_update_upload'] = "dashboard.upload";
        // return json_encode($data);
        return View::make('dashboard.mitradashboard.pegawai', $data);
    }
    public function dataPegawai(Request $request){
   

        // $data = User::with('role','company','partner')->where('role_id', $us[0]->id)->where('company_id', Auth::user()->company->id);
        $data = User::with('company','partner','division')->where('partner_id', Auth::guard('mitra')->user()->id);
        
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.member.index-action', compact('data'));})
               ->rawColumns(['action'])
               ->make(true); 

   }

   public function contact()
    {
        $this->pageTitle = 'Contact Service';
        $this->contactActive = 'active';
        $data = (array)$this;
        // $data['form'] = [
        //     [ 'name' => 'file', 'label' => 'Berkas', 'type' => 'file'],
            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        // ];
        // $data['form_action'] = "partner.store";
        // $data['form_update'] = "partner.update";
        // $data['form_update_upload'] = "dashboard.upload";
        
        // return json_encode(Auth::guard('mitra')->user()->company->id);
        return View::make('dashboard.mitradashboard.contact', $data);
    }

    public function contact_store(Request $request)
    {
        // return json_encode(Auth::guard('mitra')->user());
        
        $validator = Validator::make($request->all(), [
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $researche = Complaint::create([
            'partner_id' => Auth::guard('mitra')->user()->id,
            'company_id' => Auth::guard('mitra')->user()->company->id,
            'description' => $request->description
            ]);

        session()->put('success','Berhasil Input.');   
        return back();
    }

    public function jadwal()
    {
        $this->pageTitle = 'Jadwal';
        $this->jadwalActive = 'active';
        $data = (array)$this;
        $data['member'] = User::where('partner_id', Auth::guard('mitra')->user()->id)->where('is_active', 1)->get();
        // return json_encode(Auth::guard('mitra')->user());
        if(Auth::guard('mitra')->user()->is_premium == 1){
            return View::make('dashboard.mitradashboard.jadwal2', $data);
        }else{
            return View::make('dashboard.mitradashboard.jadwal', $data);
        };
    }

    public function indexAjax(Request $request)
    {
  
        // $data = Schedule::get(['id', 'title as mboh', 'start', 'end']);
        // return json_encode($data);
        // Log::info('masuk'. json_encode($request->all()));
        if($request->ajax()) {
       
             $data = Schedule::with('user')->where('partner_id', Auth::guard('mitra')->user()->id)
                    //    ->get(['id', 'title', 'start', 'end', DB::raw('CONCAT(start_plot,"-",end_plot) as description')]);
                    //    ->get(['id','user_id','title', 'start', 'end', 'start_plot as description','end_plot as end_time']);
                       ->get();
                       if (!empty ($request->user_id) ){
                        $data = $data->where('user_id', $request->user_id);
                    };
  
             return response()->json($data);
        }
    }

    public function indexAjax2(Request $request)
    {

        if($request->ajax()) {
       
             $data = Schedule::with('user')->where('partner_id', Auth::guard('mitra')->user()->id)->whereDate('start', '>=', $request->start)
                       ->whereDate('end',   '<=', $request->end)
                    //    ->get(['id', 'title', 'start', 'end', DB::raw('CONCAT(start_plot,"-",end_plot) as description')]);
                    //    ->get(['id','user_id','title', 'start', 'end', 'start_plot as description','end_plot as end_time','start_absen','end_absen']);
                       ->get();
                       if (!empty ($request->user_id) ){
                        $data = $data->where('user_id', $request->user_id);
                    };
  
             return response()->json($data);
        }
    }

    public function dataAbsen(Request $request){
       
        $data = Schedule::with('user.division')->where('partner_id', Auth::guard('mitra')->user()->id)->whereHas('user', function ($query) {
            $query->where('company_id', Auth::guard('mitra')->user()->company->id);
            });
                            // ->whereNotNull('start_absen');

        
        if (!empty ($request->get('user_id')) ){
            $data = $data->where('user_id', $request->get('user_id'));
        };


        if (!empty ($request->get('date')) ){
            // Log::info(json_encode($request->get('date')));
            $date = explode(' - ', $request->get('date'));
            $start = $date[0];
            $end = $date[1];
            $data = $data->whereBetween('start', [$start, $end]);
            // ->whereBetween('created_at', [$start, $end])
        };
        
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.slip.index-action', compact('data'));})
               ->editColumn('start_plot', function ($data) {     
                return date('d F Y H:i', strtotime($data->start_plot))." - ".date('d F Y H:i', strtotime($data->end_plot));
                   
                    })

                ->editColumn('start_absen', function ($data) {    
                    if($data->start_absen != null) {
                        return date('d F Y H:i', strtotime($data->start_absen));
                    }else{
                        return '-';
                    }
                           
                            })

                ->editColumn('end_absen', function ($data) {   
                    if($data->end_absen != null) {
                                return date('d F Y H:i', strtotime($data->end_absen));
                    }else{
                        return '-';
                    }
                })

                ->editColumn('lembur', function ($data) {  
                    
                    
                    if($data->end_plot < $data->end_absen){
                        $plot = Carbon::parse($data->end_plot);
                        $absen = Carbon::parse($data->end_absen);
                        $lembur = $plot->diff($absen)->format('%H Jam, %I Menit');
                        return $lembur;
                    }else{
                        return '-';
                    }
        

                                    })
               ->rawColumns(['action','start_plot', 'start_absen', 'end_absen', 'lembur'])
               ->make(true); 

   }
   public function cetak_bill($id){

    $data['invoice'] = Invoice::with('partner', 'invoice_detail')->findOrFail($id);
    $data['company'] = strtoupper(Auth::user()->company->name);
    $data['description'] = strtoupper(Auth::user()->company->description);
    $data['admin'] = 'Irawan Pramudhita, SE';
    $data['signature'] = For_signature::with('signature')->where('name', 'Invoice')->first();
    $data['rekening'] = env('REKENING');
    $angka = $this->penyebut($data['invoice']['total']).' RUPIAH';
    $data['angka'] = strtoupper($angka);



    // return json_encode($data['invoice']);
    // return ;


    $pdf = PDF::loadView('dashboard.invoice.cetak-bill', $data)->setPaper('a4', 'potrait');
        return $pdf->stream('invoice.pdf');
        // return $pdf->download('invoice.pdf');

   }
   public function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
        $temp = $this->penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
        $temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . $this->penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . $this->penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
}
public function serviceData(Request $request){
    $data = Complaint::with('partner')->where('partner_id', Auth::guard('mitra')->user()->id)->where('company_id', Auth::guard('mitra')->user()->company->id);
     return Datatables::of($data)
            ->editColumn('created_at', function ($data) {     
            
            return date('d F Y', strtotime($data->created_at));
            })
            ->editColumn('partner.name', function ($data) {     
            
                return '<span class="badge bg-cyan">'.$data->partner->name.'</span>';
                })
          
                
            ->editColumn('status', function ($data) {     
                if($data->status == 1){
                    return '<span class="badge bg-orange">Sedang Proses</span>';
                }elseif($data->status == 2){
                    return '<span class="badge bg-teal">Selesai</span>';
                }else{
                    return '<span class="badge bg-pink">Pengaduan</span>';
                };
                    })

            ->rawColumns(['created_at', 'partner.name', 'status'])
           ->make(true); 

}

}