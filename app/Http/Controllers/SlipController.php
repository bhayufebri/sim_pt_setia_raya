<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Period;
use Illuminate\Support\Facades\View;
use App\User;
use App\Partner;
use App\Role;
use App\Salary;
use App\Variable;
use App\Component;
use App\Detail;
use App\For_signature;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Schedule;
use PDF;

class SlipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
      
    }
    
    public function index()
    {
        $this->pageTitle = 'Slip Gaji';
        $this->slipActive = 'active';
        // $this->penggajianSetActive = 'active';
        $data = (array)$this;
        // $ss = Salary::all();
        // $data = Salary::with('period')->where('user_id', Auth::user()->id)->get();

        // return json_encode($data);
        // $data['partner'] = Partner::all();
        // $data['variable_penambah'] = Variable::where('description', 'Penambah')->get();
        // $data['variable_pengurang'] = Variable::where('description', 'Pengurang')->get();
        // $data['form'] = [
        //     [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
        //     [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
        //     // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
        // ];
        // $data['form_action'] = "partner/store";
        // $data['form_update'] = "partner/update";
        // return json_encode($data);
        return View::make('dashboard.slip.index', $data);
    }

    public function indexData(Request $request){
       
        $data = Salary::with('period')->where('user_id', Auth::user()->id)->whereHas('period');
        
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.slip.index-action', compact('data'));})
               
               ->rawColumns(['action'])
               ->make(true); 

   }

   public function cetak_xxx($id){

       // $data['id'] = $id;
       // $data['salary'] = Salary::with('user:name,id', )findOrFail($id);
    //    $ss = Salary::findOrFail($id);

    //    return json_encode($ss);
       $data['salary'] = Salary::with('user:name,id', 'period' )->findOrFail($id);
       $data['detail_penambah'] = Detail::where('salary_id', $id)->where('sifat', 'Penambah')->get();
       $data['detail_pengurang'] = Detail::where('salary_id', $id)->where('sifat', 'Pengurang')->get();
       $data['uraian'] = json_decode($data['salary']['uraian']);
       $data['company'] = strtoupper(Auth::user()->company->name);
       $data['admin'] = 'Irawan Pramudhita, SE';
       $data['signature'] = For_signature::with('signature')->where('name', 'Penggajian')->first();


    $pdf = PDF::loadView('dashboard.penggajian.cetak-prev', $data)->setPaper('a4', 'potrait');
        // return $pdf->stream();
        return $pdf->download('tes.pdf');

   }

   public function jadwal()
    {
        $this->jadwalActive = 'active';

        $this->pageTitle = 'Jadwal';
        // $this->penggajianSetActive = 'active';
        $data = (array)$this;
        
        return View::make('dashboard.slip.jadwal', $data);
    }

    public function indexAjax(Request $request)
    {
  
        // $data = Schedule::get(['id', 'title as mboh', 'start', 'end']);
        // return json_encode($data);
        // Log::info('masuk'. json_encode($request->all()));
        if($request->ajax()) {
       
             $data = Schedule::where('user_id', Auth::user()->id)
                    //    ->get(['id', 'title', 'start', 'end', DB::raw('CONCAT(start_plot,"-",end_plot) as description')]);
                       ->get(['id','user_id','title', 'start', 'end', 'start_plot as description','end_plot as end_time']);
                       
  
             return response()->json($data);
        }
    }
}