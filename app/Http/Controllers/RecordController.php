<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Group;
use App\Question;
use App\Partner;
use App\Master;
use App\Answer;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\For_signature;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;




class RecordController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('korlap');
        $this->recordActive = 'active';
        // $this->dataActive = 'active';
    }
    public function index()
    {
        $this->pageTitle = 'Ceklist';
        $data = (array)$this;
        $data['group'] = Group::all();
        $data['partner'] = Partner::where('company_id', Auth::user()->company->id)->get();

        // $data['form'] = [
        //     [ 'name' => 'title', 'label' => 'Nama', 'type' => 'text'],
        //     // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
        //     ['name' => 'is_active', 'label' => 'Status', 'type'=>'select', 'option' => [
        //         ['id'=>1, 'name'=>'Aktif'],
        //         ['id'=>0, 'name'=>'Non-Aktif']
        //         ]  
        //     ]
        // ];
        // $data['form_action'] = "ceklist.store";
        // $data['form_update'] = "ceklist.update";
        // $data['form_delete'] = "ceklist.destroyQuestion";
        // $division= Auth::user()->division->id;
        // $data['group'] = Group::with('division')->where('is_active', 1)->where('division_id', $division)->get();
        // $master = Master::with('partner', 'group')->whereHas('group', function ($query) {
        //     $query->where('division_id', Auth::user()->division->id);
        //     })->get();
        // return json_encode($master);
        return View::make('dashboard.record.index', $data);
    }
    public function indexData(Request $request){
        $division= Auth::user()->division->id;
        $data = Group::with('division')->where('is_active', 1)->where('division_id', $division);
         return Datatables::of($data)
               
               ->editColumn('title', function ($data) { 
                
                    if($data->is_active == 1){
                        return "<a class='btn btn-primary' href='".route('record.nilai', $data->id)."' >"
                                .$data->title.
                            "</a>";
                        }else{
                            return "<a class='btn btn-default' href='#' >"
                            .$data->title.
                            "</a>";
                        }
                })
               ->rawColumns(['title'])
               ->make(true); 

   }
   public function masterData(Request $request){
   

    $data = Master::with('partner', 'group')->whereHas('group', function ($query) {
        $query->where('division_id', Auth::user()->division->id);
        });


        // $data = Master::with('user:name,id', 'partner:id,name,company_id', 'group.question' )->whereHas('partner', function ($query) {
        //     $query->where('company_id', Auth::user()->company->id);
        //     });
        if (!empty ($request->get('group_id')) ){
            $data = $data->where('group_id', $request->get('group_id'));
        };
        if (!empty ($request->get('partner_id')) ){
            $data = $data->where('partner_id', $request->get('partner_id'));
        };

        if (!empty ($request->get('date')) ){
            // Log::info(json_encode($request->get('date')));
            $date = explode(' - ', $request->get('date'));
            $start = $date[0];
            $end = $date[1];
            $tt = Carbon::parse($end);
            $xx = $tt->addDays(1)->format('Y-m-d');
            $data = $data->whereBetween('created_at', [$start, $xx]);
            // ->whereBetween('created_at', [$start, $end])
        };


     return Datatables::of($data)
           
     ->editColumn('action', function($data){ return view('dashboard.record.index-action', compact('data'));})

           ->rawColumns(['action'])
           ->make(true); 

}
   public function nilai($id)
    {
        // return json_encode($id);
        $question = Group::findOrFail($id);
        $this->pageTitle = 'Nilai : '.$question->title;
        $data = (array)$this;
  
        // // $data['form_delete'] = "ceklist.destroyQuestion";
        $data['question'] = Question::where('group_id', $id)->get();

        $data['group_id'] = $id;
        $data['partner'] = Partner::all();

        // return json_encode($data);
        return View::make('dashboard.record.nilai', $data);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group_id' => 'required',
            'partner_id' => 'required',
        ]);
        
        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        
        $qestion = Question::where('group_id', $request->group_id)->get();
        
        // return json_encode($request->all());
        $master = Master::create([
            'user_id' => Auth::user()->id,
            'partner_id' => $request->partner_id,
            'catatan_panjang' => $request->catatan ?? null,
            'company_id' => Auth::user()->company->id,
            'group_id' => $request->group_id,
            'masalah' => $request->masalah,
            // 'tindaklanjut' => $request->tindaklanjut ?? null,
            'area_pekerjaan' => $request->area_pekerjaan ?? null
            ]);


        foreach ($qestion as $key => $dt) {
            foreach ($request->all() as $key2 => $dt2) {

                if($key2 == $dt->id){

                    // return json_encode($request['note_'.$dt->id]);
                    $researche = Answer::create([
                        'master_id' => $master->id,
                        'question_id' => $dt->id,
                        'jawaban' => $dt2,
                        'note' => $request['note_'.$dt->id],
                        'company_id' => Auth::user()->company->id
                        ]);
                }

            }
        }
        

        session()->put('success','Berhasil Input.');   
        return redirect('/record');
        
    }
    public function destroy($id)
    {
        $data = Master::find($id);
        if($data->count() < 1){
            session()->put('error','Gagal hapus.');
            return back();
        };
        $data->delete();
        $answer = Answer::where('master_id',$id)->delete();
        session()->put('success','Berhasil Hapus.');
        return back();

    }

    public function cetak_pdf($id){

        // $data['id'] = $id;
        // $data['salary'] = Salary::with('user:name,id', )findOrFail($id);
        $master = Master::with('user:name,id', 'partner:id,name', 'group.question', 'group.division' )->findOrFail($id);
        $data['master'] = $master;
        $data['answer'] = Answer::where('master_id', $master->id)->get();
        // return json_encode($data);
        // $data['uraian'] = json_decode($data['salary']['uraian']);
        $data['company'] = strtoupper(Auth::user()->company->name);
        $data['user'] = strtoupper(Auth::user()->name);
        $data['admin'] = 'Irawan Pramudhita, SE';
        
        if($master->group->division->name == "Satuan Pengaman (Satpam)"){

            $data['signature'] = For_signature::with('signature')->where('name', 'Cek List Security')->first();
        }else if($master->group->division->name == "Cleaning Service"){
            $data['signature'] = For_signature::with('signature')->where('name', 'Cek List CS')->first();

        }else{
            $data['signature'] = "";
        }
    
    
        $pdf = PDF::loadView('dashboard.record.cetak', $data)->setPaper('a4', 'potrait');
            return $pdf->stream('ceklist_harian.pdf');
            // return $pdf->download('ceklist_harian.pdf');
    
       }

       public function cek_lokasi(Request $request)
    {
        // return json_encode($request->all());

        if($request->partner_id ==null){
        return json_encode('Pilih mitra');
        }

        $partner = Partner::where('id', $request->partner_id)->get();
   
        $lat1 = $partner[0]->latitude;
        $lat2 = $request->lat;
        $lon1 = $partner[0]->longitude;
        $lon2 = $request->long;

        $theta=$lon1-$lon2;
        $dist=sin(deg2rad($lat1))*sin(deg2rad($lat2))+cos(deg2rad($lat1))*cos(deg2rad($lat2))*cos(deg2rad($theta));
        $dist=acos($dist);
        $dist=rad2deg($dist);
        $miles=$dist*60*1.1515;
        
        $hasil = $miles*1.609344;
        // return json_encode($hasil);
        // $jarak = env('JARAK_MAKSIMAL');
        $jarak = 1;
        if($hasil < $jarak){
            return json_encode('Lokasi valid');
        }else{
            // return json_encode('Lokasi salah');
            return json_encode('Lokasi valid');

        }
        
    }

    public function rekapData(Request $request){
        $data = Master::with('user:name,id', 'partner:id,name,company_id', 'group.question' )->whereHas('partner', function ($query) {
            $query->where('company_id', Auth::user()->company->id);
            });
        if (!empty ($request->get('group_id')) ){
            $data = $data->where('group_id', $request->get('group_id'));
        };
        if (!empty ($request->get('partner_id')) ){
            $data = $data->where('partner_id', $request->get('partner_id'));
        };

        if (!empty ($request->get('date')) ){
            // Log::info(json_encode($request->get('date')));
            $date = explode(' - ', $request->get('date'));
            $start = $date[0];
            $end = $date[1];
            $tt = Carbon::parse($end);
            $xx = $tt->addDays(1)->format('Y-m-d');
            Log::info(json_encode([$start, $xx]));

            $data = $data->whereBetween('created_at', [$start, $xx]);
            // ->whereBetween('created_at', [$start, $end])
        };

         return Datatables::of($data)
         ->editColumn('action', function($data){ return view('dashboard.ceklist.rekap-action', compact('data'));})

               ->rawColumns(['action'])
               ->make(true); 
            }


    public function generateRekap(Request $request){
                try{
                $date = explode(' - ', $request->date_range);
                $start = $date[0];
                $end = $date[1];
                $tt = Carbon::parse($end);;
            $xx = $tt->addDays(1)->format('Y-m-d');
                // return json_encode($start);
        
                $master = Master::with('user:name,id', 'partner:id,name', 'group.question','answer' )
                                    ->where('group_id', $request->group_id)
                                    ->where('partner_id', $request->partner_id)
                                    // ->whereDate('created_at', '>=', $start)
                                    // ->whereDate('created_at', '<=', $end)
                                    ->whereBetween('created_at', [$start, $xx])
                                    ->get();
                $data['master'] = $master;
                // $data['answer'] = Answer::where('master_id', $master->id)->get();
                $data['group'] = Group::with('question')->where('id', $request->group_id)->first();
                $data['partner'] = Partner::where('id', $request->partner_id)->first();
                $data['date'] = $request->date_range;
                // return json_encode($data);
                // $data['uraian'] = json_decode($data['salary']['uraian']);
                $data['company'] = strtoupper(Auth::user()->company->name);
                $data['user'] = strtoupper(Auth::user()->name);
                $data['admin'] = 'Irawan Pramudhita, SE';
            
            
                $pdf = PDF::loadView('dashboard.ceklist.cetak-generate', $data)->setPaper('a4', 'landscape');
                    return $pdf->stream('generate.pdf');
                    // return $pdf->download('generate.pdf');
        
                }catch (\Exception $e) {
                    session()->put('error','Gagal generate!');
                    return redirect()->back();
                }
            }
}