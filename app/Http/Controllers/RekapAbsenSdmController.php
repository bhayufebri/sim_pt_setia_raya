<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Partner;
use App\User;
use App\Schedule;
use App\Division;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use PDF;


class RekapAbsenSdmController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('sdm');
        $this->rekapAbsenSdmActive = 'active';
    }
    public function index()
    {
        // $this->dataActive = 'active';
        $this->pageTitle = 'Rekap Absen';
        $data = (array)$this;
        $data['partner'] = Partner::select('id', 'name')->where('company_id', Auth::user()->company->id)->get();
        $data['user'] = User::select('id', 'name')->where('company_id', Auth::user()->company->id)->get();
        $data['division'] = Division::select('id', 'name')->get();

        return View::make('dashboard.rekapsdm.index', $data);
    }

    public function indexData(Request $request){
       
        $data = Schedule::with('user.division','partner')->whereHas('user', function ($query) {
            $query->where('company_id', Auth::user()->company->id);
            });
                            // ->whereNotNull('start_absen');

        if (!empty ($request->get('partner_id')) ){
            // $data = $data->where('partner_id', $request->get('partner_id'));
            $fil = $request->get('partner_id');
            $data = $data->whereHas('partner', function ($query) use ($fil) {
                $query->where('id', $fil);
                });
        };
        if (!empty ($request->get('user_id')) ){
            $data = $data->where('user_id', $request->get('user_id'));
        };

        if (!empty ($request->get('division_id')) ){
            $req = $request->get('division_id');
            $data = $data->whereHas('user', function ($query) use ($req) {
                $query->where('division_id', $req);
                });
        };


        


        if (!empty ($request->get('date')) ){
            // Log::info(json_encode($request->get('date')));
            $date = explode(' - ', $request->get('date'));
            $start = $date[0];
            $end = $date[1];
            $data = $data->whereBetween('start', [$start, $end]);
            // ->whereBetween('created_at', [$start, $end])
        };
        
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.slip.index-action', compact('data'));})
               ->editColumn('start_plot', function ($data) {     
                return date('d F Y H:i', strtotime($data->start_plot))." - ".date('d F Y H:i', strtotime($data->end_plot));
                   
                    })

                ->editColumn('start_absen', function ($data) {    
                    if($data->start_absen != null) {
                        return date('d F Y H:i', strtotime($data->start_absen));
                    }else{
                        return '-';
                    }
                           
                            })

                ->editColumn('end_absen', function ($data) {   
                    if($data->end_absen != null) {
                                return date('d F Y H:i', strtotime($data->end_absen));
                    }else{
                        return '-';
                    }
                })

                ->editColumn('lembur', function ($data) {  
                    
                    
                    if($data->end_plot < $data->end_absen){
                        $plot = Carbon::parse($data->end_plot);
                        $absen = Carbon::parse($data->end_absen);
                        $lembur = $plot->diff($absen)->format('%H Jam, %I Menit');
                        return $lembur;
                    }else{
                        return '-';
                    }
        

                                    })
               ->rawColumns(['action','start_plot', 'start_absen', 'end_absen', 'lembur'])
               ->make(true); 

   }


   public function cetak_pdf(Request $request){

    // $data['id'] = $id;
    // $data['salary'] = Salary::with('user:name,id', )findOrFail($id);

    // return json_encode($request->all());

    $data = Schedule::with('user.division','partner');
                            // ->whereNotNull('start_absen');

        if (!empty ($request->get('partner_id_filter')) ){
            $data = $data->where('partner_id', $request->get('partner_id_filter'));
        };
        if (!empty ($request->get('user_id_filter')) ){
            $data = $data->where('user_id', $request->get('user_id_filter'));
        };

        if (!empty ($request->get('division_id_filter')) ){
            $req = $request->get('division_id_filter');
            $data = $data->whereHas('user', function ($query) use ($req) {
                $query->where('division_id', $req);
                });
        };

        if (!empty ($request->get('date_filter')) ){
            // Log::info(json_encode($request->get('date')));
            $date = explode(' - ', $request->get('date_filter'));
            $start = $date[0];
            $end = $date[1];
            $data = $data->whereBetween('start', [$start, $end]);
            // ->whereBetween('created_at', [$start, $end])
        };

        // return json_encode($data->get());
        $absen = $data->get();


    // $master = Master::with('user:name,id', 'partner:id,name', 'group.question' )->findOrFail($id);
    $data2['absen'] = $absen;
    // $data['answer'] = Answer::where('master_id', $master->id)->get();
    // return json_encode($data);
    // $data['uraian'] = json_decode($data['salary']['uraian']);
    $data2['company'] = strtoupper(Auth::user()->company->name);
    $data2['user'] = strtoupper(Auth::user()->name);
    $data2['admin'] = 'Irawan Pramudhita, SE';
    $data2['now']  = Carbon::now();


    $pdf = PDF::loadView('dashboard.rekapsdm.cetak', $data2)->setPaper('a4', 'landscape');
        return $pdf->stream('absensi.pdf');
        // return $pdf->download('ceklist_harian.pdf');

   }
}