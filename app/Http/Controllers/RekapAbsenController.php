<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Partner;
use App\User;
use App\Schedule;
use App\Division;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use PDF;


class RekapAbsenController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->rekapAbsenActive = 'active';
    }
    public function index()
    {
        // $this->dataActive = 'active';
        $this->pageTitle = 'Rekap Absen';
        $data = (array)$this;
        // $data['partner'] = Partner::select('id', 'name')->where('company_id', Auth::user()->company->id)->get();
        // $data['user'] = User::select('id', 'name')->where('company_id', Auth::user()->company->id)->get();
        $data['division'] = Division::select('id', 'name')->get();

        // $data['form'] = [
        //     [ 'name' => 'title', 'label' => 'Nama', 'type' => 'text'],
        //     // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
        //     ['name' => 'division_id', 'label' => 'Divisi', 'type'=>'select', 'option' =>$division],

        //     ['name' => 'is_active', 'label' => 'Status', 'type'=>'select', 'option' => [
        //         ['id'=>1, 'name'=>'Aktif'],
        //         ['id'=>0, 'name'=>'Non-Aktif']
        //         ]  
        //     ]
        // ];
        // $data['form_action'] = "ceklist.store";
        // $data['form_update'] = "ceklist.update";
        // $data['form_delete'] = "ceklist.destroyQuestion";

        // return json_encode($data);
        return View::make('dashboard.rekap.index', $data);
    }

    public function indexData(Request $request){
       
        $data = Schedule::with('user.division','partner')->whereHas('user', function ($query) {
            $query->where('company_id', Auth::user()->company->id);
            });
                            // ->whereNotNull('start_absen');

        if (!empty ($request->get('partner_id')) ){
            // $data = $data->where('partner_id', $request->get('partner_id'));
            $fil = $request->get('partner_id');
            $data = $data->whereHas('partner', function ($query) use ($fil) {
                $query->where('id', $fil);
                });
        };
        if (!empty ($request->get('user_id')) ){
            $data = $data->where('user_id', $request->get('user_id'));
        };

        if (!empty ($request->get('division_id')) ){
            $req = $request->get('division_id');
            $data = $data->whereHas('user', function ($query) use ($req) {
                $query->where('division_id', $req);
                });
        };


        


        if (!empty ($request->get('date')) ){
            // Log::info(json_encode($request->get('date')));
            $date = explode(' - ', $request->get('date'));
            $start = $date[0];
            $end = $date[1];
            $data = $data->whereBetween('start', [$start, $end]);
            // ->whereBetween('created_at', [$start, $end])
        };
        
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.slip.index-action', compact('data'));})
               ->editColumn('start_plot', function ($data) {     
                return date('d F Y H:i', strtotime($data->start_plot))." - ".date('d F Y H:i', strtotime($data->end_plot));
                   
                    })

                ->editColumn('start_absen', function ($data) {    
                    if($data->start_absen != null) {
                        $hasil = date('d F Y H:i', strtotime($data->start_absen)).' <span class="badge bg-cyan">'.number_format($data->radius_berangkat,1,",",".").' m</span>';
                        return $hasil;
                    }else{
                        return '-';
                    }
                           
                            })

                ->editColumn('end_absen', function ($data) {   
                    if($data->end_absen != null) {
                                // return date('d F Y H:i', strtotime($data->end_absen));
                        $hasil = date('d F Y H:i', strtotime($data->end_absen)).' <span class="badge bg-cyan">'.number_format($data->radius_pulang,1,",",".").' m</span>';
                        return $hasil;

                    }else{
                        return '-';
                    }
                })

                ->editColumn('lembur', function ($data) {  
                    
                    
                    if($data->end_plot < $data->end_absen){
                        $plot = Carbon::parse($data->end_plot);
                        $absen = Carbon::parse($data->end_absen);
                        $lembur = $plot->diff($absen)->format('%H Jam, %I Menit');
                        return $lembur;
                    }else{
                        return '-';
                    }
        

                                    })

                ->editColumn('radius', function ($data) {  
                    return $data->radius_pulang;
                            
                    
                                                        })
               ->rawColumns(['action','start_plot', 'start_absen', 'end_absen', 'lembur', 'radius'])
               ->make(true); 

   }


   public function cetak_pdf(Request $request){

    // $data['id'] = $id;
    // $data['salary'] = Salary::with('user:name,id', )findOrFail($id);

    // return json_encode($request->all());

    $data = Schedule::with('user.division','partner');
                            // ->whereNotNull('start_absen');

        if (!empty ($request->get('partner_id_filter')) ){
            $data = $data->where('partner_id', $request->get('partner_id_filter'));
        };
        if (!empty ($request->get('user_id_filter')) ){
            $data = $data->where('user_id', $request->get('user_id_filter'));
        };

        if (!empty ($request->get('division_id_filter')) ){
            $req = $request->get('division_id_filter');
            $data = $data->whereHas('user', function ($query) use ($req) {
                $query->where('division_id', $req);
                });
        };

        if (!empty ($request->get('date_filter')) ){
            // Log::info(json_encode($request->get('date')));
            $date = explode(' - ', $request->get('date_filter'));
            $start = $date[0];
            $end = $date[1];
            $data = $data->whereBetween('start', [$start, $end]);
            // ->whereBetween('created_at', [$start, $end])
        };

        // return json_encode($data->get());
        $absen = $data->get();


    // $master = Master::with('user:name,id', 'partner:id,name', 'group.question' )->findOrFail($id);
    $data2['absen'] = $absen;
    // $data['answer'] = Answer::where('master_id', $master->id)->get();
    // return json_encode($data);
    // $data['uraian'] = json_decode($data['salary']['uraian']);
    $data2['company'] = strtoupper(Auth::user()->company->name);
    $data2['user'] = strtoupper(Auth::user()->name);
    $data2['admin'] = 'Irawan Pramudhita, SE';
    $data2['now']  = Carbon::now();


    $pdf = PDF::loadView('dashboard.rekap.cetak', $data2)->setPaper('a4', 'landscape');
        return $pdf->stream('absensi.pdf');
        // return $pdf->download('ceklist_harian.pdf');

   }

   public function cariUserAjax(Request $request){
    
    $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data =User::select("id","name")
            		->where('name','LIKE',"%$search%")
            		->get();
        }
        return response()->json($data);

   }
   public function cariPartnerAjax(Request $request){
    
    $data = [];

        if($request->has('q')){
            $search = $request->q;
            $data =Partner::select("id","name")
            		->where('name','LIKE',"%$search%")
            		->get();
        }
        return response()->json($data);

   }
}