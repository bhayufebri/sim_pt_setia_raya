<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Variable;
use Illuminate\Support\Facades\Validator;
use DataTables;

class VariableController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->pageTitle = 'Variable';
        $this->penggajianActive = 'active';
        $this->variableSetActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
        $data['form'] = [
            [ 'name' => 'name', 'label' => 'Nama', 'type' => 'text'],
            // [ 'name' => 'description', 'label' => 'Deskripsi', 'type' => 'text'],
            // ['name' => 'ket', 'label' => 'Keterangan', 'type'=>'select', 'value' => ['sa', 'tu', 'dua']]  
            ['name' => 'is_subject', 'label' => 'Status', 'type'=>'select', 'option' => [
                ['id'=>1, 'name'=>'Pembagi'],
                ['id'=>0, 'name'=>'Biasa']
                ]
            ],
                ['name' => 'description', 'label' => 'Sifat', 'type'=>'select', 'option' => [
                    ['id'=>'Penambah', 'name'=>'Penambah'],
                    ['id'=>'Pengurang', 'name'=>'Pengurang']
                    ]  
                ]
        ];
        $data['form_action'] = "variable.store";
        $data['form_update'] = "variable.update";
        // return json_encode($data['form'][1]['type']);
        return View::make('dashboard.variable.index', $data);
    }
    public function indexData(Request $request){
        $data = Variable::all();
         return Datatables::of($data)
               ->editColumn('action', function($data){ return view('dashboard.variable.index-action', compact('data'));})
               ->rawColumns(['action'])
               ->make(true); 

   }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'is_subject' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $researche = Variable::create([
            'name' => $request->name,
            'is_subject' => $request->is_subject,
            'description' => $request->description

            ]);

        session()->put('success','Berhasil Input.');   
        return redirect('/variable');
        
        // return json_encode($request->all());
    }

    public function destroy($id)
    {
        $data = Variable::find($id);
        if($data->count() < 1){
        session()->put('error','Gagal hapus.');
            return redirect('/variable');
        };
        $data->delete();
        session()->put('success','Berhasil Hapus.');
            return redirect('/variable');
    }
    public function look(Request $request)
    {
       
        $data = Variable::findOrFail($request->data);
        return json_encode($data);

    }
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            session()->put('error','Input gagal.');
            return back();
        }
        $data = Variable::find($request->id);
        $data->name = $request->name;
        $data->is_subject = $request->is_subject;
        $data->description = $request->description;
        $data->save();
        session()->put('success','Berhasi edit!');
        return redirect('/variable');  
    }
}