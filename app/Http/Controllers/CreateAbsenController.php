<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use App\Partner;
use App\Schedule;
use App\User;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Str;
use DB;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\URL;
use App\Imports\ImportSchedule;
use DateTime;
use DatePeriod;
use DateInterval;
use Log;
use PDF;

use Illuminate\Support\Facades\Hash;

class CreateAbsenController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('admin');
        $this->pageTitle = 'Absen';
        // $this->masterActive = 'active';
        $this->absenActive = 'active';
    }
    public function index()
    {
        $data = (array)$this;
        $data['partner'] = Partner::all();
        
        // return json_encode($data);
        return View::make('dashboard.jadwal.index', $data);
    }
    public function lookUser(Request $request)
    {
        // return json_encode($request->all());
        // $user = User::where('partner_id', $request->partner_id)->get();
        $user = User::select(['id as id','name as text'])->where('partner_id', $request->partner_id)->get();
        return json_encode($user);
    }
    public function indexAjax(Request $request)
    {
  
        // $data = Schedule::get(['id', 'title as mboh', 'start', 'end']);
        // return json_encode($data);
        // Log::info('masuk'. json_encode($request->all()));
        if($request->ajax()) {
       
             $data = Schedule::with('user')->whereDate('start', '>=', $request->start)
                       ->whereDate('end',   '<=', $request->end)
                    //    ->get(['id', 'title', 'start', 'end', DB::raw('CONCAT(start_plot,"-",end_plot) as description')]);
                    //    ->get(['id','user_id','title', 'start', 'end', 'start_plot as description','end_plot as end_time']);
                       ->get();
                       if (!empty ($request->user_id) ){
                        $data = $data->where('user_id', $request->user_id);
                    };
  
             return response()->json($data);
        }
    }

    public function ajax(Request $request)
    {
        // Log::info('ajax'. json_encode($request->all()));
        // $tags = explode(",", $request->tags);
        // foreach ($tags as $dt) {
        //     // $begin = new DateTime($request->start);

        //     return json_encode($dt);

        // };

 
        switch ($request->type) {
           case 'add':

            // $begin = new DateTime($request->start);
            // $end   = new DateTime($request->end);

            // $interval = DateInterval::createFromDateString('1 day');
            // $period = new DatePeriod($begin, $interval, $end);
        $tags = explode(",", $request->tags);


            foreach ($tags as $dt) {
                // echo $dt->format("l Y-m-d H:i:s\n");
                    // echo $i->format("Y-m-d");
                    $dt = new DateTime($dt);

                    $tgl = $dt->format("Y-m-d");
                    $bsk = $dt->modify('+1 day');
                    $hari = $bsk->format("Y-m-d");
                    
                    $start = date('Y-m-d H:i:s', strtotime("$tgl $request->start_time"));
                    if(strtotime($request->start_time) > strtotime($request->end_time)){
                        $end = date('Y-m-d H:i:s', strtotime("$hari $request->end_time"));
                    }else{
                        $end = date('Y-m-d H:i:s', strtotime("$tgl $request->end_time"));
                    }
                    // $event[] = $tgl;
                    $event = Schedule::create([
                        'title' => $request->title,
                        'start' => $tgl,
                        'end' => $hari,
                        'start_plot' => $start,
                        'end_plot' => $end,
                        'user_id' => $request->user_id,
                        'partner_id' => $request->partner_id,
                    ]);
                
                // $event[] = $dt->format("Y-m-d H:i:s\n");
            }

                // for($i = $begin; $i <= $end; $i->modify('+1 day')){
                //     // echo $i->format("Y-m-d");
                //     $tgl = $i->format("Y-m-d");
                //     $bsk = $i->modify('+1 day');
                //     $hari = $bsk->format("Y-m-d");
                //     $start = date('Y-m-d H:i:s', strtotime("$tgl $request->start_time"));
                //     $end = date('Y-m-d H:i:s', strtotime("$tgl $request->end_time"));
                //     $event[] = $tgl;
                //     // $event = Schedule::create([
                //     //     'title' => $request->title,
                //     //     'start' => $tgl,
                //     //     'end' => $hari,
                //     //     'start_plot' => $start,
                //     //     'end_plot' => $end,
                //     // ]);
       
                // }

              return response()->json($event);
             break;
  
           case 'update':
              $event = Schedule::find($request->id)->update([
                  'title' => $request->title,
                  'start' => $request->start,
                  'end' => $request->end,
              ]);
 
              return response()->json($event);
             break;
  
           case 'delete':
              $event = Schedule::find($request->id)->delete();
  
              return response()->json($event);
             break;
             
           default:
             # code...
             break;
        }
    }
}