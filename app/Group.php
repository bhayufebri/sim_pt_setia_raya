<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title', 'is_active', 'company_id','division_id'
];

public function division(){
    return $this->belongsTo('App\Division');
}


public function question(){
    return $this->hasMany('App\Question');
}
}