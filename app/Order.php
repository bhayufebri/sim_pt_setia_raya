<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Order extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'company_id','user_id','partner_id'
];

public function partner(){
    return $this->belongsTo('App\Partner');
}
public function user(){
    return $this->belongsTo('App\User');
}
}