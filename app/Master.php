<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Master extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'company_id', 'user_id', 'partner_id','catatan',
        'group_id','catatan_panjang','penanggung_jawab','area_pekerjaan','masalah', 'tindaklanjut'
];

public function user(){
    return $this->belongsTo('App\User');
}
public function partner(){
    return $this->belongsTo('App\Partner');
}
public function group(){
    return $this->belongsTo('App\Group');
}
public function answer(){
    return $this->hasMany('App\Answer');
}
}