<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice_detail extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id',
        'description',
        'jumlah_detail',
        'invoice_id'
];
}