<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visit extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'start',
        'end',
        'company_id',
        'user_id',
        'partner_id'
];

public function user(){
    return $this->belongsTo('App\User');
}
public function partner(){
    return $this->belongsTo('App\Partner');
}

}