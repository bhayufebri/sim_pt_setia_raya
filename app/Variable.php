<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Variable extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'is_subject',
        'description'
];
}