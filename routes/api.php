<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// dd('as');

Route::post('/absen', 'AbsenController@apiAbsen')->name('absen.apiAbsen');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('v1/role/store', 'API\RoleController@store')->name('role.store');