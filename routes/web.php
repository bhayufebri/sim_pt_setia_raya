<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', function () {
    //     return view('auth.login');
    // })->name('login');
Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('setia_logs_sangat_rahasia', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/role', 'RoleController@index')->name('role.index');
Route::post('/role/store', 'RoleController@store')->name('role.store');
Route::get('/role/index-data', 'RoleController@indexData')->name('role.indexData');
Route::get('role/destroy/{id}', 'RoleController@destroy')->name('role.destroy');
Route::post('/role/update', 'RoleController@update')->name('role.update');
Route::post('/role/look/','RoleController@look')->name('role.look');

Route::get('/user', 'UserController@index')->name('user.index');
Route::post('/user/store', 'UserController@store')->name('user.store');
Route::get('/user/index-data', 'UserController@indexData')->name('user.indexData');
Route::get('user/destroy/{id}', 'UserController@destroy')->name('user.destroy');
Route::post('/user/update', 'UserController@update')->name('user.update');
Route::post('/user/look/','UserController@look')->name('user.look');

Route::get('/company', 'CompanyController@index')->name('company.index');
Route::post('/company/store', 'CompanyController@store')->name('company.store');
Route::get('/company/index-data', 'CompanyController@indexData')->name('company.indexData');
Route::get('company/destroy/{id}', 'CompanyController@destroy')->name('company.destroy');
Route::post('/company/update', 'CompanyController@update')->name('company.update');
Route::post('/company/look/','CompanyController@look')->name('company.look');

Route::get('/division', 'DivisionController@index')->name('division.index');
Route::post('/division/store', 'DivisionController@store')->name('division.store');
Route::get('/division/index-data', 'DivisionController@indexData')->name('division.indexData');
Route::get('division/destroy/{id}', 'DivisionController@destroy')->name('division.destroy');
Route::post('/division/update', 'DivisionController@update')->name('division.update');
Route::post('/division/look/','DivisionController@look')->name('division.look');

Route::get('/signature', 'SignatureController@index')->name('signature.index');
Route::post('/signature/store', 'SignatureController@store')->name('signature.store');
Route::get('/signature/index-data', 'SignatureController@indexData')->name('signature.indexData');
Route::get('signature/destroy/{id}', 'SignatureController@destroy')->name('signature.destroy');
Route::post('/signature/update', 'SignatureController@update')->name('signature.update');
Route::post('/signature/look/','SignatureController@look')->name('signature.look');

Route::get('/forsignature', 'ForSignatureController@index')->name('forsignature.index');
Route::post('/forsignature/store', 'ForSignatureController@store')->name('forsignature.store');
Route::get('/forsignature/index-data', 'ForSignatureController@indexData')->name('forsignature.indexData');
Route::get('forsignature/destroy/{id}', 'ForSignatureController@destroy')->name('forsignature.destroy');
Route::post('/forsignature/update', 'ForSignatureController@update')->name('forsignature.update');
Route::post('/forsignature/look/','ForSignatureController@look')->name('forsignature.look');

Route::get('/position', 'PositionController@index')->name('position.index');
Route::post('/position/store', 'PositionController@store')->name('position.store');
Route::get('/position/index-data', 'PositionController@indexData')->name('position.indexData');
Route::get('position/destroy/{id}', 'PositionController@destroy')->name('position.destroy');
Route::post('/position/update', 'PositionController@update')->name('position.update');
Route::post('/position/look/','PositionController@look')->name('position.look');

Route::get('/partner', 'PartnerController@index')->name('partner.index');
Route::post('/partner/store', 'PartnerController@store')->name('partner.store');
Route::get('/partner/index-data', 'PartnerController@indexData')->name('partner.indexData');
Route::get('partner/destroy/{id}', 'PartnerController@destroy')->name('partner.destroy');
Route::post('/partner/update', 'PartnerController@update')->name('partner.update');
Route::post('/partner/location_update', 'PartnerController@location_update')->name('partner.location_update');
Route::post('/partner/jarak_update', 'PartnerController@jarak_update')->name('partner.jarak_update');
Route::post('/partner/look/','PartnerController@look')->name('partner.look');
Route::get('/partner/detail/{id}','PartnerController@detail')->name('partner.detail');
Route::get('/partner/download_berkas', 'PartnerController@download_berkas')->name('partner.download_berkas');
Route::post('/partner/import_schedule', 'PartnerController@import_schedule')->name('partner.import_schedule');
Route::get('partner/cetak_qr/{id}', 'PartnerController@cetak_qr')->name('partner.cetak_qr');

// Route::get('/partner/indexAjax', 'PartnerController@indexAjax')->name('partner.indexAjax');
Route::post('/partner/indexAjax', 'PartnerController@indexAjax')->name('partner.indexAjax');
Route::post('/partner/ajax','PartnerController@ajax')->name('partner.ajax');
Route::post('/partner/indexAjax2', 'PartnerController@indexAjax2')->name('partner.indexAjax2');
Route::post('/partner/look_schedule', 'PartnerController@look_schedule')->name('partner.look_schedule');
Route::post('/partner/sub_schedule', 'PartnerController@sub_schedule')->name('partner.sub_schedule');
Route::post('/partner/batal_pulang', 'PartnerController@batal_pulang')->name('partner.batal_pulang');

Route::get('/partnersdm', 'PartnerSdmController@index')->name('partnersdm.index');
Route::post('/partnersdm/store', 'PartnerSdmController@store')->name('partnersdm.store');
Route::get('/partnersdm/index-data', 'PartnerSdmController@indexData')->name('partnersdm.indexData');
Route::get('partnersdm/destroy/{id}', 'PartnerSdmController@destroy')->name('partnersdm.destroy');
Route::post('/partnersdm/update', 'PartnerSdmController@update')->name('partnersdm.update');
Route::post('/partnersdm/location_update', 'PartnerSdmController@location_update')->name('partnersdm.location_update');
Route::post('/partnersdm/jarak_update', 'PartnerSdmController@jarak_update')->name('partnersdm.jarak_update');
Route::post('/partnersdm/look/','PartnerSdmController@look')->name('partnersdm.look');
Route::get('/partnersdm/detail/{id}','PartnerSdmController@detail')->name('partnersdm.detail');
Route::get('/partnersdm/download_berkas', 'PartnerSdmController@download_berkas')->name('partnersdm.download_berkas');
Route::post('/partnersdm/import_schedule', 'PartnerSdmController@import_schedule')->name('partnersdm.import_schedule');
Route::get('partnersdm/cetak_qr/{id}', 'PartnerSdmController@cetak_qr')->name('partnersdm.cetak_qr');

// Route::get('/partnersdm/indexAjax', 'PartnerSdmController@indexAjax')->name('partnersdm.indexAjax');
Route::post('/partnersdm/indexAjax', 'PartnerSdmController@indexAjax')->name('partnersdm.indexAjax');
Route::post('/partnersdm/ajax','PartnerSdmController@ajax')->name('partnersdm.ajax');
Route::post('/partnersdm/indexAjax2', 'PartnerSdmController@indexAjax2')->name('partnersdm.indexAjax2');
Route::post('/partnersdm/look_schedule', 'PartnerSdmController@look_schedule')->name('partnersdm.look_schedule');
Route::post('/partnersdm/sub_schedule', 'PartnerSdmController@sub_schedule')->name('partnersdm.sub_schedule');
Route::post('/partnersdm/batal_pulang', 'PartnerSdmController@batal_pulang')->name('partnersdm.batal_pulang');



Route::get('/status', 'StatusController@index')->name('status.index');
Route::post('/status/store', 'StatusController@store')->name('status.store');
Route::get('/status/index-data', 'StatusController@indexData')->name('status.indexData');
Route::get('status/destroy/{id}', 'StatusController@destroy')->name('status.destroy');
Route::post('/status/update', 'StatusController@update')->name('status.update');
Route::post('/status/look/','StatusController@look')->name('status.look');

Route::get('/member', 'MemberController@index')->name('member.index');
Route::post('/member/store', 'MemberController@store')->name('member.store');
Route::post('/member/store_file', 'MemberController@store_file')->name('member.store_file');
Route::get('/member/index-data', 'MemberController@indexData')->name('member.indexData');
Route::get('/member/index-edit', 'MemberController@indexEdit')->name('member.indexEdit');
Route::get('member/destroy/{id}', 'MemberController@destroy')->name('member.destroy');
Route::post('/member/update', 'MemberController@update')->name('member.update');
Route::post('/member/update_password', 'MemberController@update_password')->name('member.update_password');
Route::post('/member/update_image', 'MemberController@update_image')->name('member.update_image');
Route::post('/member/update_pkwt', 'MemberController@update_pkwt')->name('member.update_pkwt');
Route::post('/member/update_partner', 'MemberController@update_partner')->name('member.update_partner');
Route::post('/member/look/','MemberController@look')->name('member.look');
Route::post('/member/look_position/','MemberController@look_position')->name('member.look_position');
Route::post('/member/look_file/','MemberController@look_file')->name('member.look_file');
Route::post('/member/delete_file','MemberController@delete_file')->name('member.delete_file');
Route::post('/member/delete_history','MemberController@delete_history')->name('member.delete_history');
Route::get('/member/edit/{id}', 'MemberController@edit')->name('member.edit');
Route::get('/member/download_fileex', 'MemberController@download_fileex')->name('member.download_fileex');
Route::post('/member/import', 'MemberController@import')->name('member.import');
Route::post('/member/look_pkwt/','MemberController@look_pkwt')->name('member.look_pkwt');
Route::post('/member/delete_pkwt','MemberController@delete_pkwt')->name('member.delete_pkwt');
Route::post('/member/export_pdf','MemberController@export_pdf')->name('member.export_pdf');

Route::get('absen/{id}', 'AbsenController@index')->name('absen.index');
Route::get('absen/camera', 'AbsenController@camera')->name('absen.camera');
Route::get('blank', 'AbsenController@blank')->name('absen.blank');
Route::post('/absen/store', 'AbsenController@store')->name('absen.store');
Route::post('/absen/store2', 'AbsenController@store2')->name('absen.store2');
Route::post('/absen/batal', 'AbsenController@batal')->name('absen.batal');

Route::get('/penggajian', 'PenggajianController@index')->name('penggajian.index');
Route::get('/penggajian/index-data', 'PenggajianController@indexData')->name('penggajian.indexData');
Route::post('/penggajian/look_gaji/','PenggajianController@look_gaji')->name('penggajian.look_gaji');
Route::post('/penggajian/update_gaji', 'PenggajianController@update_gaji')->name('penggajian.update_gaji');
Route::get('/penggajian/generate', 'PenggajianController@generate')->name('penggajian.generate');
Route::post('/penggajian/store', 'PenggajianController@store')->name('penggajian.store');
Route::get('/penggajian/dataPeriod', 'PenggajianController@dataPeriod')->name('penggajian.dataPeriod');
Route::get('penggajian/destroy/{id}', 'PenggajianController@destroy')->name('penggajian.destroy');
Route::get('penggajian/detail/{id}', 'PenggajianController@detail')->name('penggajian.detail');
Route::get('/penggajian/dataSalary', 'PenggajianController@dataSalary')->name('penggajian.dataSalary');
Route::post('/penggajian/generateProcess', 'PenggajianController@generateProcess')->name('penggajian.generateProcess');
Route::get('/penggajian/cetak_pdf/{id}', 'PenggajianController@cetak_pdf')->name('penggajian.cetak_pdf');
Route::get('penggajian/salaryDestroy/{id}', 'PenggajianController@salaryDestroy')->name('penggajian.salaryDestroy');
Route::post('/penggajian/look_salary/','PenggajianController@look_salary')->name('penggajian.look_salary');
Route::post('/penggajian/store_component/','PenggajianController@store_component')->name('penggajian.store_component');
Route::post('/penggajian/look_component/','PenggajianController@look_component')->name('penggajian.look_component');
Route::post('/penggajian/sub_obj/','PenggajianController@sub_obj')->name('penggajian.sub_obj');
Route::post('/penggajian/destroy_component/','PenggajianController@destroy_component')->name('penggajian.destroy_component');
Route::post('/penggajian/manual/','PenggajianController@manual')->name('penggajian.manual');
Route::post('/penggajian/look_detail/','PenggajianController@look_detail')->name('penggajian.look_detail');
Route::post('/penggajian/store_detail/','PenggajianController@store_detail')->name('penggajian.store_detail');
Route::post('/penggajian/update_detail/','PenggajianController@update_detail')->name('penggajian.update_detail');
Route::post('/penggajian/destroy_detail/','PenggajianController@destroy_detail')->name('penggajian.destroy_detail');
Route::get('/penggajian/cetak_report_pdf/{id}/{partner_id}','PenggajianController@cetak_report_pdf')->name('penggajian.cetak_report_pdf');




Route::get('/ceklist', 'CeklistController@index')->name('ceklist.index');
Route::post('/ceklist/store', 'CeklistController@store')->name('ceklist.store');
Route::get('/ceklist/index-data', 'CeklistController@indexData')->name('ceklist.indexData');
Route::get('ceklist/destroy/{id}', 'CeklistController@destroy')->name('ceklist.destroy');
Route::post('/ceklist/update', 'CeklistController@update')->name('ceklist.update');
Route::post('/ceklist/look/','CeklistController@look')->name('ceklist.look');
Route::get('/ceklist/detail/{id}','CeklistController@detail')->name('ceklist.detail');
Route::post('/ceklist/storeQuestion', 'CeklistController@storeQuestion')->name('ceklist.storeQuestion');
Route::get('/ceklist/dataQuestion', 'CeklistController@dataQuestion')->name('ceklist.dataQuestion');
Route::get('ceklist/destroyQuestion/{id}', 'CeklistController@destroyQuestion')->name('ceklist.destroyQuestion');
Route::post('/ceklist/updateQuestion', 'CeklistController@updateQuestion')->name('ceklist.updateQuestion');
Route::post('/ceklist/lookQuestion/','CeklistController@lookQuestion')->name('ceklist.lookQuestion');
Route::get('/ceklist/lihat/{id}','CeklistController@lihat')->name('ceklist.lihat');
Route::get('/ceklist/rekap','CeklistController@rekap')->name('ceklist.rekap');
Route::get('/ceklist/rekapData', 'CeklistController@rekapData')->name('ceklist.rekapData');
Route::get('/ceklist/cetak_pdf_ceklist/{id}', 'CeklistController@cetak_pdf_ceklist')->name('ceklist.cetak_pdf_ceklist');
Route::post('/ceklist/generateRekap/','CeklistController@generateRekap')->name('ceklist.generateRekap');
Route::get('/ceklist/kunjungan','CeklistController@kunjungan')->name('ceklist.kunjungan');
Route::post('/ceklist/indexAjax','CeklistController@indexAjax')->name('ceklist.indexAjax');




Route::get('/record', 'RecordController@index')->name('record.index');
Route::get('/record/index-data', 'RecordController@indexData')->name('record.indexData');
Route::get('/record/nilai/{id}','RecordController@nilai')->name('record.nilai');
Route::post('/record/store', 'RecordController@store')->name('record.store');
Route::post('/record/cek_lokasi', 'RecordController@cek_lokasi')->name('record.cek_lokasi');
Route::get('/record/master-data', 'RecordController@masterData')->name('record.masterData');
Route::get('record/destroy/{id}', 'RecordController@destroy')->name('record.destroy');
Route::get('/record/cetak_pdf/{id}', 'RecordController@cetak_pdf')->name('record.cetak_pdf');
Route::get('/record/rekapData', 'RecordController@rekapData')->name('record.rekapData');
Route::post('/record/generateRekap/','RecordController@generateRekap')->name('record.generateRekap');


Route::get('/variable', 'VariableController@index')->name('variable.index');
Route::post('/variable/store', 'VariableController@store')->name('variable.store');
Route::get('/variable/index-data', 'VariableController@indexData')->name('variable.indexData');
Route::get('variable/destroy/{id}', 'VariableController@destroy')->name('variable.destroy');
Route::post('/variable/update', 'VariableController@update')->name('variable.update');
Route::post('/variable/look/','VariableController@look')->name('variable.look');

Route::get('/slip', 'SlipController@index')->name('slip.index');
Route::get('/slip/index-data', 'SlipController@indexData')->name('slip.indexData');
Route::get('/slip/cetak_xxx/{id}', 'SlipController@cetak_xxx')->name('slip.cetak_xxx');
Route::get('/slip/jadwal', 'SlipController@jadwal')->name('slip.jadwal');
Route::post('/slip/indexAjax', 'SlipController@indexAjax')->name('slip.indexAjax');




Route::get('/mitra', 'MitraController@index')->name('mitra.index');
Route::post('/mitra/login', 'MitraController@login')->name('mitra.login');
Route::post('/mitra/logout', 'MitraController@logout')->name('mitra.logout');
Route::get('/dashboard', 'MitraDashboardController@index')->name('dashboard.index');
Route::get('/dashboard/tagihan', 'MitraDashboardController@tagihan')->name('dashboard.tagihan');
Route::get('/dashboard/index-data', 'MitraDashboardController@indexData')->name('dashboard.indexData');
Route::post('/dashboard/upload', 'MitraDashboardController@upload')->name('dashboard.upload');
Route::post('/dashboard/look/','MitraDashboardController@look')->name('dashboard.look');
Route::get('dashboard/cetak_pdf/{id}', 'MitraDashboardController@cetak_pdf')->name('dashboard.cetak_pdf');
Route::get('/dashboard/pegawai', 'MitraDashboardController@pegawai')->name('dashboard.pegawai');
Route::get('/dashboard/dataPegawai', 'MitraDashboardController@dataPegawai')->name('dashboard.dataPegawai');
Route::get('/dashboard/contact', 'MitraDashboardController@contact')->name('dashboard.contact');
Route::post('/dashboard/contact_store', 'MitraDashboardController@contact_store')->name('dashboard.contact_store');
Route::get('/dashboard/jadwal', 'MitraDashboardController@jadwal')->name('dashboard.jadwal');
Route::post('/dashboard/indexAjax', 'MitraDashboardController@indexAjax')->name('dashboard.indexAjax');
Route::get('/dashboard/serviceData', 'MitraDashboardController@serviceData')->name('dashboard.serviceData');
Route::post('/dashboard/indexAjax2', 'MitraDashboardController@indexAjax2')->name('dashboard.indexAjax2');
Route::get('/dashboard/dataAbsen', 'MitraDashboardController@dataAbsen')->name('dashboard.dataAbsen');
Route::get('dashboard/cetak_bill/{id}', 'MitraDashboardController@cetak_bill')->name('dashboard.cetak_bill');





Route::get('/invoice', 'InvoiceController@index')->name('invoice.index');
Route::post('/invoice/store', 'InvoiceController@store')->name('invoice.store');
Route::post('/invoice/store_detail', 'InvoiceController@store_detail')->name('invoice.store_detail');
Route::get('/invoice/index-data', 'InvoiceController@indexData')->name('invoice.indexData');
Route::get('invoice/destroy/{id}', 'InvoiceController@destroy')->name('invoice.destroy');
Route::get('invoice/destroy_detail/{id}', 'InvoiceController@destroy_detail')->name('invoice.destroy_detail');
Route::post('/invoice/update', 'InvoiceController@update')->name('invoice.update');
Route::post('/invoice/look/','InvoiceController@look')->name('invoice.look');
Route::get('invoice/cetak_pdf/{id}', 'InvoiceController@cetak_pdf')->name('invoice.cetak_pdf');
Route::get('invoice/cetak_bill/{id}', 'InvoiceController@cetak_bill')->name('invoice.cetak_bill');
Route::post('/invoice/lookInvoice/','InvoiceController@lookInvoice')->name('invoice.lookInvoice');
Route::post('/invoice/generateRekapInvoice/','InvoiceController@generateRekapInvoice')->name('invoice.generateRekapInvoice');


Route::get('/transaction', 'TransactionController@index')->name('transaction.index');
Route::post('/transaction/store', 'TransactionController@store')->name('transaction.store');
Route::get('/transaction/index-data', 'TransactionController@indexData')->name('transaction.indexData');
Route::get('/transaction/dataPenggajian', 'TransactionController@dataPenggajian')->name('transaction.dataPenggajian');
Route::get('/transaction/dataInvoice', 'TransactionController@dataInvoice')->name('transaction.dataInvoice');
Route::get('transaction/destroy/{id}', 'TransactionController@destroy')->name('transaction.destroy');
Route::post('/transaction/update', 'TransactionController@update')->name('transaction.update');
Route::post('/transaction/look/','TransactionController@look')->name('transaction.look');


Route::get('/report', 'ReportController@index')->name('report.index');
Route::post('/report/store', 'ReportController@store')->name('report.store');
Route::get('/report/index-data', 'ReportController@indexData')->name('report.indexData');
Route::get('report/destroy/{id}', 'ReportController@destroy')->name('report.destroy');
Route::post('/report/update', 'ReportController@update')->name('report.update');
Route::post('/report/look/','ReportController@look')->name('report.look');
Route::get('report/cetak_pdf/{id}', 'ReportController@cetak_pdf')->name('report.cetak_pdf');
Route::get('report/custom_pdf/{id}', 'ReportController@custom_pdf')->name('report.custom_pdf');

Route::get('/rekap', 'RekapAbsenController@index')->name('rekap.index');
Route::get('/rekap/index-data', 'RekapAbsenController@indexData')->name('rekap.indexData');
Route::post('/rekap/cetak_pdf', 'RekapAbsenController@cetak_pdf')->name('rekap.cetak_pdf');
Route::get('/rekap/cariUserAjax', 'RekapAbsenController@cariUserAjax')->name('rekap.cariUserAjax');
Route::get('/rekap/cariPartnerAjax', 'RekapAbsenController@cariPartnerAjax')->name('rekap.cariPartnerAjax');

Route::get('/rekapsdm', 'RekapAbsenSdmController@index')->name('rekapsdm.index');
Route::get('/rekapsdm/index-data', 'RekapAbsenSdmController@indexData')->name('rekapsdm.indexData');
Route::post('/rekapsdm/cetak_pdf', 'RekapAbsenSdmController@cetak_pdf')->name('rekapsdm.cetak_pdf');

Route::get('/barangmitra', 'BarangMitraController@index')->name('barangmitra.index');
Route::post('/barangmitra/store', 'BarangMitraController@store')->name('barangmitra.store');
Route::get('/barangmitra/indexData', 'BarangMitraController@indexData')->name('barangmitra.indexData');
Route::get('/barangmitra/destroy/{id}', 'BarangMitraController@destroy')->name('barangmitra.destroy');
Route::post('/barangmitra/look', 'BarangMitraController@look')->name('barangmitra.look');
Route::post('/barangmitra/look_item', 'BarangMitraController@look_item')->name('barangmitra.look_item');
Route::get('/barangmitra/destroy_detail/{id}', 'BarangMitraController@destroy_detail')->name('barangmitra.destroy_detail');

Route::get('/permintaanbarang', 'PermintaanBarangController@index')->name('permintaanbarang.index');
Route::get('/permintaanbarang/indexData', 'PermintaanBarangController@indexData')->name('permintaanbarang.indexData');
Route::get('/permintaanbarang/dataItem', 'PermintaanBarangController@dataItem')->name('permintaanbarang.dataItem');
Route::get('/permintaanbarang/cetak/{id}', 'PermintaanBarangController@cetak')->name('permintaanbarang.cetak');
Route::post('/permintaanbarang/store', 'PermintaanBarangController@store')->name('permintaanbarang.store');
Route::get('permintaanbarang/destroy/{id}', 'PermintaanBarangController@destroy')->name('permintaanbarang.destroy');
Route::post('/permintaanbarang/update', 'PermintaanBarangController@update')->name('permintaanbarang.update');
Route::post('/permintaanbarang/look/','PermintaanBarangController@look')->name('permintaanbarang.look');
Route::post('/permintaanbarang/setujui/','PermintaanBarangController@setujui')->name('permintaanbarang.setujui');
Route::post('/permintaanbarang/tolak/','PermintaanBarangController@tolak')->name('permintaanbarang.tolak');
Route::post('/permintaanbarang/look_permintaan/','PermintaanBarangController@look_permintaan')->name('permintaanbarang.look_permintaan');
Route::post('/permintaanbarang/update_permintaan/','PermintaanBarangController@update_permintaan')->name('permintaanbarang.update_permintaan');
Route::get('/permintaanbarang/destroy_detail/{id}', 'PermintaanBarangController@destroy_detail')->name('permintaanbarang.destroy_detail');


Route::get('/service', 'ServiceController@index')->name('service.index');
Route::get('/service/indexData', 'ServiceController@indexData')->name('service.indexData');
Route::get('/service/cetak_pdf/{id}', 'ServiceController@cetak_pdf')->name('service.cetak_pdf');
Route::post('/service/proses', 'ServiceController@proses')->name('service.proses');
Route::post('/service/selesai', 'ServiceController@selesai')->name('service.selesai');


Route::get('/kunjungan', 'KunjunganController@index')->name('kunjungan.index');
Route::get('/kunjungan/visit', 'KunjunganController@visit')->name('kunjungan.visit');
Route::post('/kunjungan/indexAjax', 'KunjunganController@indexAjax')->name('kunjungan.indexAjax');
Route::post('/kunjungan/visitAjax', 'KunjunganController@visitAjax')->name('kunjungan.visitAjax');
Route::post('/kunjungan/store', 'KunjunganController@store')->name('kunjungan.store');
Route::post('/kunjungan/destroy', 'KunjunganController@destroy')->name('kunjungan.destroy');


Route::get('/member_sdm', 'MemberSdmController@index')->name('member_sdm.index');
Route::post('/member_sdm/store', 'MemberSdmController@store')->name('member_sdm.store');
Route::post('/member_sdm/store_file', 'MemberSdmController@store_file')->name('member_sdm.store_file');
Route::get('/member_sdm/index-data', 'MemberSdmController@indexData')->name('member_sdm.indexData');
Route::get('/member_sdm/index-edit', 'MemberSdmController@indexEdit')->name('member_sdm.indexEdit');
Route::get('member_sdm/destroy/{id}', 'MemberSdmController@destroy')->name('member_sdm.destroy');
Route::post('/member_sdm/update', 'MemberSdmController@update')->name('member_sdm.update');
Route::post('/member_sdm/update_password', 'MemberSdmController@update_password')->name('member_sdm.update_password');
Route::post('/member_sdm/update_image', 'MemberSdmController@update_image')->name('member_sdm.update_image');
Route::post('/member_sdm/update_pkwt', 'MemberSdmController@update_pkwt')->name('member_sdm.update_pkwt');
Route::post('/member_sdm/update_partner', 'MemberSdmController@update_partner')->name('member_sdm.update_partner');
Route::post('/member_sdm/look/','MemberSdmController@look')->name('member_sdm.look');
Route::post('/member_sdm/look_position/','MemberSdmController@look_position')->name('member_sdm.look_position');
Route::post('/member_sdm/look_file/','MemberSdmController@look_file')->name('member_sdm.look_file');
Route::post('/member_sdm/delete_file','MemberSdmController@delete_file')->name('member_sdm.delete_file');
Route::post('/member_sdm/delete_history','MemberSdmController@delete_history')->name('member_sdm.delete_history');
Route::get('/member_sdm/edit/{id}', 'MemberSdmController@edit')->name('member_sdm.edit');
Route::get('/member_sdm/download_fileex', 'MemberSdmController@download_fileex')->name('member_sdm.download_fileex');
Route::post('/member_sdm/import', 'MemberSdmController@import')->name('member_sdm.import');
Route::post('/member_sdm/look_pkwt/','MemberSdmController@look_pkwt')->name('member_sdm.look_pkwt');
Route::post('/member_sdm/delete_pkwt','MemberSdmController@delete_pkwt')->name('member_sdm.delete_pkwt');
Route::post('/member_sdm/export_pdf','MemberSdmController@export_pdf')->name('member_sdm.export_pdf');

Route::get('/logistik', 'LogistikController@index')->name('logistik.index');
Route::get('/logistik/indexData', 'LogistikController@indexData')->name('logistik.indexData');
Route::get('/logistik/dataItem', 'LogistikController@dataItem')->name('logistik.dataItem');
Route::get('/logistik/cetak/{id}', 'LogistikController@cetak')->name('logistik.cetak');
Route::post('/logistik/store', 'LogistikController@store')->name('logistik.store');
Route::get('logistik/destroy/{id}', 'LogistikController@destroy')->name('logistik.destroy');
Route::post('/logistik/update', 'LogistikController@update')->name('logistik.update');
Route::post('/logistik/look/','LogistikController@look')->name('logistik.look');
Route::post('/logistik/setujui/','LogistikController@setujui')->name('logistik.setujui');
Route::post('/logistik/tolak/','LogistikController@tolak')->name('logistik.tolak');

Route::get('/visit', 'VisitController@index')->name('visit.index');
Route::post('/visit/store', 'VisitController@store')->name('visit.store');
Route::post('/visit/indexAjax', 'VisitController@indexAjax')->name('visit.indexAjax');
Route::post('/visit/destroy', 'VisitController@destroy')->name('visit.destroy');

Route::get('/activity', 'ActivityController@index')->name('activity.index');
Route::get('/activity/indexData', 'ActivityController@indexData')->name('activity.indexData');
Route::post('/activity/look/','ActivityController@look')->name('activity.look');

Route::get('/permintaanbarangkorlap', 'PermintaanBarangKorlapController@index')->name('permintaanbarangkorlap.index');
Route::get('/permintaanbarangkorlap/indexData', 'PermintaanBarangKorlapController@indexData')->name('permintaanbarangkorlap.indexData');
Route::get('/permintaanbarangkorlap/cetak/{id}', 'PermintaanBarangKorlapController@cetak')->name('permintaanbarangkorlap.cetak');

Route::get('/servicekorlap', 'ServiceKorlapController@index')->name('servicekorlap.index');
Route::get('/servicekorlap/indexData', 'ServiceKorlapController@indexData')->name('servicekorlap.indexData');

Route::get('/createAbsen', 'CreateAbsenController@index')->name('createAbsen.index');
Route::post('/createAbsen/lookUser', 'CreateAbsenController@lookUser')->name('createAbsen.lookUser');
Route::post('/createAbsen/indexAjax', 'CreateAbsenController@indexAjax')->name('createAbsen.indexAjax');
Route::post('/createAbsen/ajax','CreateAbsenController@ajax')->name('createAbsen.ajax');