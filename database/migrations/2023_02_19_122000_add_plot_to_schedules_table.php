<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPlotToSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->timestamp('start_plot')->after('end')->nullable();
            $table->timestamp('end_plot')->after('end')->nullable();
            $table->timestamp('start_absen')->after('end')->nullable();
            $table->timestamp('end_absen')->after('end')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->dropColumn('start_plot');
            $table->dropColumn('end_plot');
            $table->dropColumn('start_absen');
            $table->dropColumn('end_absen');
            
        });
    }
}