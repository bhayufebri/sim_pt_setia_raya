<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPpnPphInvoiceToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->integer('ppn')->after('expired')->nullable();
            $table->integer('pph')->after('expired')->nullable();
            $table->integer('total')->after('expired')->nullable();
            $table->text('invoice_number')->after('expired')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('ppn');
            $table->dropColumn('pph');
            $table->dropColumn('total');
            $table->dropColumn('invoice_number');
            
        });
    }
}