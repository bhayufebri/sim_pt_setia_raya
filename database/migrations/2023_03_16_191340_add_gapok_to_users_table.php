<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGapokToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('gaji_pokok')->after('is_payroll')->nullable();
            $table->integer('tunjangan_jabatan')->after('is_payroll')->nullable();
            $table->integer('potongan_bpjs_kesehatan')->after('is_payroll')->nullable();
            $table->integer('potongan_bpjs_ketenagakerjaan')->after('is_payroll')->nullable();
            $table->integer('potongan_payroll')->after('is_payroll')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gaji_pokok');
            $table->dropColumn('tunjangan_jabatan');
            $table->dropColumn('potongan_bpjs_kesehatan');
            $table->dropColumn('potongan_bpjs_ketenagakerjaan');
            $table->dropColumn('potongan_payroll');
        });
    }
}