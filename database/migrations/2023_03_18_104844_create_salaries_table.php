<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->id();
            $table->string('company_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('period_id')->nullable();
            $table->integer('pendapatan_all')->nullable();
            $table->integer('potongan_all')->nullable();
            $table->integer('total_gaji')->nullable();
            $table->text('uraian')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}