<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeColunmToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('nik')->after('email')->nullable();
            $table->string('tempat_lahir')->after('email')->nullable();
            $table->date('tanggal_lahir')->after('email')->nullable();
            $table->string('alamat')->after('email')->nullable();
            $table->string('no_handpone')->after('email')->nullable();
            $table->string('nama_ibu_kandung')->after('email')->nullable();
            $table->date('awal_masuk')->after('email')->nullable();
            $table->string('is_bpjstk')->after('email')->nullable();
            $table->string('is_bpjskes')->after('email')->nullable();
            $table->string('partner_id')->after('email')->nullable();
            $table->string('status_id')->after('email')->nullable();
            $table->string('division_id')->after('email')->nullable();
            $table->string('position_id')->after('email')->nullable();
            $table->string('is_active')->after('email')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('nik');
            $table->dropColumn('tempat_lahir');
            $table->dropColumn('tanggal_lahir');
            $table->dropColumn('alamat');
            $table->dropColumn('no_handpone');
            $table->dropColumn('nama_ibu_kandung');
            $table->dropColumn('awal_masuk');
            $table->dropColumn('is_bpjstk');
            $table->dropColumn('is_bpjskes');
            $table->dropColumn('partner_id');
            $table->dropColumn('status_id');
            $table->dropColumn('division_id');
            $table->dropColumn('position_id');
            $table->dropColumn('is_active');
        });
    }
}