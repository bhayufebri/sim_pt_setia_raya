<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLatitudeToSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->string('radius_berangkat')->after('title')->nullable();
            $table->string('radius_pulang')->after('title')->nullable();
            $table->text('latlngBerangkat')->after('title')->nullable();
            $table->text('latlngPulang')->after('title')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedules', function (Blueprint $table) {
            $table->dropColumn('radius_berangkat');
            $table->dropColumn('radius_pulang');
            $table->dropColumn('latlngBerangkat');
            $table->dropColumn('latlngPulang');
            
        });
    }
}